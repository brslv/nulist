import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import { Page, PageMain } from "../../../components/atomic/Page/Page";
import { ListEditor } from "../../../components/ListEditor/ListEditor";
import { useAuth } from "../../../core/auth/context/AuthProvider";
import { ListEditorProvider } from "../../../core/list/context/ListEditorProvider";

export default function ListEdit() {
  const router = useRouter();
  const { user, loading } = useAuth();
  const { uid } = router.query;

  useEffect(() => {
    if (!user && !loading) router.push("/");
  }, [loading, router, user]);

  if (!user || loading) return null;

  return (
    <Page>
      <Head>
        <title key="title">Nulist • Edit list</title>
        <meta property="og:title" content="Nulist • Edit list" key="og:title" />
      </Head>
      <PageMain>
        <ListEditorProvider uid={uid as string}>
          <ListEditor />
        </ListEditorProvider>
      </PageMain>
    </Page>
  );
}
