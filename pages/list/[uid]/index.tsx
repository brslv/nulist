import { GetServerSideProps } from "next";
import React from "react";
import { ListViewContainer } from "../../../components/pages/ListViewContainer/ListViewContainer";
import { setAuth } from "../../../core/auth/authService";
import { getList } from "../../../core/list/listService";
import { IList } from "../../../core/list/types/IList";
import PageNotFound from "../../404";

interface IProps {
  list: IList | null;
}

export default function ListViewPage({ list }: IProps) {
  if (!list) {
    return <PageNotFound />;
  }

  return <ListViewContainer list={list} />;
}

export const getServerSideProps: GetServerSideProps = async ({
  query,
  req,
  res,
}) => {
  setAuth(req);

  const { uid } = query;

  if (Array.isArray(uid)) {
    res.statusCode = 404;
    return { props: {} };
  }

  let list;
  try {
    const { list: l } = await getList({ uid });
    list = l;
  } catch (error) {}

  if (!list) {
    res.statusCode = 404;
    return { props: {} };
  }

  return {
    props: {
      list,
    },
  };
};
