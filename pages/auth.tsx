import { GetServerSideProps } from "next";
import { Auth } from "../components/pages/Auth/Auth";
import { getUserByCookie } from "../core/auth/authService";

export default function AuthPage() {
  return <Auth />;
}

export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  const { user } = await getUserByCookie(req);

  if (user) {
    return {
      redirect: {
        destination: "/dashboard",
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
};
