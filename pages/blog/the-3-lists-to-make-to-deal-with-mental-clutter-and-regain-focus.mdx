---
title: "The 3 lists to make to deal with mental clutter and regain focus"
date: "2021-11-16"
description: "Mental clutter hinders your judgement, focus and productivity. Fix it in 15 minutes with these 3 simple lists!"
image:
  src: "/blog/images/the-3-lists-to-make-to-deal-with-mental-clutter-and-regain-focus/main.png"
  alt: "Bulleted list illustration"
  coverPosition: "center"
  coverPositionInArticle: "center"
---

import Link from "next/link";
import { withLayout } from "../../core/blog/withLayout";
import { BlogSubscribeForm } from "../../components/blog/BlogSubscribeForm/BlogSubscribeForm";
import { Separator } from "../../components/blog/Separator/Separator";
import { Button } from "../../components/atomic/Button/Button";
import { CallToActionPanel } from "../../components/blog/CallToActionPanel/CallToActionPanel";
import { BackToBlogBtn } from "../../components/blog/BackToBlogBtn/BackToBlogBtn";

## Your mind is like your desktop

We all know how cluttered a desktop can get.

I have tons of screenshots, random files, spreadsheets, docs, folders, and whatnot.

It sounds scary, but there is a system to this chaos.

Once a day, I go through every file on my desktop, put it into a folder, or delete it right away. In a matter of 5 to 10 minutes, my desktop is back to normal.

### Your mental desktop

There's a big chance that your mental desktop looks like this.

![Cluttered desktop illustration](/blog/images/the-3-lists-to-make-to-deal-with-mental-clutter-and-regain-focus/desktop.png)

Mine often does.

I put everything on it throughout the day. Thoughts and ideas on my running projects. Goals. Things to buy. People to call. Books to read, articles to explore.

It's all there.

The more stuff I put on it, the more cluttered it gets, the harder for me to think clearly and act with intention.

### How to deal with mental clutter using lists?

Here's what you should do.

Sit in a quiet place with a cup of coffee or tea.

Get a sharp pencil and a sheet of paper. Or, if you prefer digital, give nuli.st a try (more on this in a moment).

Make these three lists, one after the other:

## 1. List of things to do today

![Today calendar illustration](/blog/images/the-3-lists-to-make-to-deal-with-mental-clutter-and-regain-focus/todo-today-list.png)

As obvious as it may sound, this is your most important list for the day.

My favorite technique when it comes to to-do lists is the so-called "1-2-3 technique".

Here's the gist.

You can only do one big task, 2 medium tasks, and 3 smaller ones in a day.

It's that simple.

I like to add a twist to the framework, though.

After I've written down my 1-2-3 tasks, I like to dump all the tasks that didn't make it to the top.

Like:

- To read the article I've been postponing for two days already, or
- To come up with an engaging Twitter thread on a given topic.

These are tasks that are not so urgent but are taking some mental space up there. Writing a to-do list is the best way to free up some of that processing power for your brain.

## 2. List of things to do this month

![Calendar illustration](/blog/images/the-3-lists-to-make-to-deal-with-mental-clutter-and-regain-focus/monthly-list.png)

I like to make a list of 5 to 15 monthly tasks at the beginning of each month. I usually do it on the first Sunday of the current month, but you could do it anytime.

Once I have it, I review it every day, adding new items to it or prioritizing the ones that are more urgent, moving them to the 1-2-3 daily list.

Don't go into details, though. This is not a strategy document, nor an action plan with many subtasks. It's a high-level overview of where your focus should go this month.

I urge you to be as concise as possible.

Try to stick to 3-5 words per item, as the primary reason for this list to exist is to provide a skeleton for your best month.

Remember, an architect doesn't start designing a house from the furniture. They sketch the "shell" first.

That's what this list is all about. Stay focused.

## 3. The trash list

![Trash illustration](/blog/images/the-3-lists-to-make-to-deal-with-mental-clutter-and-regain-focus/trash-list.png)

This is my favorite list of all, as there are no rules in it!

You list down whatever is on your mind.

The idea is to see your thoughts. Once you see them, they're no longer abstract. They get moved from your mental desktop to a folder in your mind if you stick to the analogy.

Here's an example from one of my latest trash lists.

- Love "We the living". I must definitely re-read it!
- Loved the idea of anti-fragility from Taleb's book. Write an article about it in my blog?
- What are some ways to become more robust and less fragile?
- Gotta explore web3 more!
- How can I optimize the time spent on my side project? Think about it!

It doesn't have to be perfect.

Go for raw, unedited, direct writing. Don't look for the right words, and don't bother with the punctuation.

Spill your thoughts on the list and forget about it.

That's the beauty of it.

## How nulist can help you declutter your mind?

Nulist is the perfect digital companion when it comes to making lists.

It's simple, intuitive and feels like a real-world blank sheet...with digital superpowers.

The free plan is quite generous, so why don't you give it a spin and make your first three most important lists there?

<CallToActionPanel>
  <div
    style={{
      textAlign: "center",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      flexDirection: "column",
      padding: 20,
    }}
  >
    <Link href="/" passHref>
      <Button fontSize={40} component="a" variant="primary">
        Take me to the list editor
      </Button>
    </Link>
    <small>It's totally free, no registration needed!</small>
  </div>
</CallToActionPanel>

## Conclusion

Mental clutter is a thing.

That's why we need to make decluttering a habit.

It's not rocket science - all you need is a couple of lists and 15 minutes a day!

Even though a pencil and paper would do, nulist is a great digital companion if you need to reference some links or other "zeros-and-ones" information out there.

Have fun, make lists and stay calm!

<Separator />

<div className="guard">
  <BlogSubscribeForm />
</div>

<Separator />

<BackToBlogBtn />
