import Head from "next/head";
import React from "react";
import { Page, PageMain } from "../../components/atomic/Page/Page";
import { BlogSubscribeForm } from "../../components/blog/BlogSubscribeForm/BlogSubscribeForm";
import { Blog } from "../../components/pages/Blog/Blog";
import { IBlogArticle } from "../../core/blog/types/IBlogArticle";
import { getAllArticles } from "../../core/blog/utils/getAllArticles";

interface IProps {
  articles: IBlogArticle[];
}

export default function BlogPage({ articles }: IProps) {
  return (
    <Page>
      <Head>
        <title key="title">Nulist • Blog</title>
        <meta property="og:title" content="Nulist • Blog" key="og:title" />
      </Head>
      <PageMain>
        <Blog articles={articles} />
        <BlogSubscribeForm />
      </PageMain>
    </Page>
  );
}

export async function getStaticProps() {
  const articles = getAllArticles();

  // Feed the page with articles data
  return { props: { articles } };
}
