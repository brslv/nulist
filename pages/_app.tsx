import { AppProps } from "next/app";
import "../styles/globals.css";
import "../components/atomic/RichEditor/RichEditor.css";
import Progress from "nextjs-progressbar";
import React, { ReactNode } from "react";
import Head from "next/head";
import { Image } from "../components/blog/Image/Image";
import { OnboardingModalsProvider } from "../core/onboarding/modals/OnboardingModalsProvider";
import { PageControls } from "../components/PageControls/PageControls";
import { AuthProvider } from "../core/auth/context/AuthProvider";
import { NotificationStack } from "../components/atomic/NotificationStack/NotificationStack";
import { NotificationsProvider } from "../core/notifications/context/NotificationsProvider";
import { UserListsProvider } from "../core/list/context/UserListsProvider";
import { AuthenticatedUserProfileProvider } from "../core/profile/context/AuthenticatedUserProfileProvider";
import { PageControlsProvider } from "../components/PageControls/PageControlsProvider";
import { PlansModal } from "../components/PlansModal/PlansModal";
import { PlansModalProvider } from "../core/plans/context/PlansModalProvider";
import { FeedbackModal } from "../components/FeedbackModal/FeedbackModal";
import { HelpModal } from "../components/Help/HelpModal";
import { MDXProvider } from "@mdx-js/react";
import { useCanonicalUrl } from "../lib/useCanonicalUrl";
import { withLayout } from "../core/blog/withLayout";
import { BundlesProvider } from "../core/bundles/context/BundlesProvider";
import { BundleFormProvider } from "../core/bundles/context/BundleFormProvider";
import { BundleFormModal } from "../components/BundleFormModal/BundleFormModal";
import { BreadcrumbsProvider } from "../core/breadcrumbs/context/BreadcrumbsProvider";

function MyApp({ Component, pageProps }: AppProps) {
  const canonicalUrl = useCanonicalUrl();

  return (
    <>
      <Head>
        <title key="title">Nulist</title>
        <meta property="og:title" content="Nulist" key="og:title" />
        <meta
          property="description"
          content="All purpose list-making app, embracing simplicity and functionality. Organize your thoughts, knowledge and resources in lists."
          key="description"
        />
        <meta
          property="og:description"
          content="All purpose list-making app, embracing simplicity and functionality. Organize your thoughts, knowledge and resources in lists."
          key="og:description"
        />
        <meta
          property="og:image"
          content="https://nuli.st/main-image.png"
          key="og:image"
        />
        <meta property="og:url" content="https://nuli.st" key="og:url" />
        <meta
          name="twitter:card"
          content="summary_large_image"
          key="og:twitter:card"
        />

        <meta property="og:site_name" content="Nulist" key="og:site_name" />
        <meta
          name="twitter:image:alt"
          content="Nulist main image"
          key="og:twitter:image:alt"
        />
        <link rel="canonical" href={canonicalUrl} />

        <script
          defer
          data-domain="nuli.st"
          src="https://plausible.io/js/plausible.js"
        ></script>
        {/* <script defer src="https://gumroad.com/js/gumroad.js"></script> */}
        <script
          dangerouslySetInnerHTML={{
            __html: `
          if (window !== undefined)
          {
            (window.plausible =
              window.plausible ||
              function () {
                (window.plausible.q = window.plausible.q || []).push(arguments);
              })
          }
        `,
          }}
        ></script>
      </Head>
      <BreadcrumbsProvider>
        <NotificationsProvider>
          <AuthProvider>
            <AuthenticatedUserProfileProvider>
              <UserListsProvider>
                <PlansModalProvider>
                  <BundlesProvider>
                    <BundleFormProvider>
                      <OnboardingModalsProvider>
                        <PageControlsProvider>
                          <PageControls />
                          <MDXProvider
                            components={{
                              img: Image,
                              wrapper: ({
                                frontMatter,
                                children,
                              }: {
                                frontMatter: any;
                                children: ReactNode;
                              }) => {
                                return withLayout(frontMatter)({ children });
                              },
                            }}
                          >
                            <Component {...pageProps} />
                          </MDXProvider>
                          <PlansModal />
                          <NotificationStack />
                          <FeedbackModal />
                          <HelpModal />
                          <BundleFormModal />
                        </PageControlsProvider>
                      </OnboardingModalsProvider>
                    </BundleFormProvider>
                  </BundlesProvider>
                </PlansModalProvider>
              </UserListsProvider>
            </AuthenticatedUserProfileProvider>
          </AuthProvider>
        </NotificationsProvider>
      </BreadcrumbsProvider>
      <Progress
        color="#ef7e75"
        startPosition={0.3}
        stopDelayMs={100}
        height={3}
        showOnShallow={true}
      />
    </>
  );
}

export default MyApp;
