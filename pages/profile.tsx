import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import { Page, PageMain } from "../components/atomic/Page/Page";
import { Profile } from "../components/pages/Profile/Profile";
import { useAuth } from "../core/auth/context/AuthProvider";
import { IUser } from "../core/auth/types/IUser";
import { useAuthenticatedUserProfile } from "../core/profile/context/AuthenticatedUserProfileProvider";

export default function ProfilePage() {
  const router = useRouter();
  const { user, loading } = useAuth();
  const { profile } = useAuthenticatedUserProfile();

  useEffect(() => {
    if (!user && !loading) {
      router.push("/");
    }
  }, [loading, router, user]);

  if (!user && loading) return null;
  if (!user) return null;

  return (
    <Page>
      <Head>
        <title key="title">Nulist • Profile</title>
        <meta property="og:title" content="Nulist • Profile" key="og:title" />
      </Head>
      <PageMain>
        <Profile user={user as IUser} profile={profile} />
      </PageMain>
    </Page>
  );
}
