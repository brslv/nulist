import Link from "next/link";
import React from "react";
import { Button } from "../components/atomic/Button/Button";
import { Title } from "../components/atomic/Title/Title";

export default function PageNotFound() {
  return (
    <div
      style={{
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <Title size={40} weight="light" style={{ paddingBottom: 10 }}>
        Page not found
      </Title>
      <Link href="/" passHref>
        <Button component="a" variant="basic">
          Home
        </Button>
      </Link>
    </div>
  );
}
