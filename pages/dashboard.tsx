import { GetServerSideProps } from "next";
import Head from "next/head";
import React from "react";
import { Page, PageMain } from "../components/atomic/Page/Page";
import { Dashboard } from "../components/pages/Dashboard/Dashboard";
import { getUserByCookie } from "../core/auth/authService";

export default function DashboardPage() {
  return (
    <Page>
      <Head>
        <title key="title">Nulist • Dashboard</title>
        <meta property="og:title" content="Nulist • Dashboard" key="og:title" />
      </Head>
      <PageMain wide>
        <Dashboard />
      </PageMain>
    </Page>
  );
}

export const getServerSideProps: GetServerSideProps = async ({
  req,
}): Promise<any> => {
  const { user } = await getUserByCookie(req);

  if (!user) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
};
