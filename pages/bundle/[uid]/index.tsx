import { GetServerSideProps } from "next";
import React from "react";
import { BundleView } from "../../../components/pages/BundleView/BundleView";
import { setAuth } from "../../../core/auth/authService";
import { getBundle } from "../../../core/bundles/bundleService";
import { IBundle } from "../../../core/bundles/types/IBundle";

interface IProps {
  bundle: IBundle;
}

export default function BundleViewPage({ bundle }: IProps) {
  return <BundleView bundle={bundle} />;
}

export const getServerSideProps: GetServerSideProps = async ({
  query,
  req,
  res,
}) => {
  setAuth(req);

  const { uid } = query;

  if (Array.isArray(uid)) {
    res.statusCode = 404;
    return { props: {} };
  }

  let bundle;
  try {
    // @Todo: remove Number cast
    const { data } = await getBundle({ id: Number(uid) });
    bundle = data ? data[0] : null;
  } catch (error) {}

  if (!bundle) {
    res.statusCode = 404;
    return { props: {} };
  }

  return {
    props: {
      bundle,
    },
  };
};
