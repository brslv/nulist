import type { NextApiRequest, NextApiResponse } from "next";
import {
  handleCancellation,
  handleOtherEvents,
  handleSale,
  handleSubscriptionEnd,
} from "../../core/gumroad/gumroad";
import { supabase } from "../../lib/supabaseClient";

const GUMROAD_PRODUCT_ID = process.env.GUMROAD_PRODUCT_ID;
const AUTH_KEY = process.env.SUPABASE_SERVICE_KEY;

export default async function GumroadWebhook(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (!AUTH_KEY) throw new Error("No supabase service key found.");

  supabase.auth.setAuth(AUTH_KEY);

  const { product_id: productId, test, resource_name: resourceName } = req.body;
  const isTest = test === "true";
  const isProd = process.env.NODE_ENV === "production";

  try {
    if (
      ((!isTest && isProd) || !isProd) &&
      req.method?.toUpperCase() === "POST" &&
      productId === GUMROAD_PRODUCT_ID
    ) {
      if (resourceName === "sale") {
        await handleSale(req.body);
      } else if (resourceName === "cancellation") {
        await handleCancellation(req.body);
      } else if (resourceName === "subscription_ended") {
        await handleSubscriptionEnd(req.body);
      } else {
        await handleOtherEvents(req.body);
      }
    }

    res.status(200).send("");
  } catch (error: any) {
    console.log(error);
    res.status(500).send(error.message);
  }
}
