import React from "react";
import Head from "next/head";
import { Create } from "../components/pages/Create/Create";
import { GetServerSideProps } from "next";
import { resetServerContext } from "react-beautiful-dnd";
import { ListEditorProvider } from "../core/list/context/ListEditorProvider";
import { useUnsavedListEditorState } from "../core/list/hooks/useUnsavedListEditorState";
import { useRouter } from "next/router";
import { GuestModal } from "../components/GuestModal/GuestModal";
import { getUserByCookie } from "../core/auth/authService";

interface IProps {
  isGuestModalOpen: boolean;
}

export default function HomePage({ isGuestModalOpen }: IProps) {
  const router = useRouter();
  const query = router.query;
  const loadUnsaved = query["load-unsaved"] === "true";
  const { unsavedListEditorState } = useUnsavedListEditorState();

  return (
    <>
      <Head>
        <title key="title">Nulist</title>
        <meta property="og:title" content="Nulist" key="og:title" />
      </Head>
      <ListEditorProvider
        initialState={loadUnsaved ? unsavedListEditorState : null}
      >
        <Create />
        <GuestModal isOpen={isGuestModalOpen} />
      </ListEditorProvider>
    </>
  );
}

export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  // We need to reset react-beautiful-dnd's state on the server.
  // -----------------------------------------------------------------------------------
  // The following error occurs only in development, because of the StrictMode.
  // Error: Prop `data-rbd-droppable-context-id` did not match. Server: "0" Client: "1".
  // -----------------------------------------------------------------------------------
  // As noted in the discussion from the link bellow:
  // [..] looks like this can be attributed to StrictMode.
  // In a dev environment rbd will render twice,
  // so the expected ID is incremented to 1 instead of 0.
  // The server reset doesn't expect this, thus the mismatch.
  // -----------------------------------------------------------------------------------
  // More: https://giters.com/atlassian/react-beautiful-dnd/issues/2276?amp=1
  // -----------------------------------------------------------------------------------
  resetServerContext();

  const { user } = await getUserByCookie(req);

  return {
    props: {
      isGuestModalOpen: !!!user,
    },
  };
};
