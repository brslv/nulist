import dayjs from "dayjs";
import { GetServerSideProps } from "next";
import { ROOT_URL } from "../core/config";
import { pages, blogArticles } from "../core/sitemap/config";

const Sitemap = () => {
  return;
};

export const getServerSideProps: GetServerSideProps = async ({ res }) => {
  const sitemap = `<?xml version="1.0" encoding="UTF-8"?>
  <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    ${pages
      .map(url => {
        return `
          <url>
            <loc>${url}</loc>
            <lastmod>${dayjs().format("YYYY-MM-DD")}</lastmod>
            <changefreq>monthly</changefreq>
            <priority>1.0</priority>
          </url>
        `;
      })
      .join("")}

    ${blogArticles.map(article => {
      const { metadata, slug } = article;
      return `
           <url>
            <loc>${ROOT_URL + slug}</loc>
            <lastmod>${metadata.date}</lastmod>
            <changefreq>monthly</changefreq>
            <priority>1.0</priority>
          </url>
      `;
    })}
  </urlset>
`;

  res.setHeader("Content-Type", "text/xml");
  res.write(sitemap);
  res.end();

  return {
    props: {},
  };
};

export default Sitemap;
