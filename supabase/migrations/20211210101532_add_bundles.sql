-- This script was generated by the Schema Diff utility in pgAdmin 4
-- For the circular dependencies, the order in which Schema Diff writes the objects is not very sophisticated
-- and may require manual changes to the script to ensure changes are applied in the correct order.
-- Please report an issue for any failure with the reproduction steps.

CREATE TABLE IF NOT EXISTS public.bundles
(
    id bigint NOT NULL GENERATED BY DEFAULT AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    title text COLLATE pg_catalog."default" NOT NULL,
    "isPublic" boolean NOT NULL DEFAULT false,
    "userUid" uuid NOT NULL,
    "profileId" uuid NOT NULL,
    "createdAt" timestamp with time zone NOT NULL DEFAULT timezone('utc'::text, now()),
    "updatedAt" timestamp with time zone NOT NULL DEFAULT timezone('utc'::text, now()),
    CONSTRAINT bundles_pkey PRIMARY KEY (id),
    CONSTRAINT "bundles_profileId_fkey" FOREIGN KEY ("profileId")
        REFERENCES public.profiles (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,
    CONSTRAINT "bundles_userUid_fkey" FOREIGN KEY ("userUid")
        REFERENCES auth.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.bundles
    OWNER to supabase_admin;

ALTER TABLE IF EXISTS public.bundles
    ENABLE ROW LEVEL SECURITY;

GRANT ALL ON TABLE public.bundles TO anon;

GRANT ALL ON TABLE public.bundles TO authenticated;

GRANT ALL ON TABLE public.bundles TO postgres;

GRANT ALL ON TABLE public.bundles TO service_role;

GRANT ALL ON TABLE public.bundles TO supabase_admin;
CREATE POLICY "Enable access to private bundles to owners only"
    ON public.bundles
    AS PERMISSIVE
    FOR SELECT
    TO public
    USING ((auth.uid() = "userUid"));
CREATE POLICY "Enable access to public bundles"
    ON public.bundles
    AS PERMISSIVE
    FOR SELECT
    TO public
    USING (("isPublic" = true));
CREATE POLICY "Enable delete of own bundles."
    ON public.bundles
    AS PERMISSIVE
    FOR DELETE
    TO public
    USING ((auth.uid() = "userUid"));
CREATE POLICY "Enable insert for authenticated users only."
    ON public.bundles
    AS PERMISSIVE
    FOR INSERT
    TO public
    WITH CHECK ((auth.role() = 'authenticated'::text));
CREATE POLICY "Enable update for users based on userUid"
    ON public.bundles
    AS PERMISSIVE
    FOR UPDATE
    TO public
    USING ((auth.uid() = "userUid"))
    WITH CHECK ((auth.uid() = "userUid"));

CREATE TABLE IF NOT EXISTS public."bundlesLists"
(
    "bundleId" integer NOT NULL,
    "listId" integer NOT NULL,
    "userUid" uuid,
    "profileId" uuid,
    CONSTRAINT "bundlesListPkey" PRIMARY KEY ("bundleId", "listId"),
    CONSTRAINT "bundlesLists_bundleId_fkey" FOREIGN KEY ("bundleId")
        REFERENCES public.bundles (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT "bundlesLists_listId_fkey" FOREIGN KEY ("listId")
        REFERENCES public.lists (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT "bundlesLists_profileId_fkey" FOREIGN KEY ("profileId")
        REFERENCES public.profiles (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION,
    CONSTRAINT "bundlesLists_userUid_fkey" FOREIGN KEY ("userUid")
        REFERENCES auth.users (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public."bundlesLists"
    OWNER to supabase_admin;

ALTER TABLE IF EXISTS public."bundlesLists"
    ENABLE ROW LEVEL SECURITY;

GRANT ALL ON TABLE public."bundlesLists" TO anon;

GRANT ALL ON TABLE public."bundlesLists" TO authenticated;

GRANT ALL ON TABLE public."bundlesLists" TO postgres;

GRANT ALL ON TABLE public."bundlesLists" TO service_role;

GRANT ALL ON TABLE public."bundlesLists" TO supabase_admin;
CREATE POLICY "Enable access all items in bundlesLists"
    ON public."bundlesLists"
    AS PERMISSIVE
    FOR SELECT
    TO public
    USING (true);
CREATE POLICY "Enable delete of own items."
    ON public."bundlesLists"
    AS PERMISSIVE
    FOR DELETE
    TO public
    USING ((auth.uid() = "userUid"));
CREATE POLICY "Enable insert for authenticated users only."
    ON public."bundlesLists"
    AS PERMISSIVE
    FOR INSERT
    TO public
    WITH CHECK ((auth.role() = 'authenticated'::text));
CREATE POLICY "Enable update for users based on userUid"
    ON public."bundlesLists"
    AS PERMISSIVE
    FOR UPDATE
    TO public
    USING ((auth.uid() = "userUid"))
    WITH CHECK ((auth.uid() = "userUid"));

CREATE OR REPLACE FUNCTION public."addListToBundles"(
	"listId" integer,
	"bundleIds" integer[])
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
  bid integer;
BEGIN
  FOREACH bid IN ARRAY "bundleIds"
  LOOP
    -- RAISE 'bid: %, listId: %, auth.uid(): %', bid, "listId", auth.uid();
    INSERT INTO "bundlesLists"("bundleId", "listId", "userUid", "profileId")
    VALUES (bid, "listId", auth.uid(), auth.uid());
  END LOOP;
  RETURN 1;
END
$BODY$;

ALTER FUNCTION public."addListToBundles"(integer, integer[])
    OWNER TO supabase_admin;

GRANT EXECUTE ON FUNCTION public."addListToBundles"(integer, integer[]) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public."addListToBundles"(integer, integer[]) TO anon;

GRANT EXECUTE ON FUNCTION public."addListToBundles"(integer, integer[]) TO authenticated;

GRANT EXECUTE ON FUNCTION public."addListToBundles"(integer, integer[]) TO postgres;

GRANT EXECUTE ON FUNCTION public."addListToBundles"(integer, integer[]) TO service_role;

GRANT EXECUTE ON FUNCTION public."addListToBundles"(integer, integer[]) TO supabase_admin;

CREATE OR REPLACE FUNCTION public."addListWithItems"(
	list json,
	"listItems" json,
	"bundleIds" integer[])
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE 
  "insertedListId" integer;
  "addListItemsResult" integer;
  "addListToBundlesResult" integer;
BEGIN
  SELECT "addList"(list) INTO "insertedListId";
  SELECT "addListItems"("listItems", "insertedListId") INTO "addListItemsResult";
  SELECT "addListToBundles"("insertedListId", "bundleIds") INTO "addListToBundlesResult";
  RETURN 1;
END
$BODY$;

ALTER FUNCTION public."addListWithItems"(json, json, integer[])
    OWNER TO supabase_admin;

GRANT EXECUTE ON FUNCTION public."addListWithItems"(json, json, integer[]) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public."addListWithItems"(json, json, integer[]) TO anon;

GRANT EXECUTE ON FUNCTION public."addListWithItems"(json, json, integer[]) TO authenticated;

GRANT EXECUTE ON FUNCTION public."addListWithItems"(json, json, integer[]) TO postgres;

GRANT EXECUTE ON FUNCTION public."addListWithItems"(json, json, integer[]) TO service_role;

GRANT EXECUTE ON FUNCTION public."addListWithItems"(json, json, integer[]) TO supabase_admin;

CREATE OR REPLACE FUNCTION public."updateListWithItems"(
	list json,
	"listItems" json,
	"bundleIds" integer[])
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
  "addListItemsResult" integer;
  "addListToBundlesResult" integer;
BEGIN
  UPDATE lists
  SET title = list->>'title',
      description = list->>'description',
      emoji = list->>'emoji',
      "emojiName" = list->>'emojiName',
      "isPublic" = (list->>'isPublic')::boolean
  WHERE id = (list->>'id')::bigint;

  DELETE FROM "listItems" WHERE "listItems"."listId" = (list->>'id')::bigint;

  SELECT "addListItems"("listItems", (list->>'id')::bigint) INTO "addListItemsResult";

  DELETE FROM "bundlesLists" WHERE "listId" = (list->>'id')::bigint;

  SELECT "addListToBundles"((list->>'id')::bigint, "bundleIds") INTO "addListToBundlesResult";

  RETURN 1;
END
$BODY$;

ALTER FUNCTION public."updateListWithItems"(json, json, integer[])
    OWNER TO supabase_admin;

GRANT EXECUTE ON FUNCTION public."updateListWithItems"(json, json, integer[]) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public."updateListWithItems"(json, json, integer[]) TO anon;

GRANT EXECUTE ON FUNCTION public."updateListWithItems"(json, json, integer[]) TO authenticated;

GRANT EXECUTE ON FUNCTION public."updateListWithItems"(json, json, integer[]) TO postgres;

GRANT EXECUTE ON FUNCTION public."updateListWithItems"(json, json, integer[]) TO service_role;

GRANT EXECUTE ON FUNCTION public."updateListWithItems"(json, json, integer[]) TO supabase_admin;

CREATE OR REPLACE FUNCTION public."addListToBundles"(
	"listId" bigint,
	"bundleIds" integer[])
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
  bid integer;
BEGIN
  FOREACH bid IN ARRAY "bundleIds"
  LOOP
    -- RAISE 'bid: %, listId: %, auth.uid(): %', bid, "listId", auth.uid();
    INSERT INTO "bundlesLists"("bundleId", "listId", "userUid", "profileId")
    VALUES (bid, "listId", auth.uid(), auth.uid());
  END LOOP;
  RETURN 1;
END
$BODY$;

ALTER FUNCTION public."addListToBundles"(bigint, integer[])
    OWNER TO supabase_admin;

GRANT EXECUTE ON FUNCTION public."addListToBundles"(bigint, integer[]) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public."addListToBundles"(bigint, integer[]) TO anon;

GRANT EXECUTE ON FUNCTION public."addListToBundles"(bigint, integer[]) TO authenticated;

GRANT EXECUTE ON FUNCTION public."addListToBundles"(bigint, integer[]) TO postgres;

GRANT EXECUTE ON FUNCTION public."addListToBundles"(bigint, integer[]) TO service_role;

GRANT EXECUTE ON FUNCTION public."addListToBundles"(bigint, integer[]) TO supabase_admin;

DROP FUNCTION IF EXISTS public."addListWithItems"(list json, "listItems" json);
