import React, { ComponentPropsWithRef } from "react";
import Tippy from "@tippyjs/react";
import "tippy.js/dist/tippy.css";
import "tippy.js/animations/shift-away.css";

export function WithTooltip({
  disabled = false,
  ...props
}: { disabled?: boolean } & ComponentPropsWithRef<typeof Tippy>) {
  if (disabled) return props.children || null;

  return (
    <Tippy
      animateFill
      animation="shift-away"
      arrow={false}
      offset={[0, 5]}
      placement="bottom"
      className="tooltip"
      touch="hold"
      trigger="mouseenter"
      {...props}
    >
      <span tabIndex={0} style={{ outline: "none" }}>
        {props.children}
      </span>
    </Tippy>
  );
}
