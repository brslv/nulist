import { useRouter } from "next/router";
import { ROOT_URL } from "../core/config";

export function useCanonicalUrl() {
  const CANONICAL_DOMAIN = ROOT_URL;

  const router = useRouter();
  const _pathSliceLength = Math.min.apply(Math, [
    router.asPath.indexOf("?") > 0
      ? router.asPath.indexOf("?")
      : router.asPath.length,
    router.asPath.indexOf("#") > 0
      ? router.asPath.indexOf("#")
      : router.asPath.length,
  ]);

  const canonicalURL =
    CANONICAL_DOMAIN + router.asPath.substring(0, _pathSliceLength);

  return canonicalURL.replace(/\/$/, "");
}
