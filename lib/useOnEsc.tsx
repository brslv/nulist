import { useOnKeyUp } from "./useOnKeyUp";

export default function useOnEsc(handler: () => void) {
  useOnKeyUp<KeyboardEvent>({
    fn: (e: any) => {
      if (e.key === "Escape") {
        handler();
      }
    },
  });
}
