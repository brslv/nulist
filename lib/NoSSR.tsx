import { ReactNode, useEffect, useState } from "react";

interface IProps {
  children: ReactNode;
}

export function NoSSR({ children }: IProps) {
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    setIsMounted(true);
  }, []);

  return <>{isMounted ? children : null}</>;
}
