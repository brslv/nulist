import dayjs from "dayjs";

export enum DateFormats {
  MonthDayYear,
  ShortMonthDayYearHr,
}

const formats = {
  [DateFormats.MonthDayYear]: "MMMM D, YYYY",
  [DateFormats.ShortMonthDayYearHr]: "MMM D, YYYY h:mm A",
};

export function formatDate(
  date: string,
  format: DateFormats = DateFormats.MonthDayYear
) {
  return dayjs(date).format(formats[format]);
}
