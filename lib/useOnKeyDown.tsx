import { useEffect, useState } from "react";

export function useOnKeyDown<EventType>({
  fn,
  ref,
}: {
  fn: (e: EventType) => void;
  ref?: any;
}) {
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    setIsMounted(true);
  }, []);

  useEffect(() => {
    const isRef = ref !== undefined;

    if (!isMounted || (isRef && (ref === null || !ref.current))) return;

    const root = isRef ? ref.current : window;

    const handler = (e: EventType) => {
      fn(e);
    };

    root.addEventListener("keydown", handler);

    return () => {
      root.removeEventListener("keydown", handler);
    };
  }, [fn, isMounted, ref]);
}
