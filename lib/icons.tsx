import { CgFormatSlash } from "react-icons/cg";
import { AiOutlineEnter } from "react-icons/ai";
import { IoMdFlashlight, IoMdRocket } from "react-icons/io";
import { MdMinimize } from "react-icons/md";
import {
  RiAddLine,
  RiDeleteBinLine,
  RiEditLine,
  RiLogoutBoxLine,
  RiSettings2Line,
  RiUser3Line,
  RiLinksLine,
  RiTwitterLine,
  RiFileTextLine,
  RiYoutubeLine,
  RiFile3Line,
  RiImageLine,
  RiMenuLine,
  RiSave3Line,
  RiFileList2Line,
  RiGroupLine,
  RiArrowRightLine,
  RiCloseLine,
  RiMoreLine,
  RiAtLine,
  RiKeyboardLine,
  RiShapeLine,
  RiHandHeartLine,
  RiHeartLine,
  RiMoneyDollarCircleLine,
  RiShoppingCartLine,
  RiTrophyLine,
  RiArrowUpDownLine,
  RiBookMarkLine,
  RiFolder3Line,
  RiChatQuoteLine,
  RiCheckboxBlankLine,
  RiMap2Line,
  RiLeafLine,
  RiSubtractLine,
  RiQuestionLine,
  RiArrowDownSFill,
  RiArrowUpSFill,
  RiCommandLine,
  RiDashboardLine,
  RiLockLine,
  RiProfileLine,
  RiStackLine,
  RiCalendarLine,
  RiEmotionLine,
  RiInformationLine,
  RiRefreshLine,
  RiStarLine,
  RiCheckLine,
  RiArchiveLine,
  RiHomeLine,
  RiLoginBoxLine,
  RiArrowGoBackLine,
  RiBookOpenLine,
  RiEye2Line,
} from "react-icons/ri";

export const icons = {
  trash: RiDeleteBinLine,
  edit: RiEditLine,
  user: RiUser3Line,
  add: RiAddLine,
  settings: RiSettings2Line,
  logout: RiLogoutBoxLine,
  link: RiLinksLine,
  twitter: RiTwitterLine,
  paragraph: RiFileTextLine,
  youtube: RiYoutubeLine,
  image: RiImageLine,
  file: RiFile3Line,
  menu: RiMenuLine,
  save: RiSave3Line,
  list: RiFileList2Line,
  people: RiGroupLine,
  arrowRight: RiArrowRightLine,
  close: RiCloseLine,
  separator: RiMoreLine,
  atSign: RiAtLine,
  keyboard: RiKeyboardLine,
  shape: RiShapeLine,
  generous: RiHandHeartLine,
  heart: RiHeartLine,
  money: RiMoneyDollarCircleLine,
  groceries: RiShoppingCartLine,
  trophy: RiTrophyLine,
  upDownArrow: RiArrowUpDownLine,
  journal: RiBookMarkLine,
  folder: RiFolder3Line,
  quotes: RiChatQuoteLine,
  checkbox: RiCheckboxBlankLine,
  map: RiMap2Line,
  leaf: RiLeafLine,
  minus: RiSubtractLine,
  questionmark: RiQuestionLine,
  down: RiArrowDownSFill,
  up: RiArrowUpSFill,
  cmd: RiCommandLine,
  forwardSlash: CgFormatSlash,
  enter: AiOutlineEnter,
  dashboard: RiDashboardLine,
  remove: RiDeleteBinLine,
  lock: RiLockLine,
  profile: RiProfileLine,
  stack: RiStackLine,
  email: RiAtLine,
  calendar: RiCalendarLine,
  name: RiEmotionLine,
  info: RiInformationLine,
  refresh: RiRefreshLine,
  star: RiStarLine,
  check: RiCheckLine,
  archive: RiArchiveLine,
  home: RiHomeLine,
  signUp: RiLoginBoxLine,
  flashlight: IoMdFlashlight,
  rocket: IoMdRocket,
  text: RiFileTextLine,
  goBack: RiArrowGoBackLine,
  blog: RiBookOpenLine,
  minimize: MdMinimize,
  bundle: RiStackLine,
  view: RiEye2Line,
};

export type IconKey = keyof typeof icons;
