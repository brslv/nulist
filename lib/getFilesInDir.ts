import fs from "fs";

type Opts = {
  exclude?: string[];
};

export function getFilesInDir(dirPath: string, { exclude = [] }: Opts = {}) {
  const files = fs.readdirSync(dirPath, "utf-8");
  return files.filter(f => !exclude.includes(f));
}
