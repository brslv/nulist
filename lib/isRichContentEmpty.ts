export const isRichContentEmpty = (content = "") => {
  const strippedHtmlTags = content.replace(/<[^>]+>/g, "");
  return strippedHtmlTags.trim() === "";
};
