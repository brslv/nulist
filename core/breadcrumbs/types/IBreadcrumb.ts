export interface IBreadcrumb {
  title: string;
  href: string;
  clickable?: boolean;
}
