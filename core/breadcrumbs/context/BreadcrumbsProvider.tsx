import {
  createContext,
  ReactNode,
  useContext,
  useState,
  Dispatch,
  SetStateAction,
} from "react";
import { IBreadcrumb } from "../types/IBreadcrumb";

interface ICtx {
  breadcrumbs: IBreadcrumb[] | null;
  setBreadcrumbs: Dispatch<SetStateAction<IBreadcrumb[] | null>>;
}

interface IProps {
  children: ReactNode;
}

const BreadcrumbsContext = createContext<ICtx | null>(null);

export function BreadcrumbsProvider({ children }: IProps) {
  const [breadcrumbs, setBreadcrumbs] = useState<IBreadcrumb[] | null>(null);
  const value = {
    breadcrumbs,
    setBreadcrumbs,
  };

  return (
    <BreadcrumbsContext.Provider value={value}>
      {children}
    </BreadcrumbsContext.Provider>
  );
}

export function useBreadcrumbs() {
  const ctx = useContext(BreadcrumbsContext);
  if (!ctx) throw new Error("Improper use of BreadcrumbsProvider");
  return ctx;
}
