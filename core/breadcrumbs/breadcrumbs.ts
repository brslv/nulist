export const breadcrumbs = {
  dashboard: {
    href: "/dashboard",
    title: "Dashboard",
  },
  bundle: {
    href: (id: number) => `/bundle/${id}`,
    title: (title: string) => `Bundle "${title}"`,
  },
  profile: {
    href: "/profile",
    title: "Profile",
  },
};
