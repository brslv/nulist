import { useEffect, useState } from "react";
import { useOnClickOutside } from "../lib/useClickOutside";
import useOnEsc from "../lib/useOnEsc";

interface IProps {
  ref: any;
  isOpen: boolean;
  onClose?: () => void;
  closeOnOutsideClick?: boolean;
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
const noop = () => {};

export function useOverlayInteractions({
  ref,
  onClose,
  isOpen,
  closeOnOutsideClick = true,
}: IProps) {
  const [isMounted, setIsMounted] = useState(false);

  useOnEsc(onClose || noop);
  useOnClickOutside(ref, () => {
    if (isOpen && typeof onClose === "function" && closeOnOutsideClick)
      onClose();
  });

  useEffect(() => setIsMounted(true), []);

  useEffect(() => {
    if (isMounted && isOpen) {
      setTimeout(() => document.body.classList.add("noscroll"), 0);
    }
    if (isMounted && !isOpen) document.body.classList.remove("noscroll");
  }, [isMounted, isOpen]);
}
