import {
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useReducer,
} from "react";
import { BundlesLimitReached } from "../../../components/PlansModal/reasons/BundlesLimitReached";
import { useAuth } from "../../auth/context/AuthProvider";
import { useNotifications } from "../../notifications/context/NotificationsProvider";
import { NotificationTypes } from "../../notifications/types/NotificationTypes";
import { usePlansModal } from "../../plans/context/PlansModalProvider";
import { EventReasons } from "../../plans/EventReasons";
import { usePlanValidators } from "../../plans/usePlanValidators";
import { useAuthenticatedUserProfile } from "../../profile/context/AuthenticatedUserProfileProvider";
import {
  remove,
  save,
  update as updateBundleRequest,
  getBundles as getBundlesRequest,
} from "../bundleService";
import { IBundle } from "../types/IBundle";
import { BundlesManagerProvider } from "./BundlesManagerProvider";

interface IProps {
  children: ReactNode;
}

interface ICtx {
  bundles: IState["bundles"];
  loading: IState["loading"];
  initiallyLoaded: IState["initiallyLoaded"];
  isPlansModalOpen: boolean;
  addBundle: ({ title, isPublic }: Partial<IBundle>) => Promise<IBundle | void>;
  updateBundle: (
    data: Pick<IBundle, "id"> & Partial<IBundle>
  ) => Promise<IBundle | void>;
  getBundles: () => Promise<void>;
  removeBundle: (id?: IBundle["id"] | null) => Promise<void>;
}

const BundlesContext = createContext<ICtx | null>(null);

interface IState {
  bundles: IBundle[];
  loading: boolean;
  initiallyLoaded: boolean;
}

enum Actions {
  StartGettingBundles,
  FailGettingBundles,
  DoneGettingBundles,
  StartAddingBundle,
  FailAddingBundle,
  DoneAddingBundle,
  StartUpdatingBundle,
  FailUpdatingBundle,
  DoneUpdatingBundle,
  StartRemovingBundle,
  FailRemovingBundle,
  DoneRemovingBundle,
  UserLoggedOut,
}

type Action =
  | {
      type: Actions.StartGettingBundles;
    }
  | {
      type: Actions.FailGettingBundles;
    }
  | {
      type: Actions.DoneGettingBundles;
      payload: IBundle[];
    }
  | {
      type: Actions.StartAddingBundle;
    }
  | {
      type: Actions.FailAddingBundle;
    }
  | {
      type: Actions.DoneAddingBundle;
      payload: IBundle;
    }
  | {
      type: Actions.StartUpdatingBundle;
    }
  | {
      type: Actions.FailUpdatingBundle;
    }
  | {
      type: Actions.DoneUpdatingBundle;
      payload: Pick<IBundle, "id"> & Partial<IBundle>;
    }
  | {
      type: Actions.StartRemovingBundle;
    }
  | {
      type: Actions.FailRemovingBundle;
    }
  | {
      type: Actions.DoneRemovingBundle;
      payload: IBundle["id"];
    }
  | {
      type: Actions.UserLoggedOut;
    };

const reducer = (state: IState, action: Action): IState => {
  switch (action.type) {
    case Actions.StartGettingBundles:
      return {
        ...state,
        loading: true,
      };
    case Actions.DoneGettingBundles:
      return {
        ...state,
        loading: false,
        bundles: action.payload,
        initiallyLoaded: true,
      };
    case Actions.StartAddingBundle:
      return {
        ...state,
        loading: true,
      };
    case Actions.DoneAddingBundle:
      return {
        ...state,
        loading: false,
        bundles: [...state.bundles, action.payload],
      };
    case Actions.StartRemovingBundle:
      return {
        ...state,
        loading: true,
      };
    case Actions.DoneRemovingBundle: {
      const idToBeRemoved = action.payload;
      return {
        ...state,
        bundles: state.bundles.filter(b => b.id !== idToBeRemoved),
        loading: false,
      };
    }
    case Actions.StartUpdatingBundle: {
      return {
        ...state,
        loading: true,
      };
    }
    case Actions.DoneUpdatingBundle: {
      return {
        ...state,
        bundles: state.bundles.map(bundle => {
          if (bundle.id === action.payload.id) {
            return { ...bundle, ...action.payload };
          }
          return bundle;
        }),
        loading: false,
      };
    }
    // Fails
    case Actions.FailGettingBundles:
    case Actions.FailAddingBundle:
    case Actions.FailRemovingBundle:
    case Actions.FailUpdatingBundle:
      return {
        ...state,
        loading: false,
      };
    case Actions.UserLoggedOut:
      return initialState;
    default:
      return state;
  }
};

const initialState: IState = {
  bundles: [],
  loading: false,
  initiallyLoaded: false,
};

export function BundlesProvider({ children }: IProps) {
  const { user, loading: loadingUser } = useAuth();
  const { addNotification } = useNotifications();
  const { profile } = useAuthenticatedUserProfile();
  const [state, dispatch] = useReducer(reducer, initialState);
  const { open: openPlansModal, isOpen } = usePlansModal();
  const { isExceedingFreeBundlesCount } = usePlanValidators();

  useEffect(() => {
    if (!user && !loadingUser) {
      dispatch({ type: Actions.UserLoggedOut });
    }
  }, [loadingUser, user]);

  const getBundles: ICtx["getBundles"] = useCallback(async () => {
    if (!profile) return;

    dispatch({ type: Actions.StartGettingBundles });

    const { error, data } = await getBundlesRequest({ profileId: profile.id });

    if (error) {
      dispatch({ type: Actions.FailGettingBundles });
      console.warn("Failed to fetch bundles.");
      console.error(error.message);
      return;
    }

    dispatch({ type: Actions.DoneGettingBundles, payload: data || [] });
  }, [profile]);

  useEffect(() => {
    getBundles();
  }, [getBundles]);

  const addBundle: ICtx["addBundle"] = async ({ title, isPublic }) => {
    if (title === "") return;

    if (isExceedingFreeBundlesCount(state.bundles)) {
      openPlansModal({
        reason: <BundlesLimitReached />,
        eventReason: EventReasons.BundlesLimitReached,
      });
      return;
    }

    dispatch({ type: Actions.StartAddingBundle });

    const { error, data } = await save({
      title,
      isPublic,
      userUid: user?.id,
      profileId: profile?.id,
    });

    if (error || !data || !data.length) {
      dispatch({ type: Actions.FailAddingBundle });
      addNotification({
        type: NotificationTypes.Error,
        content: error?.message || "Failed to create bundle.",
      });
      return;
    }

    const bundleData = data[0];

    dispatch({
      type: Actions.DoneAddingBundle,
      payload: bundleData,
    });

    addNotification({
      type: NotificationTypes.Success,
      hasCloseBtn: false,
      content: `Bundle "${bundleData.title}" created.`,
    });

    return bundleData;
  };

  const updateBundle = async (data: Pick<IBundle, "id"> & Partial<IBundle>) => {
    dispatch({ type: Actions.StartUpdatingBundle });

    const { error, data: response } = await updateBundleRequest({ ...data });

    if (error || !response || !response.length) {
      dispatch({ type: Actions.FailAddingBundle });
      addNotification({
        type: NotificationTypes.Error,
        content: error?.message || "Failed to create bundle.",
      });
      return;
    }

    const bundleData = response[0];

    dispatch({
      type: Actions.DoneUpdatingBundle,
      payload: bundleData,
    });

    addNotification({
      type: NotificationTypes.Success,
      hasCloseBtn: false,
      content: `Bundle "${bundleData.title}" updated.`,
    });

    return bundleData;
  };

  const removeBundle = async (id?: IBundle["id"] | null) => {
    if (!id) return;

    dispatch({ type: Actions.StartRemovingBundle });

    const { error } = await remove(id);

    if (error) {
      dispatch({ type: Actions.FailRemovingBundle });
      addNotification({
        type: NotificationTypes.Error,
        content: error?.message || "Failed to remove bundle.",
      });
      return;
    }

    dispatch({
      type: Actions.DoneRemovingBundle,
      payload: id,
    });

    addNotification({
      type: NotificationTypes.Success,
      hasCloseBtn: false,
      content: `Bundle deleted.`,
    });
  };

  return (
    <BundlesContext.Provider
      value={{
        bundles: state.bundles,
        loading: state.loading,
        initiallyLoaded: state.initiallyLoaded,
        isPlansModalOpen: isOpen,
        getBundles,
        addBundle,
        updateBundle,
        removeBundle,
      }}
    >
      <BundlesManagerProvider>{children}</BundlesManagerProvider>
    </BundlesContext.Provider>
  );
}

export function useBundles() {
  const ctx = useContext(BundlesContext);
  if (!ctx) throw new Error("Improper use of BundlesProvider.");
  return ctx;
}
