import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from "react";
import { useAuth } from "../../auth/context/AuthProvider";
import { useListEditor } from "../../list/context/ListEditorProvider";
import { IBundle } from "../types/IBundle";

interface IProps {
  children: ReactNode;
}

interface IState {
  selectedBundles: Array<IBundle["id"]>;
}

interface ICtx {
  selectedBundles: Array<IBundle["id"]>;
  updateBundles: () => void;
  select: (bundle: IBundle) => void;
  deselect: (bundle: IBundle) => void;
  toggle: (bundle: IBundle) => void;
  isSelected: (bundle: IBundle) => boolean;
}

const ListBundlesContext = createContext<ICtx | null>(null);

const initialState = { selectedBundles: [] };
export function ListBundlesProvider({ children }: IProps) {
  const [state, setState] = useState<IState>(initialState);
  const { user, loading: loadingUser } = useAuth();
  const {
    state: { bundles },
    setBundles,
  } = useListEditor();

  useEffect(() => {
    if (!user && !loadingUser) {
      setState(initialState);
    }
  }, [loadingUser, user]);

  // Make sure that the list editor bundles are in sync with the ListBundlesProvider.selectedBundles
  useEffect(() => {
    setState({ selectedBundles: bundles });
  }, [bundles]);

  const select = (bundle: IBundle) =>
    setState(prev => ({
      ...prev,
      selectedBundles: [...prev.selectedBundles, bundle.id],
    }));

  const deselect = (bundle: IBundle) =>
    setState(prev => ({
      selectedBundles: prev.selectedBundles.filter(id => id !== bundle.id),
    }));

  const toggle = (bundle: IBundle) => {
    if (isSelected(bundle)) deselect(bundle);
    else select(bundle);
  };

  const isSelected = (bundle: IBundle) =>
    state.selectedBundles.some(selectedId => selectedId === bundle.id);

  const updateBundles = () => setBundles(state.selectedBundles.map(id => id));

  return (
    <ListBundlesContext.Provider
      value={{
        selectedBundles: state.selectedBundles,
        updateBundles,
        select,
        deselect,
        toggle,
        isSelected,
      }}
    >
      {children}
    </ListBundlesContext.Provider>
  );
}

export function useListBundles() {
  const ctx = useContext(ListBundlesContext);
  if (!ctx) throw new Error("Improper use of ListBundlesProvider.");
  return ctx;
}
