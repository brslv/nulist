import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from "react";
import { IBundle } from "../types/IBundle";
import { useBundles } from "./BundlesProvider";

export enum BundleFormMode {
  Create,
  Edit,
}

interface ICtx {
  mode: BundleFormMode;
  isOpen: boolean;
  errors: IErrors;
  hasErrors: boolean;
  state: IState;
  setState: (arg0: IState) => void;
  open: (bundle?: Pick<IBundle, "id"> & Partial<IBundle>) => void;
  close: () => void;
  onSubmit: () => Promise<void | IBundle>;
}

interface IErrors {
  title: string | null;
}

interface IProps {
  children: ReactNode;
}

const initialErrors = {
  title: null,
};

const initialState = { title: "", isPublic: false };

const BundleFormContext = createContext<ICtx | null>(null);

interface IState {
  title: string;
  isPublic: boolean;
}

export function BundleFormProvider({ children }: IProps) {
  const { addBundle, updateBundle } = useBundles();
  const [state, setState] = useState<IState>(initialState);
  const [errors, setErrors] = useState<IErrors>(initialErrors);
  const [data, setData] = useState<Pick<IBundle, "id"> & Partial<IBundle>>();
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    setState(prev => ({
      ...prev,
      ...(data || initialState),
    }));
  }, [data]);

  const open = (data?: Pick<IBundle, "id"> & Partial<IBundle>) => {
    setIsOpen(true);
    setData(data);
  };
  const close = () => {
    setIsOpen(false);
    setData(undefined);
  };

  const onSubmit: ICtx["onSubmit"] = async () => {
    const { title, isPublic } = state;
    const _errors = validate({ title, isPublic });
    setErrors(_errors);
    if (Object.values(_errors).some(error => error !== null)) {
      return Promise.reject();
    }

    let bundle;
    if (data && data.id) {
      // perform update
      bundle = await updateBundle({ id: data.id, ...state });
    } else {
      bundle = await addBundle({ title, isPublic });
    }

    return bundle;
  };

  const validate: (arg0: { title: string; isPublic?: boolean }) => IErrors = ({
    title,
  }) => {
    const _errors: IErrors = { ...initialErrors };

    if (!title.length) {
      _errors.title = "Title cannot be empty.";
    }

    if (title.length > 40) {
      _errors.title = "Title is too long (40 symbols max).";
    }

    return _errors;
  };

  const value = {
    mode: !!data ? BundleFormMode.Edit : BundleFormMode.Create,
    isOpen,
    errors,
    hasErrors: Object.values(errors).some(value => value !== null),
    state,
    setState,
    open,
    close,
    onSubmit,
  };

  return (
    <BundleFormContext.Provider value={value}>
      {children}
    </BundleFormContext.Provider>
  );
}

export function useBundleForm() {
  const ctx = useContext(BundleFormContext);
  if (!ctx) throw new Error("Improper use of BundleFormProvider");
  return ctx;
}
