import { createContext, ReactNode, useContext, useState } from "react";

interface IProps {
  children: ReactNode;
}

interface ICtx {
  isOpen: boolean;
  open: () => void;
  close: () => void;
  toggle: () => void;
}

const BundlesManagerContext = createContext<ICtx | null>(null);

export function BundlesManagerProvider({ children }: IProps) {
  const [isOpen, setIsOpen] = useState(false);

  const open = () => setIsOpen(true);
  const close = () => setIsOpen(false);
  const toggle = () => setIsOpen(prev => !prev);

  return (
    <BundlesManagerContext.Provider value={{ isOpen, open, close, toggle }}>
      {children}
    </BundlesManagerContext.Provider>
  );
}

export function useBundlesManager() {
  const ctx = useContext(BundlesManagerContext);
  if (!ctx) throw new Error("Improper use of BundlesManagerProvider.");
  return ctx;
}
