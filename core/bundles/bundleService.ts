import { supabase } from "../../lib/supabaseClient";
import { IBundle } from "./types/IBundle";

export async function getBundles({ profileId }: { profileId: string }) {
  return supabase
    .from<IBundle>("bundles")
    .select(`*, lists ( * )`)
    .eq("profileId", profileId)
    .order("id", { ascending: true });
}

export async function getBundle({ id }: { id: number }) {
  return supabase
    .from<IBundle>("bundles")
    .select(
      `
      *,
      lists (
        *,
        bundles ( * )
      )
    `
    )
    .eq("id", id);
}

export async function save(bundle: Partial<IBundle>) {
  return supabase.from("bundles").insert(bundle);
}

export async function update(bundle: Pick<IBundle, "id"> & Partial<IBundle>) {
  return supabase
    .from("bundles")
    .update({
      title: bundle.title,
      isPublic: bundle.isPublic,
    })
    .match({ id: bundle.id });
}

export async function remove(id: IBundle["id"]) {
  return supabase.from("bundles").delete().eq("id", id);
}
