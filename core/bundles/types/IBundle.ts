import { IList } from "../../list/types/IList";

export interface IBundle {
  id: number;
  title: string;
  isPublic: boolean;
  userUid: string;
  profileId: string;
  createdAt: string;
  updatedAt: string;
  lists?: IList[];
}
