import path from "path";
import { getAllArticles } from "../blog/utils/getAllArticles";
import { ROOT_URL } from "../config";

/**
 * These are pages to be included in the sitemap.
 */
export const pages = ["/", "auth", "blog"].map(page =>
  `${ROOT_URL}/${page}`.replace(/\/$/g, "")
);

/**
 * Resolve all blog articles to be included in the sitemap.
 */
export const blogArticles = getAllArticles({
  blogDirPath: path.resolve(process.cwd(), "./pages/blog"),
});
