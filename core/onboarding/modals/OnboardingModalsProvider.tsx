import React, { createContext, ReactNode, useContext, useState } from "react";
import { ClientEvents } from "../../clientEvents/ClientEvents";
import { fireClientEvent } from "../../clientEvents/fireClientEvent";

export enum OnboardingModals {
  Welcome,
  Help,
  Feedback,
  Waitlist,
}

interface ICtx {
  isOpen: (modal: OnboardingModals) => boolean;
  open: (modal: OnboardingModals) => void;
  close: () => void;
}

interface IProps {
  children: ReactNode;
}

const OnboardingModalsContext = createContext<ICtx | null>(null);

const initialState = {
  [OnboardingModals.Welcome]: false,
  [OnboardingModals.Help]: false,
  [OnboardingModals.Feedback]: false,
  [OnboardingModals.Waitlist]: false,
};

export function OnboardingModalsProvider({ children }: IProps) {
  const [openState, setOpenState] = useState(initialState);

  const isOpen: ICtx["isOpen"] = modal => !!openState[modal];
  const open: ICtx["open"] = modal => {
    setOpenState(() => ({ ...initialState, [modal]: true }));

    // Fire client events
    if (modal === OnboardingModals.Help)
      fireClientEvent(ClientEvents.HelpModalOpen);
    if (modal === OnboardingModals.Feedback)
      fireClientEvent(ClientEvents.FeedbackModalOpen);
    if (modal === OnboardingModals.Waitlist)
      fireClientEvent(ClientEvents.WaitlistModalOpen);
  };
  const close: ICtx["close"] = () => setOpenState(initialState);

  return (
    <OnboardingModalsContext.Provider
      value={{
        isOpen,
        open,
        close,
      }}
    >
      {children}
    </OnboardingModalsContext.Provider>
  );
}

export function useOnboardingModals() {
  const ctx = useContext(OnboardingModalsContext);
  if (!ctx) throw new Error("Improper use of OnboardingModalsProvider.");
  return ctx;
}
