import React, {
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useState,
} from "react";
import { ClientEvents } from "../../clientEvents/ClientEvents";
import { fireClientEvent } from "../../clientEvents/fireClientEvent";
import { EventReasons } from "../EventReasons";

interface IProps {
  children: ReactNode;
}

interface ICtx {
  isOpen: boolean;
  title: ReactNode | null;
  content: ReactNode | null;
  reason: ReactNode | null;
  setTitle: (arg0: ReactNode | null) => void;
  setContent: (arg0: ReactNode | null) => void;
  open: (arg0?: {
    content?: ICtx["content"];
    title?: ICtx["title"];
    reason?: ICtx["reason"];
    eventReason: EventReasons;
  }) => void;
  close: () => void;
}

const PlansModalContext = createContext<ICtx | null>(null);

export function PlansModalProvider({ children }: IProps) {
  const [isOpen, setIsOpen] = useState(false);
  const [title, setTitle] = useState<ReactNode | null>(null);
  const [content, setContent] = useState<ReactNode | null>(null);
  const [reason, setReason] = useState<ReactNode | null>(null);
  const open = useCallback(
    (
      {
        content,
        title,
        reason,
        eventReason,
      }: {
        content?: ICtx["content"];
        title?: ICtx["title"];
        reason?: ICtx["reason"];
        eventReason: EventReasons;
      } = {
        title: undefined,
        content: undefined,
        eventReason: EventReasons.Unknown,
      }
    ) => {
      setIsOpen(true);

      if (title !== undefined) setTitle(title);
      if (content !== undefined) setContent(content);
      if (reason !== undefined) setReason(reason);

      fireClientEvent(ClientEvents.UpgradeModalOpen, {
        reason: eventReason,
      });
    },
    []
  );

  const close = () => {
    setIsOpen(false);
    setContent(null);
    setReason(null);
  };

  return (
    <PlansModalContext.Provider
      value={{
        isOpen,
        title,
        content,
        reason,
        setTitle,
        setContent,
        open,
        close,
      }}
    >
      {children}
    </PlansModalContext.Provider>
  );
}

export function usePlansModal() {
  const ctx = useContext(PlansModalContext);
  if (!ctx) throw new Error("Improper use of PlanConstraintModalProvider.");
  return ctx;
}
