export const CONSTRAINTS = {
  FREE_PLAN: {
    MAX_LISTS: 30,
    MAX_LIST_ITEMS: 20,
    MAX_BUNDLES: 30,
  },
};
