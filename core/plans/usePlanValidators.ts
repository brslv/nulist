import { useCallback } from "react";
import { IBundle } from "../bundles/types/IBundle";
import { IList } from "../list/types/IList";
import { IListItem } from "../list/types/IListItem";
import { useAuthenticatedUserProfile } from "../profile/context/AuthenticatedUserProfileProvider";
import { CONSTRAINTS } from "./constraints";

export function usePlanValidators() {
  const { profile } = useAuthenticatedUserProfile();
  const isPro = profile && profile.isPro;

  const isExceedingFreeListsCount = useCallback(
    (lists: IList[]) => {
      if (!isPro) {
        if (lists.length >= CONSTRAINTS.FREE_PLAN.MAX_LISTS) {
          return true;
        }
      }

      return false;
    },
    [isPro]
  );

  const isExceedingFreeListItemsCount = useCallback(
    (listItems: IListItem[]) => {
      if (!isPro) {
        if (listItems.length >= CONSTRAINTS.FREE_PLAN.MAX_LIST_ITEMS) {
          return true;
        }
      }

      return false;
    },
    [isPro]
  );

  const isExceedingFreeBundlesCount = useCallback(
    (bundles: IBundle[]) => {
      if (!isPro) {
        if (bundles.length >= CONSTRAINTS.FREE_PLAN.MAX_BUNDLES) {
          return true;
        }
      }

      return false;
    },
    [isPro]
  );

  return {
    isExceedingFreeListsCount,
    isExceedingFreeListItemsCount,
    isExceedingFreeBundlesCount,
  };
}
