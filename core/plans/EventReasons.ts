export enum EventReasons {
  Unknown = "Unknown",
  UserInitiated = "UserInitiated",
  ListsLimitReached = "ListsLimitReached",
  ListItemsLimitReached = "ListItemsLimitReached",
  BundlesLimitReached = "BundlesLimitReached",
}
