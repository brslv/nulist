export const ROOT_URL = (
  process.env.NEXT_PUBLIC_ROOT_URL ||
  process.env.NEXT_PUBLIC_VERCEL_URL ||
  "http://localhost:3000"
).replace(/\/$/, ""); // remove trailing slashes, if any
