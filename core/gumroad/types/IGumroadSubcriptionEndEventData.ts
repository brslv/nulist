export interface IGumroadSubcriptionEndEventData {
  email: string;
  [key: string]: any;
}
