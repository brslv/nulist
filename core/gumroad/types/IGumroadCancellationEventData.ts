export interface IGumroadCancellationEventData {
  subscription_id: string;
  product_id: string;
  product_name: string;
  user_id: string;
  user_email: string;
  "purchase_ids[]": string;
  created_at: string;
  charge_occurrence_count: string;
  recurrence: string;
  free_trial_ends_at: string;
  resource_name: string;
  cancelled: string;
  cancelled_at: string;
  cancelled_by_buyer: string;
  [key: string]: any;
}
