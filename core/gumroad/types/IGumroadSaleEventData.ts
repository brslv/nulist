export interface IGumroadSaleEventData {
  email: string;
  [key: string]: any;
}
