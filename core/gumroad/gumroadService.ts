import { supabase } from "../../lib/supabaseClient";
import { IGumroadCancellationEventData } from "./types/IGumroadCancellationEventData";
import { IGumroadSaleEventData } from "./types/IGumroadSaleEventData";
import { IGumroadSubcriptionEndEventData } from "./types/IGumroadSubcriptionEndEventData";

export async function goPro(
  email: string,
  gumroadEventData: IGumroadSaleEventData
) {
  const { data: gumroadEventId, error } = await supabase.rpc("goPro", {
    emailInput: email,
    gumroadEventData,
  });

  if (error) throw error;

  return gumroadEventId;
}

export async function cancelPro(
  email: string,
  gumroadEventData: IGumroadCancellationEventData
) {
  const { data: gumroadEventId, error } = await supabase.rpc("cancelPro", {
    emailInput: email,
    gumroadEventData,
  });

  if (error) throw error;

  return gumroadEventId;
}

export async function endSubscription(
  email: string,
  gumroadEventData: IGumroadSubcriptionEndEventData
) {
  const { data: gumroadEventId, error } = await supabase.rpc("endPro", {
    emailInput: email,
    gumroadEventData,
  });

  if (error) throw error;

  return gumroadEventId;
}
