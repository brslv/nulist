import { supabase } from "../../lib/supabaseClient";
import { cancelPro, endSubscription, goPro } from "./gumroadService";
import { IGumroadCancellationEventData } from "./types/IGumroadCancellationEventData";
import { IGumroadSaleEventData } from "./types/IGumroadSaleEventData";
import { IGumroadSubcriptionEndEventData } from "./types/IGumroadSubcriptionEndEventData";

export async function handleSale(data: IGumroadSaleEventData) {
  const { email } = data;

  return goPro(email, data);
}

export async function handleCancellation(data: IGumroadCancellationEventData) {
  const { user_email: email } = data;

  return cancelPro(email, data);
}

export async function handleOtherEvents(data: IGumroadSaleEventData) {
  const { data: insertResult, error } = await supabase
    .from("gumroadEvents")
    .insert({ data });

  if (error) throw error;

  return insertResult;
}

export async function handleSubscriptionEnd(
  data: IGumroadSubcriptionEndEventData
) {
  const { email } = data;

  return endSubscription(email, data);
}
