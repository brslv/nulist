import { ClientEvents } from "./ClientEvents";

export function fireClientEvent(
  event: ClientEvents,
  props?: { [key: string]: string }
) {
  console.log("client evt:", { event, props });
  if (!window.plausible) {
    console.warn("Missing window.plausible function.");
    return;
  }

  window.plausible(event, props);
}
