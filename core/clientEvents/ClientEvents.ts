export enum ClientEvents {
  // modals
  FeedbackModalOpen = "FeedbackModalOpen",
  WaitlistModalOpen = "WaitlistModalOpen",
  HelpModalOpen = "HelpModalOpen",

  // upgrade modal
  UpgradeModalOpen = "UpgradeModalOpen",
  UpgradeProClick = "UpgradeProClick",

  // landing page
  LandingOpenListEditorBtnClick = "LandingOpenListEditorBtnClick",

  // list actions
  CreateListBtnClick = "CreateListBtnClick",
  CopyShareableLinkBtnClick = "CopyShareableLinkBtnClick",
  ListCreated = "ListCreated",
  ListUpdated = "ListUpdated",
  ListDeleted = "ListDeleted",
}
