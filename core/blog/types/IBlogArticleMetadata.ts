export interface IBlogArticleMetadata {
  title: string;
  description: string;
  date: string;
  image: {
    src: string;
    alt: string;
    coverPosition?: "top" | "center" | "bottom";
    coverPositionInArticle?: "top" | "center" | "bottom";
  };
}
