import { IBlogArticleMetadata } from "./IBlogArticleMetadata";

export interface IBlogArticle {
  slug: string;
  metadata: IBlogArticleMetadata;
}
