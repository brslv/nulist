import React, { ReactNode } from "react";
import { BlogArticleLayout } from "../../components/blog/BlogArticleLayout/BlogArticleLayout";
import { IBlogArticleMetadata } from "./types/IBlogArticleMetadata";

interface IProps {
  children: ReactNode;
}

export const withLayout =
  (metadata: IBlogArticleMetadata) =>
  ({ children }: IProps) => {
    return (
      <BlogArticleLayout metadata={metadata}>{children}</BlogArticleLayout>
    );
  };
