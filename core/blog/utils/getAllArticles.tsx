import fs from "fs";
import path from "path";
import matter from "gray-matter";
import { getFilesInDir } from "../../../lib/getFilesInDir";
import { IBlogArticle } from "../types/IBlogArticle";
import { IBlogArticleMetadata } from "../types/IBlogArticleMetadata";

const getMetaData = (filePath: string): IBlogArticleMetadata => {
  const fileContents = fs.readFileSync(filePath, "utf-8");
  const { data: metadata } = matter(fileContents);
  return metadata as IBlogArticleMetadata;
};

export function getAllArticles(
  {
    blogDirPath = "./pages/blog",
  }: {
    blogDirPath?: string;
  } = {
    blogDirPath: "./pages/blog",
  }
) {
  // Get all the articles under /pages/blog (exclude index.tsx, the blog's homepage)
  const exclude = ["index.tsx"];
  const files = getFilesInDir(blogDirPath, {
    exclude,
  });

  // Gather article's metadata + slug from the .mdx files
  const articles: IBlogArticle[] = [];
  files.forEach((file: string) => {
    const metadata = getMetaData(path.join(blogDirPath, file));
    const cleanFileName = file.replace(/\.md(x)?/gi, "");
    const slug = `/blog/${cleanFileName}`;
    const article = { metadata: metadata, slug };

    articles.push(article);
  });

  return articles;
}
