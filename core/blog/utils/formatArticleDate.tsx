import dayjs from "dayjs";
import customParseFormat from "dayjs/plugin/customParseFormat";

type FromFormat = string | string[];
type ToFormat = string;

export function formatArticleDate(
  date: string,
  fromFormat: FromFormat = "YYYY-MM-DD",
  toFormat: ToFormat = "D MMMM, YYYY"
) {
  dayjs.extend(customParseFormat);
  return dayjs(date, fromFormat).format(toFormat);
}
