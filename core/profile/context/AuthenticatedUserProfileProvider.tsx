import {
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import { getProfile, updateProfile } from "../profileService";
import { useAuth } from "../../auth/context/AuthProvider";
import { IUserProfile } from "../../auth/types/IUserProfile";
import { useNotifications } from "../../notifications/context/NotificationsProvider";
import { NotificationTypes } from "../../notifications/types/NotificationTypes";

interface IProps {
  children: ReactNode;
}

interface ICtx {
  profile: IUserProfile | null;
  update: (newProfile: Partial<IUserProfile>) => Promise<void>;
}

const AuthenticatedUserProfileContext = createContext<ICtx | null>(null);

export function AuthenticatedUserProfileProvider({ children }: IProps) {
  const { user, loading } = useAuth();
  const { addNotification } = useNotifications();
  const [profile, setProfile] = useState(null);

  const loadProfile = useCallback(async (id: IUserProfile["id"]) => {
    const { data, error } = await getProfile(id);
    if (error) throw error;
    if (data && data.length) {
      setProfile(data[0]);
    }
  }, []);

  const update: ICtx["update"] = async newProfile => {
    try {
      if (!profile) throw new Error("No profile available.");

      const { error } = await updateProfile({
        ...(profile as IUserProfile),
        ...newProfile,
      });

      if (error) {
        return addNotification({
          type: NotificationTypes.Error,
          content: error.message,
        });
      }

      if (user?.id) await loadProfile(user?.id);

      addNotification({
        type: NotificationTypes.Success,
        content: "Profile updated.",
      });
    } catch (error: any) {
      addNotification({
        type: NotificationTypes.Error,
        content: error.message,
      });
    }
  };

  useEffect(() => {
    if (user && !loading) {
      loadProfile(user.id);
    }
  }, [loadProfile, loading, user]);

  const value = {
    profile,
    update,
  };

  return (
    <AuthenticatedUserProfileContext.Provider value={value}>
      {children}
    </AuthenticatedUserProfileContext.Provider>
  );
}

export function useAuthenticatedUserProfile() {
  const ctx = useContext(AuthenticatedUserProfileContext);
  if (!ctx) throw new Error("Improper use of AuthenticatedUserProfileProvider");
  return ctx;
}
