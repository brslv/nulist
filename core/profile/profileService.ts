import { supabase } from "../../lib/supabaseClient";
import { IUserProfile } from "../auth/types/IUserProfile";

export function getProfile(id: IUserProfile["id"]) {
  return supabase.from("profiles").select("*").filter("id", "eq", id).limit(1);
}

export function updateProfile(profile: Partial<IUserProfile>) {
  if (!profile.id) throw new Error("No profile.id provided.");
  return supabase.from("profiles").update(profile).eq("id", profile.id);
}
