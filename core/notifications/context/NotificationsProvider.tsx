import {
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useState,
} from "react";
import { INotification } from "../types/INotification";

interface ICtx {
  notifications: INotification[];
  addNotification: (
    notification: Omit<INotification, "id"> & Partial<Pick<INotification, "id">>
  ) => void;
  removeNotification: (notificationId: INotification["id"]) => void;
}
interface IProps {
  children: ReactNode;
}

const NotificationsContext = createContext<ICtx | null>(null);

export function NotificationsProvider({ children }: IProps) {
  const [notifications, setNotifications] = useState<INotification[]>([]);

  const addNotification: ICtx["addNotification"] = useCallback(notification => {
    const notificationWithId: INotification = {
      hasCloseBtn: true,
      ...notification,
      id: Math.random(),
    };

    setNotifications(prev => [notificationWithId, ...prev]);
  }, []);

  const removeNotification: ICtx["removeNotification"] = useCallback(id => {
    setNotifications(prev => prev.filter(n => n.id !== id));
  }, []);

  return (
    <NotificationsContext.Provider
      value={{
        notifications,
        addNotification,
        removeNotification,
      }}
    >
      {children}
    </NotificationsContext.Provider>
  );
}

export function useNotifications() {
  const ctx = useContext(NotificationsContext);
  if (!ctx) throw new Error("Improper use of NotificationsProvider.");
  return ctx;
}
