import { ReactNode } from "react";
import { NotificationTypes } from "./NotificationTypes";

export interface INotification {
  id: number;
  content: ReactNode;
  type: NotificationTypes;
  autoClose?: number;
  hasCloseBtn?: boolean;
}
