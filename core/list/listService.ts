import { supabase } from "../../lib/supabaseClient";
import { IUser } from "../auth/types/IUser";
import { IBundle } from "../bundles/types/IBundle";
import { IList } from "./types/IList";
import { IListItem } from "./types/IListItem";
import { ListVisibilities } from "./types/ListVisibilities";

export async function save(
  list: Partial<IList>,
  listItems: IListItem[],
  bundles: Array<IBundle["id"]>
) {
  return supabase.rpc("addListWithItems", {
    list,
    listItems: listItems.map(item => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { id, ...exceptId } = item;
      return exceptId;
    }),
    bundleIds: bundles,
  });
}

export async function update(
  list: Partial<IList>,
  listItems: IListItem[],
  bundles: Array<IBundle["id"]>
) {
  return supabase.rpc("updateListWithItems", {
    list,
    listItems: listItems.map(item => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { id, ...exceptId } = item;
      return exceptId;
    }),
    bundleIds: bundles,
  });
}

export async function getList(criteria: {
  id?: IList["id"];
  uid?: IList["uid"];
}): Promise<{ list: IList | null }> {
  const query = supabase.from("lists").select(`
    *,
    author:profileId (
      id, name, email, isPro
    ),
    listItems (
      *
    ),
    bundles (
      id, title, isPublic
    )
  `);
  if (criteria.id) query.filter("id", "eq", criteria.id);
  if (criteria.uid) query.filter("uid", "eq", criteria.uid);
  const { data, error } = await query;

  if (error) {
    throw error;
  }

  if (!data || !data.length) return { list: null };

  const list = data[0] as IList;

  return {
    list,
  };
}

export async function getAllListsForUser(
  user: IUser,
  { visibility }: { visibility: ListVisibilities }
) {
  const query = supabase
    .from("lists")
    .select(
      `
      *,
      author:profileId (
        id, name, email, isPro
      ),
      listItems (
        *
      ),
      bundles (
        id, title, isPublic
      )
    `
    )
    .filter("userUid", "eq", user.id);

  if (visibility === ListVisibilities.Private) {
    query.filter("isPublic", "eq", false);
  } else if (visibility === ListVisibilities.Public) {
    query.filter("isPublic", "eq", true);
  }

  query.order("createdAt", { ascending: false });

  const { data, error } = await query;

  if (error) {
    throw error;
  }

  if (!data) return null;

  return data as IList[];
}

export async function removeList(id: IList["id"]) {
  return supabase.from("lists").delete().eq("id", id);
}
