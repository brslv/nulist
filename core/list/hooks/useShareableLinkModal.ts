import { useState } from "react";
import { ClientEvents } from "../../clientEvents/ClientEvents";
import { fireClientEvent } from "../../clientEvents/fireClientEvent";
import { ROOT_URL } from "../../config";

interface IProps {
  listUid?: string;
}

export function useShareableLinkModal({ listUid }: IProps) {
  const [isOpen, setIsOpen] = useState(false);
  const [isCopied, setIsCopied] = useState(false);
  const listUrl = `${ROOT_URL}/list/${listUid}`;

  const open = () => {
    setIsOpen(true);
    setIsCopied(false);
  };

  const close = () => {
    setIsOpen(false);
    setIsCopied(false);
  };

  const copy = () => {
    setIsCopied(true);
    fireClientEvent(ClientEvents.CopyShareableLinkBtnClick);
  };

  return {
    isOpen,
    isCopied,
    listUrl,
    open,
    close,
    copy,
  };
}
