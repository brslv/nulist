import { useState, useCallback, useReducer } from "react";
import { ListItemTypes } from "../../../components/ListEditor/ListItemTypeContextMenu/types/IListItemType";
import { AddItemFnParam } from "../types/AddItemFnParam";
import { IEmoji } from "../types/IEmoji";
import { IListEditorErrors } from "../types/IListEditorErrors";
import { IListEditorProviderState } from "../types/IListEditorProviderState";
import { IListItem } from "../types/IListItem";
import { reorder } from "../utils/reorder";
import { IListEditorState } from "../types/IListEditorState";
import { usePlanValidators } from "../../plans/usePlanValidators";
import { usePlansModal } from "../../plans/context/PlansModalProvider";
import { ListItemsLimitReached } from "../../../components/PlansModal/reasons/ListItemsLimitReached";
import { EventReasons } from "../../plans/EventReasons";

const initialState: IListEditorState = {
  id: 0,
  uid: "",
  isPublic: false,
  title: "",
  description: "",
  emoji: {
    emoji: "😊",
    name: "smiling face",
  },
  items: [],
  bundles: [],
  addingItem: true,
  focusedItem: null,
  editingItem: null,
  managingProperties: null,
  managingItemType: null,
};

enum Actions {
  SetIsPublic,
  SetTitle,
  SetDescription,
  SetEmoji,

  SetItems,
  AddItem,
  UpdateItem,
  RemoveItem,
  StartAddingItem,
  StartEditingItem,
  CancelAddingItem,

  SetFocusedItem,
  SetEditingItem,
  SetManagingProperties,
  SetManagingItemType,

  SetBundles,

  UpdateState,
  ResetState,
}

type Action =
  | { type: Actions.SetIsPublic; payload: IListEditorState["isPublic"] }
  | { type: Actions.SetTitle; payload: IListEditorState["title"] }
  | { type: Actions.SetDescription; payload: IListEditorState["description"] }
  | { type: Actions.SetEmoji; payload: IListEditorState["emoji"] }
  | {
      type: Actions.SetItems;
      payload: IListEditorState["items"];
    }
  | {
      type: Actions.AddItem;
      payload: { item: IListItem; options: AddItemFnParam["options"] };
    }
  | {
      type: Actions.UpdateItem;
      payload: { id: IListItem["id"]; data: Partial<IListItem> };
    }
  | { type: Actions.RemoveItem; payload: IListItem["id"] }
  | { type: Actions.StartAddingItem }
  | { type: Actions.StartEditingItem; payload: IListItem["id"] }
  | { type: Actions.CancelAddingItem }
  | { type: Actions.SetFocusedItem; payload: IListEditorState["focusedItem"] }
  | { type: Actions.SetEditingItem; payload: IListEditorState["editingItem"] }
  | {
      type: Actions.SetManagingProperties;
      payload: IListEditorState["managingProperties"];
    }
  | {
      type: Actions.SetManagingItemType;
      payload: IListEditorState["managingItemType"];
    }
  | {
      type: Actions.SetBundles;
      payload: IListEditorState["bundles"];
    }
  | {
      type: Actions.UpdateState;
      payload: Partial<IListEditorState>;
    }
  | { type: Actions.ResetState };

const reducer = (state: IListEditorState, action: Action): IListEditorState => {
  switch (action.type) {
    case Actions.SetIsPublic:
      return { ...state, isPublic: action.payload };
    case Actions.SetTitle:
      return { ...state, title: action.payload };
    case Actions.SetDescription:
      return { ...state, description: action.payload };
    case Actions.SetEmoji:
      return { ...state, emoji: action.payload };
    case Actions.UpdateItem: {
      const updatedItems = state.items.map(item => {
        if (item.id === action.payload.id) {
          return {
            ...item,
            ...action.payload.data,
          };
        }

        return item;
      });

      return {
        ...state,
        items: updatedItems,
      };
    }
    case Actions.RemoveItem: {
      const id = action.payload;
      const newItems = state.items.filter(i => i.id !== id);
      return {
        ...state,
        items: newItems,
        editingItem: null,
      };
    }
    case Actions.StartAddingItem: {
      return {
        ...state,
        addingItem: true,
        editingItem: null,
        focusedItem: null,
      };
    }
    case Actions.StartEditingItem: {
      return {
        ...state,
        addingItem: false,
        focusedItem: action.payload,
        editingItem: action.payload,
      };
    }
    case Actions.CancelAddingItem: {
      return {
        ...state,
        addingItem: false,
      };
    }
    case Actions.SetItems: {
      return { ...state, items: action.payload };
    }
    case Actions.AddItem:
      const { item, options = {} } = action.payload;
      let newItems = [...state.items, item];

      if (options.bellowItemWithId) {
        const indexOfRefItem = state.items.findIndex(
          item => item.id === options.bellowItemWithId
        );
        if (indexOfRefItem >= 0) {
          newItems = [
            ...state.items.slice(0, indexOfRefItem + 1),
            item,
            ...state.items.slice(indexOfRefItem + 1),
          ];
        }
      }

      return { ...state, items: newItems };
    case Actions.SetFocusedItem:
      return { ...state, focusedItem: action.payload };
    case Actions.SetEditingItem:
      return { ...state, editingItem: action.payload };
    case Actions.SetManagingProperties:
      return { ...state, managingProperties: action.payload };
    case Actions.SetManagingItemType:
      return { ...state, managingItemType: action.payload };
    case Actions.SetBundles:
      return { ...state, bundles: action.payload };
    case Actions.UpdateState: {
      return {
        ...state,
        ...action.payload,
      };
    }
    case Actions.ResetState: {
      return initialState;
    }
    default:
      return state;
  }
};

export function useListEditorState() {
  const [errors, setErrors] = useState({});
  const [state, dispatch] = useReducer(reducer, initialState);
  const { isExceedingFreeListItemsCount } = usePlanValidators();
  const { open: openPlansModal } = usePlansModal();

  // Update any of the state in one shot
  const updateState: IListEditorProviderState["updateState"] = useCallback(
    newState => {
      dispatch({ type: Actions.UpdateState, payload: newState });
    },
    []
  );

  // Title + Description + Emoji
  const updateIsPublic = (isPublic: boolean) =>
    dispatch({ type: Actions.SetIsPublic, payload: isPublic });
  const updateTitle = useCallback(
    (title: string) => dispatch({ type: Actions.SetTitle, payload: title }),
    []
  );
  const updateDescription = useCallback(
    (description: string) =>
      dispatch({ type: Actions.SetDescription, payload: description }),
    []
  );
  const updateEmoji = useCallback(
    (emoji: IEmoji) => dispatch({ type: Actions.SetEmoji, payload: emoji }),
    []
  );

  // Item

  const getItemWithId = useCallback(
    (id?: IListItem["id"] | null) => {
      if (!id) return undefined;

      const item = state.items.find(_item => _item.id === id);
      return item;
    },
    [state.items]
  );

  const addItem: IListEditorProviderState["addItem"] = ({
    type,
    data,
    options,
  }) => {
    if (isExceedingFreeListItemsCount(state.items)) {
      openPlansModal({
        reason: <ListItemsLimitReached />,
        eventReason: EventReasons.ListItemsLimitReached,
      });
      return;
    }

    if (type === ListItemTypes.Separator || data === "---" || data === "----") {
      return addSeparator(data, options);
    }

    if (type === ListItemTypes.Li) {
      return addLi(data, options);
    }

    if (type === ListItemTypes.Text) {
      return addText(data, options);
    }

    return addLi(data, options);
  };

  const addSeparator = (
    data: AddItemFnParam["data"],
    options?: AddItemFnParam["options"]
  ) => {
    // @Todo: factory?
    const separator: IListItem = {
      id: Math.random(),
      type: ListItemTypes.Separator,
      title: "",
      url: "",
      content: "",
    };

    _addItem(separator, options);

    return separator;
  };

  const addLi = (
    data: AddItemFnParam["data"],
    options: AddItemFnParam["options"] = {}
  ) => {
    // @Todo: factory?
    const item: IListItem = {
      id: Math.random(),
      type: ListItemTypes.Li,
      title: data || "",
      url: "",
      content: "",
    };

    _addItem(item, options);

    return item;
  };

  const addText = (
    data: AddItemFnParam["data"],
    options: AddItemFnParam["options"] = {}
  ) => {
    const item: IListItem = {
      id: Math.random(),
      type: ListItemTypes.Text,
      title: "",
      url: "",
      content: data || "",
    };

    _addItem(item, options);

    return item;
  };

  const _addItem = (
    item: IListItem,
    options: AddItemFnParam["options"] = {}
  ) => {
    dispatch({ type: Actions.AddItem, payload: { item, options } });
  };

  const updateItem = (id: IListItem["id"], data: Partial<IListItem>) => {
    dispatch({
      type: Actions.UpdateItem,
      payload: {
        id,
        data,
      },
    });
  };

  const removeItem = (id: IListItem["id"]) => {
    dispatch({ type: Actions.RemoveItem, payload: id });
  };

  const startAddingItem = useCallback(() => {
    dispatch({ type: Actions.StartAddingItem });
  }, []);
  const cancelAddingItem = useCallback(
    () => dispatch({ type: Actions.CancelAddingItem }),
    []
  );
  const startEditing = useCallback((id: IListItem["id"]) => {
    dispatch({ type: Actions.StartEditingItem, payload: id });
  }, []);
  const cancelEditing = () =>
    dispatch({ type: Actions.SetEditingItem, payload: null });

  const getIndexOfFocusedItem = useCallback(
    (id?: IListItem["id"]) => {
      const refItemId = id || state.focusedItem;
      const refItem = getItemWithId(refItemId);
      return refItem ? state.items.findIndex(i => i.id === refItem.id) : null;
    },
    [getItemWithId, state.focusedItem, state.items]
  );

  const startEditingNext = useCallback(
    (id?: IListItem["id"]) => {
      if (state.managingItemType) return;

      const index = getIndexOfFocusedItem(id);
      const nextItem =
        index !== null
          ? state.items[index + 1]
          : getItemWithId(state.focusedItem);

      if (!nextItem && state.focusedItem) startAddingItem();
      else startEditing(nextItem?.id || state.items[0].id);
    },
    [
      getIndexOfFocusedItem,
      getItemWithId,
      startAddingItem,
      startEditing,
      state.focusedItem,
      state.items,
      state.managingItemType,
    ]
  );

  const startEditingPrev = useCallback(
    (id?: IListItem["id"]) => {
      if (state.managingItemType) return;

      const index = getIndexOfFocusedItem(id);
      const prevItem =
        index !== null
          ? state.items[index - 1]
          : getItemWithId(state.focusedItem);
      if (!prevItem && !state.addingItem) startAddingItem();
      else startEditing(prevItem?.id || state.items[state.items.length - 1].id);
    },
    [
      getIndexOfFocusedItem,
      getItemWithId,
      startAddingItem,
      startEditing,
      state.addingItem,
      state.focusedItem,
      state.items,
      state.managingItemType,
    ]
  );

  const startEditingLast = useCallback(() => {
    if (state.managingItemType) return;

    const lastItem = state.items[state.items.length - 1];

    if (lastItem) startEditing(lastItem.id);
  }, [startEditing, state.items, state.managingItemType]);

  const startEditingFirst = useCallback(() => {
    if (state.managingItemType) return;

    const firstItem = state.items[0];

    if (firstItem) startEditing(firstItem.id);
  }, [startEditing, state.items, state.managingItemType]);

  const onDragItem: IListEditorProviderState["onDragItem"] = (
    startIndex,
    endIndex
  ) => {
    if (endIndex === undefined) return;
    const newItems = reorder(state.items, startIndex, endIndex);
    dispatch({ type: Actions.SetItems, payload: newItems });
  };

  const validate = () => {
    const _errors: IListEditorErrors = {};
    if (state.title.length < 2) {
      _errors.title = "Title is too short.";
    }
    return _errors;
  };

  // Item type context

  const startManagingItemType = (id?: IListItem["id"]) =>
    dispatch({ type: Actions.SetManagingItemType, payload: id || true });
  const cancelManagingItemType = () =>
    dispatch({ type: Actions.SetManagingItemType, payload: null });

  // Properties

  const startManagingProperties = (id: IListItem["id"]) =>
    dispatch({ type: Actions.SetManagingProperties, payload: { id } });
  const cancelManagingProperties = () =>
    dispatch({ type: Actions.SetManagingProperties, payload: null });

  // Focus
  const cancelFocus = () =>
    dispatch({ type: Actions.SetFocusedItem, payload: null });

  // Reset state
  const resetState = useCallback(() => {
    dispatch({ type: Actions.ResetState });
  }, []);

  // Bundles
  const setBundles = (bundles: IListEditorState["bundles"]) =>
    dispatch({ type: Actions.SetBundles, payload: bundles });

  const _state: IListEditorProviderState = {
    // reset
    updateState,
    resetState,
    state,

    // errors
    errors,
    setErrors,
    validate,

    id: state.id,
    uid: state.uid,

    // isPublic + title + description + emoji
    isPublic: state.isPublic,
    title: state.title,
    description: state.description,
    emoji: state.emoji,
    updateIsPublic,
    updateTitle,
    updateDescription,
    updateEmoji,

    // item
    items: state.items,
    focusedItem: state.focusedItem,
    editingItem: state.editingItem,
    addingItem: state.addingItem,

    addItem,
    setItems: useCallback(
      items => dispatch({ type: Actions.SetItems, payload: items }),
      []
    ),
    updateItem,
    removeItem,
    startEditing,
    cancelEditing,
    startEditingNext,
    startEditingPrev,
    startEditingLast,
    startEditingFirst,
    startAddingItem,
    cancelAddingItem,
    cancelFocus,
    onDragItem,
    getItemWithId,

    // item type
    managingItemType: state.managingItemType,
    startManagingItemType,
    cancelManagingItemType,

    // item properties
    managingProperties: state.managingProperties,
    startManagingProperties,
    cancelManagingProperties,

    // bundles
    bundles: state.bundles,
    setBundles,
  };

  return _state;
}
