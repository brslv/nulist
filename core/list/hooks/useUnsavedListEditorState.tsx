import { useState, useEffect } from "react";
import { useNotifications } from "../../notifications/context/NotificationsProvider";
import { NotificationTypes } from "../../notifications/types/NotificationTypes";
import { IListEditorState } from "../types/IListEditorState";

export const UNSAVED_LIST_EDITOR_STATE_LOCAL_STORAGE_KEY =
  "unsavedListEditorState";

export function useUnsavedListEditorState() {
  const { addNotification } = useNotifications();
  const [unsavedListEditorState, setUnsavedListEditorState] =
    useState<IListEditorState | null>(null);

  useEffect(() => {
    try {
      updateFromLocalStorage();
    } catch (error) {
      addNotification({
        type: NotificationTypes.Error,
        content: "Broken unsaved list state.",
      });
    }
  }, [addNotification]);

  const updateFromLocalStorage = () => {
    const state = JSON.parse(
      localStorage.getItem(UNSAVED_LIST_EDITOR_STATE_LOCAL_STORAGE_KEY) || "{}"
    );
    if (Object.keys(state).length === 0) {
      setUnsavedListEditorState(null);
    } else {
      setUnsavedListEditorState(state);
    }
  };

  const storeUnsavedListEditorState = (state: IListEditorState) =>
    localStorage.setItem(
      UNSAVED_LIST_EDITOR_STATE_LOCAL_STORAGE_KEY,
      JSON.stringify(state)
    );

  const clearUnsavedListEditorState = () => {
    localStorage.removeItem(UNSAVED_LIST_EDITOR_STATE_LOCAL_STORAGE_KEY);
    updateFromLocalStorage();
  };

  return {
    unsavedListEditorState,
    storeUnsavedListEditorState,
    clearUnsavedListEditorState,
  };
}
