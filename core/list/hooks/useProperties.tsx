import { useEffect, useReducer } from "react";
import {
  ManagingPropertyDescriptor,
  useListEditor,
} from "../context/ListEditorProvider";
import { IListItem } from "../types/IListItem";
import { ListItemProperties } from "../types/ListItemProperties";

interface IState {
  url: string;
  content: string;
}

const initialState: IState = {
  url: "",
  content: "",
};

enum Actions {
  UpdateProperty = "UpdateProperty",
  LoadPropertiesForItem = "LoadPropertiesForItem",
}

type Action =
  | {
      type: Actions.UpdateProperty;
      payload: { name: string; value: string };
    }
  | {
      type: Actions.LoadPropertiesForItem;
      payload: IListItem | null | undefined;
    };

const reducer = (state: IState, action: Action): IState => {
  switch (action.type) {
    case Actions.LoadPropertiesForItem: {
      return {
        ...state,
        url: action.payload?.url || "",
        content: action.payload?.content || "",
      };
    }
    case Actions.UpdateProperty: {
      return {
        ...state,
        [action.payload.name]: action.payload.value,
      };
    }
    default:
      return state;
  }
};

export function useProperties(
  managingPropertyDescriptor: ManagingPropertyDescriptor | null
) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const { updateItem, getItemWithId } = useListEditor();

  useEffect(() => {
    if (managingPropertyDescriptor) {
      dispatch({
        type: Actions.LoadPropertiesForItem,
        payload: getItemWithId(managingPropertyDescriptor.id),
      });
    }
  }, [getItemWithId, managingPropertyDescriptor]);

  const updateProperty = (name: ListItemProperties, value: string) => {
    dispatch({
      type: Actions.UpdateProperty,
      payload: { name, value: value.trim() },
    });
  };

  const save = () => {
    if (managingPropertyDescriptor)
      updateItem(managingPropertyDescriptor.id, {
        url: state.url.trim(),
        content: state.content.trim(),
      });
  };

  return {
    state,
    updateProperty,
    save,
  };
}
