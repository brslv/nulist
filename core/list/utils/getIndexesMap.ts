import { ListItemTypes } from "../../../components/ListEditor/ListItemTypeContextMenu/types/IListItemType";
import { IListItem } from "../types/IListItem";

export type IndexesMap = { [listItemId: number]: number | undefined };

export const getIndexesMap = (items: IListItem[]) => {
  const map: IndexesMap = {};
  let currentIndex = 1;
  for (const item of items) {
    if (
      item.type === ListItemTypes.Separator ||
      item.type === ListItemTypes.SubscriptionForm
    ) {
      map[item.id] = undefined;
      continue;
    }

    map[item.id] = currentIndex;

    currentIndex++;
  }
  return map;
};

export const getMaxIndex = (indexesMap: IndexesMap) => {
  const indexes = Object.values(indexesMap).filter(
    v => v !== undefined
  ) as number[];

  if (!indexes.length) return 0;

  return Math.max(...indexes);
};
