import { IListItem } from "../types/IListItem";

export const reorder = (
  items: IListItem[],
  startIndex: number,
  endIndex: number
): IListItem[] => {
  const result = Array.from(items);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};
