import {
  createContext,
  Dispatch,
  ReactNode,
  SetStateAction,
  useCallback,
  useContext,
  useState,
} from "react";
import { IUser } from "../../auth/types/IUser";
import { ClientEvents } from "../../clientEvents/ClientEvents";
import { fireClientEvent } from "../../clientEvents/fireClientEvent";
import { useNotifications } from "../../notifications/context/NotificationsProvider";
import { NotificationTypes } from "../../notifications/types/NotificationTypes";
import { getAllListsForUser, removeList } from "../listService";
import { IList } from "../types/IList";
import { ListVisibilities } from "../types/ListVisibilities";

interface ICtx {
  lists: IList[] | null;
  loading: boolean;
  load: (user: IUser, opts: { visibility: ListVisibilities }) => Promise<void>;
  remove: (id: IList["id"]) => Promise<void>;
  setLists: Dispatch<SetStateAction<IList[] | null>>;
}
interface IProps {
  children: ReactNode;
}

const UserListsContext = createContext<ICtx | null>(null);

export function UserListsProvider({ children }: IProps) {
  const [lists, setLists] = useState<IList[] | null>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const { addNotification } = useNotifications();

  const remove = async (id: IList["id"]) => {
    try {
      const { error } = await removeList(id);
      if (error) throw error;
      setLists(prev => prev && prev.filter(list => list.id !== id));
      addNotification({
        type: NotificationTypes.Success,
        content: "List deleted 👌",
      });
      fireClientEvent(ClientEvents.ListDeleted);
    } catch (error: any) {
      addNotification({
        type: NotificationTypes.Error,
        content: error.message,
      });
    }
  };

  const load = useCallback(
    async (user: IUser, { visibility }: { visibility: ListVisibilities }) => {
      setLoading(true);

      try {
        const lists = await getAllListsForUser(user, { visibility });
        setLists(lists || []);
      } catch (error: any) {
        addNotification({
          type: NotificationTypes.Error,
          content: error.message,
        });
      } finally {
        setLoading(false);
      }
    },
    [addNotification]
  );

  return (
    <UserListsContext.Provider
      value={{ lists, loading, load, remove, setLists }}
    >
      {children}
    </UserListsContext.Provider>
  );
}

export function useUserLists() {
  const ctx = useContext(UserListsContext);
  if (!ctx) throw new Error("Improper use of UsersListProvider.");
  return ctx;
}
