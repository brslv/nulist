import {
  ReactNode,
  useCallback,
  useEffect,
  useState,
  useContext,
  createContext,
} from "react";
import { useAuth } from "../../auth/context/AuthProvider";
import { IListItem } from "../types/IListItem";
import {
  getList,
  save as saveList,
  update as updateList,
} from "../listService";
import { NotificationTypes } from "../../notifications/types/NotificationTypes";
import { useNotifications } from "../../notifications/context/NotificationsProvider";
import { useRouter } from "next/router";
import { IListEditorProviderState } from "../types/IListEditorProviderState";
import { useListEditorState } from "../hooks/useListEditorState";
import { ERROR_CODES } from "../../supabase/errorCodes";
import { AuthFrom } from "../../../components/pages/Auth/Auth";
import { IListEditorState } from "../types/IListEditorState";
import { useUnsavedListEditorState } from "../hooks/useUnsavedListEditorState";
import { fireClientEvent } from "../../clientEvents/fireClientEvent";
import { ClientEvents } from "../../clientEvents/ClientEvents";
import { usePlanValidators } from "../../plans/usePlanValidators";
import { useUserLists } from "./UserListsProvider";
import { usePlansModal } from "../../plans/context/PlansModalProvider";
import { ListsLimitReached } from "../../../components/PlansModal/reasons/ListsLimitReached";
import { EventReasons } from "../../plans/EventReasons";

export type ManagingPropertyDescriptor = {
  id: IListItem["id"];
};

interface ICtx extends IListEditorProviderState {
  onSave: () => Promise<void>;
  saveBtnLabel: string;
  loading: boolean;
}

const ListEditorContext = createContext<ICtx | null>(null);

export enum ListEditorMode {
  Create,
  Edit,
}

export function ListEditorProvider({
  children,
  uid,
  initialState,
}: {
  children: ReactNode;
  uid?: string;
  initialState?: IListEditorState | null;
}) {
  const { user } = useAuth();
  const { lists } = useUserLists();
  const { open: openPlansModal } = usePlansModal();
  const { isExceedingFreeListsCount } = usePlanValidators();
  const router = useRouter();
  const { addNotification } = useNotifications();
  const editorState = useListEditorState();
  const { updateState } = editorState;
  const mode = uid ? ListEditorMode.Edit : ListEditorMode.Create;
  const [loading, setLoading] = useState<boolean>(mode === ListEditorMode.Edit);
  const { storeUnsavedListEditorState, clearUnsavedListEditorState } =
    useUnsavedListEditorState();
  const saveBtnLabel = mode === ListEditorMode.Edit ? "Update" : "Save";

  const loadListStateForEditing = useCallback(
    async (uid: string) => {
      setLoading(true);

      const { list } = await getList({ uid });

      if (!list) {
        addNotification({
          type: NotificationTypes.Error,
          content: `Couldn't load list data for list ${uid}`,
        });
        setLoading(false);
        return;
      }

      const { listItems } = list;

      updateState({
        id: list.id,
        uid: list.uid,
        isPublic: list.isPublic,
        title: list.title,
        description: list.description || "",
        emoji: {
          emoji: list.emoji,
          name: list.emojiName || "emoji",
        },
        items: listItems,
        bundles: list.bundles.map(b => b.id),
      });

      setLoading(false);
    },
    [addNotification, updateState]
  );

  useEffect(() => {
    if (uid) {
      loadListStateForEditing(uid);
    }
  }, [loadListStateForEditing, uid]);

  useEffect(() => {
    if (!uid && initialState) {
      updateState(initialState);
    }
  }, [initialState, uid, updateState]);

  const save = async () => {
    editorState.setErrors({});
    const errors = editorState.validate();
    editorState.setErrors(errors);

    if (Object.keys(errors).length) return;

    if (user && isExceedingFreeListsCount(lists || [])) {
      openPlansModal({
        reason: <ListsLimitReached />,
        eventReason: EventReasons.ListsLimitReached,
      });
      return;
    }

    const { error } = await saveList(
      {
        isPublic: editorState.isPublic,
        title: editorState.title,
        description: editorState.description,
        emoji: editorState.emoji.emoji,
        emojiName: editorState.emoji.name,
        userUid: user?.id,
      },
      editorState.items,
      editorState.bundles
    );

    if (error) {
      // save list to local storage
      storeUnsavedListEditorState(editorState.state);

      if (error.code === ERROR_CODES.RLS_POLICY_VIOLATION) {
        fireClientEvent(ClientEvents.ListCreated, {
          fromAuthenticatedUser: "false",
        });

        router.push({
          pathname: "/auth",
          query: { from: AuthFrom.ListEditor },
        });
        return;
      }

      addNotification({
        type: NotificationTypes.Error,
        content: error.message,
      });
      return;
    }

    addNotification({
      type: NotificationTypes.Success,
      content: "List saved 👌",
    });

    if (!uid && initialState) {
      // the user has saved unsaved list - clear the unsaved list editor state
      clearUnsavedListEditorState();
    }

    fireClientEvent(ClientEvents.ListCreated, {
      fromAuthenticatedUser: "true",
    });

    router.push("/dashboard");
  };

  const update = async () => {
    editorState.setErrors({});
    const errors = editorState.validate();
    editorState.setErrors(errors);

    if (Object.keys(errors).length) return;

    const { error } = await updateList(
      {
        id: editorState.id,
        uid: editorState.uid,
        isPublic: editorState.isPublic,
        title: editorState.title,
        description: editorState.description,
        emoji: editorState.emoji.emoji,
        emojiName: editorState.emoji.name,
        userUid: user?.id,
      },
      editorState.items,
      editorState.bundles
    );

    if (error) {
      addNotification({
        type: NotificationTypes.Error,
        content: error.message,
      });
      return;
    }

    addNotification({
      type: NotificationTypes.Success,
      content: "List updated 👌",
    });
    fireClientEvent(ClientEvents.ListUpdated);
    router.push("/dashboard");
  };

  const onSave = async () => {
    if (mode === ListEditorMode.Edit) {
      await update();
    } else {
      // save
      await save();
    }
  };

  const value = {
    ...editorState,
    onSave,
    saveBtnLabel,
    loading,
  };

  return (
    <ListEditorContext.Provider value={value}>
      {children}
    </ListEditorContext.Provider>
  );
}

export function useListEditor() {
  const ctx = useContext(ListEditorContext);
  if (!ctx) throw new Error("Improper use of ListContext.");
  return ctx;
}
