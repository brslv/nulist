import { ListItemTypes } from "../../../components/ListEditor/ListItemTypeContextMenu/types/IListItemType";
import { IListItem } from "./IListItem";

export type AddItemOptions = {
  bellowItemWithId?: IListItem["id"];
};

export type AddItemFnParam =
  | {
      type: ListItemTypes.Li;
      data: IListItem["title"];
      options?: AddItemOptions;
    }
  | {
      type: ListItemTypes.Separator;
      data?: null;
      options?: AddItemOptions;
    }
  | {
      type: ListItemTypes.Text;
      data?: string;
      options?: AddItemOptions;
    };
