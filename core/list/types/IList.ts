import { IUserProfile } from "../../auth/types/IUserProfile";
import { IBundle } from "../../bundles/types/IBundle";
import { IListItem } from "./IListItem";

export interface IList {
  id: number;
  uid: string;
  title: string;
  description?: string;
  emoji?: string;
  emojiName?: string;
  createdAt: string;
  updatedAt: string;
  userUid: string;
  profileUid: string;
  author?: {
    id: IUserProfile["id"];
    email: IUserProfile["email"];
    name: IUserProfile["name"];
    isPro: IUserProfile["isPro"];
  };
  listItems?: IListItem[];
  isPublic: boolean;
  bundles: Array<{
    id: IBundle["id"];
    title: IBundle["title"];
    isPublic: IBundle["isPublic"];
  }>;
}
