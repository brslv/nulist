import { IListEditorProviderState } from "./IListEditorProviderState";

export interface IListEditorState {
  id: IListEditorProviderState["id"];
  uid: IListEditorProviderState["uid"];
  isPublic: IListEditorProviderState["isPublic"];
  title: IListEditorProviderState["title"];
  description: IListEditorProviderState["description"];
  emoji: IListEditorProviderState["emoji"];
  items: IListEditorProviderState["items"];
  bundles: IListEditorProviderState["bundles"];
  addingItem: IListEditorProviderState["addingItem"];
  focusedItem: IListEditorProviderState["focusedItem"];
  editingItem: IListEditorProviderState["editingItem"];
  managingProperties: IListEditorProviderState["managingProperties"];
  managingItemType: IListEditorProviderState["managingItemType"];
}
