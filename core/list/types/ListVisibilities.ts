export enum ListVisibilities {
  All = "all",
  Public = "public",
  Private = "private",
}
