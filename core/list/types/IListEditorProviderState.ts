import { IBundle } from "../../bundles/types/IBundle";
import { ManagingPropertyDescriptor } from "../context/ListEditorProvider";
import { AddItemFnParam } from "./AddItemFnParam";
import { IEmoji } from "./IEmoji";
import { IList } from "./IList";
import { IListEditorErrors } from "./IListEditorErrors";
import { IListEditorState } from "./IListEditorState";
import { IListItem } from "./IListItem";

export interface IListEditorProviderState {
  id: IList["id"];
  uid: IList["uid"];
  // reset
  updateState: (state: Partial<IListEditorState>) => void;
  resetState: () => void;
  state: IListEditorState;

  // errors
  errors: IListEditorErrors;
  setErrors: (arg0: IListEditorErrors) => void;
  validate: () => IListEditorErrors;

  // isPublic + title + description + emoji
  isPublic: boolean;
  title: string;
  description: string;
  emoji: IEmoji;
  updateIsPublic: (isPublic: boolean) => void;
  updateTitle: (title: string) => void;
  updateDescription: (description: string) => void;
  updateEmoji: (emoji: IEmoji) => void;

  // item
  items: IListItem[];
  focusedItem: IListItem["id"] | null;
  editingItem: IListItem["id"] | null;
  addingItem: boolean;

  addItem: (arg0: AddItemFnParam) => IListItem | undefined;
  setItems: (items: IListItem[]) => void;
  updateItem: (id: IListItem["id"], data: Partial<IListItem>) => void;
  removeItem: (id: IListItem["id"]) => void;
  startEditing: (id: IListItem["id"]) => void;
  cancelEditing: () => void;
  startEditingNext: (id?: IListItem["id"]) => void;
  startEditingPrev: (id?: IListItem["id"]) => void;
  startEditingLast: () => void;
  startEditingFirst: () => void;
  startAddingItem: () => void;
  cancelAddingItem: () => void;
  cancelFocus: () => void;
  onDragItem: (startIndex: number, endIndex?: number) => void;
  getItemWithId: (id?: IListItem["id"] | null) => IListItem | undefined;

  // item type
  managingItemType: IListItem["id"] | boolean | null;
  startManagingItemType: (id?: IListItem["id"]) => void;
  cancelManagingItemType: () => void;

  // item properties
  managingProperties: ManagingPropertyDescriptor | null;
  startManagingProperties: (id: IListItem["id"]) => void;
  cancelManagingProperties: () => void;

  // bundles
  bundles: Array<IBundle["id"]>;
  setBundles: (bundles: Array<IBundle["id"]>) => void;
}
