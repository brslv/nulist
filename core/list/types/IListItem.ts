import { ListItemTypes } from "../../../components/ListEditor/ListItemTypeContextMenu/types/IListItemType";

export interface IListItem {
  id: number;
  title: string;
  type: ListItemTypes;
  url?: string;
  content?: string;
  userUid?: string;
  listId?: number;
}
