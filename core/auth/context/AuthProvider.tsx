import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from "react";
import { supabase, SupabaseAuthPayload } from "../../../lib/supabaseClient";
import { IUser } from "../types/IUser";
import {
  signIn as supabaseSignIn,
  signUp as supabaseSignUp,
  signOut as supabaseSignOut,
  getSession,
} from "../authService";
import { useRouter } from "next/router";
import { AuthChangeEvent, Session } from "@supabase/supabase-js";
import { useNotifications } from "../../notifications/context/NotificationsProvider";
import { NotificationTypes } from "../../notifications/types/NotificationTypes";

interface ICtx {
  user: IUser | null;
  loading: boolean;
  signUp: (payload: SupabaseAuthPayload) => void;
  signIn: (payload: SupabaseAuthPayload) => void;
  signOut: () => void;
}

interface IProps {
  children: ReactNode;
}

export const AuthContext = createContext<ICtx | null>(null);

export function AuthProvider({ children }: IProps) {
  const router = useRouter();
  const { addNotification } = useNotifications();
  const [user, setUser] = useState<IUser | null>(null);
  const [loading, setLoading] = useState(true);

  const signUp = async (payload: SupabaseAuthPayload) => {
    try {
      setLoading(true);
      return await supabaseSignUp(payload);
    } catch (error: any) {
      addNotification({
        type: NotificationTypes.Error,
        content: error.message,
      });
    } finally {
      setLoading(false);
    }
  };

  const signIn = async (payload: SupabaseAuthPayload) => {
    try {
      setLoading(true);
      return await supabaseSignIn(payload);
    } catch (error: any) {
      // @Todo: handle errors
      addNotification({
        type: NotificationTypes.Error,
        content: error.message,
      });
    } finally {
      setLoading(false);
    }
  };

  const setServerSession = async (
    event: AuthChangeEvent,
    session: Session | null
  ) => {
    await fetch("/api/auth", {
      method: "POST",
      headers: new Headers({ "Content-Type": "application/json" }),
      credentials: "same-origin",
      body: JSON.stringify({ event, session }),
    });
  };

  const signOut = async () => {
    await supabaseSignOut();
  };

  useEffect(() => {
    const session = getSession();

    setLoading(true);

    if (session) {
      setServerSession("SIGNED_IN", session);
      setUser(session?.user ?? null);
    }

    const { data: authListener } = supabase.auth.onAuthStateChange(
      async (event, session) => {
        await setServerSession(event, session);
        setUser(session?.user ? session.user : null);

        if (event === "SIGNED_OUT") router.push("/");
        if (event === "SIGNED_IN") {
          await router.push("/dashboard");
        }

        setLoading(false);
      }
    );

    setLoading(false);

    return () => {
      authListener?.unsubscribe();
    };
  }, [addNotification]); // eslint-disable-line

  return (
    <AuthContext.Provider value={{ user, signIn, signUp, signOut, loading }}>
      {children}
    </AuthContext.Provider>
  );
}

export function useAuth() {
  const ctx = useContext(AuthContext);
  if (!ctx) throw new Error("Improper use of AuthProvider.");
  return ctx;
}
