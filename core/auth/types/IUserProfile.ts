export interface IUserProfile {
  id: string;
  name: string | null;
  email: string;
  isPro: boolean;
  proCreatedAt: string | null;
  proEndedAd: string | null;
  proCancelledAt: string | null;
  proStartedAt: string | null;
  createdAt: string;
  updatedAt: string;
}
