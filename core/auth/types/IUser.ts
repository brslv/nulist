import { SupabaseUser } from "../../../lib/supabaseClient";

export type IUser = SupabaseUser;
