import { IncomingMessage } from "http";
import { NextApiRequestCookies } from "next/dist/server/api-utils";
import { supabase } from "../../lib/supabaseClient";

export function signUp({
  email,
  password,
}: {
  email: string;
  password: string;
}) {
  return supabase.auth.signUp({
    email,
    password,
  });
}

export function signIn({
  email,
  password,
}: {
  email: string;
  password: string;
}) {
  return supabase.auth.signIn({
    email,
    password,
  });
}

export function signOut() {
  return supabase.auth.signOut();
}

export function getUser() {
  return supabase.auth.user();
}

export function getSession() {
  return supabase.auth.session();
}

export function getUserByCookie(req: IncomingMessage) {
  return supabase.auth.api.getUserByCookie(req);
}

export function setAuth(
  req: IncomingMessage & { cookies: NextApiRequestCookies }
) {
  const sbToken = req.cookies["sb:token"];
  supabase.auth.setAuth(sbToken);
}
