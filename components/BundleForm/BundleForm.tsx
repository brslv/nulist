import { ChangeEvent } from "react";
import {
  BundleFormMode,
  useBundleForm,
} from "../../core/bundles/context/BundleFormProvider";
import { Button } from "../atomic/Button/Button";
import { FormRow } from "../atomic/FormRow/FormRow";
import { Input } from "../atomic/Input/Input";
import { Label } from "../atomic/Label/Label";
import Toggle from "../atomic/Toggle/Toggle";
import styles from "./BundleForm.module.css";

export function BundleForm() {
  const { onSubmit, close, hasErrors, errors, state, mode, setState } =
    useBundleForm();

  const updateToggle = (isOn: boolean) =>
    setState({ ...state, isPublic: isOn });

  const updateTitle = (e: ChangeEvent<HTMLInputElement>) =>
    setState({ ...state, [e.currentTarget.name]: e.currentTarget.value });

  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        onSubmit().then(close);
      }}
    >
      <FormRow>
        <Label>Title</Label>
        <Input
          maxLength={40}
          fullWidth
          autoFocus
          placeholder="E.g. Programming"
          name="title"
          value={state.title}
          onChange={updateTitle}
          error={hasErrors ? errors.title : null}
        />
      </FormRow>

      <Label htmlFor="toggle">
        <FormRow background isInline spaceBetween bottomSpace>
          <Label htmlFor="toggle">Public</Label>
          <Toggle isOn={state.isPublic} onToggle={updateToggle} id="toggle" />
        </FormRow>
      </Label>

      <FormRow isInline bottomSpace={false}>
        <Button
          variant="accent"
          rootClassName={styles.submitBtnRoot}
          type="submit"
        >
          {mode === BundleFormMode.Create ? "Create" : "Update"}
        </Button>
        <Button variant="plain" onClick={close}>
          Cancel
        </Button>
      </FormRow>
    </form>
  );
}
