import { breadcrumbs } from "../../../core/breadcrumbs/breadcrumbs";
import { IBundle } from "../../../core/bundles/types/IBundle";
import { Page, PageMain, PageTitle } from "../../atomic/Page/Page";
import { ListsStack } from "../../ListsStack/ListsStack";

interface IProps {
  bundle: IBundle;
}

export function BundleView({ bundle }: IProps) {
  return (
    <Page>
      <PageMain
        wide
        breadcrumbs={[
          breadcrumbs.dashboard,
          {
            href: breadcrumbs.bundle.href(bundle.id),
            title: breadcrumbs.bundle.title(bundle.title),
            clickable: false,
          },
        ]}
      >
        <PageTitle title={bundle.title} icon="bundle" />
        <div>
          {bundle?.lists?.length ? (
            <ListsStack
              showCreateBtn={false}
              showUnsavedLists={false}
              lists={bundle.lists || []}
            />
          ) : (
            <div>No lists found</div>
          )}
        </div>
      </PageMain>
    </Page>
  );
}
