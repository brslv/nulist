import React from "react";
import Link from "next/link";
import { Button } from "../../atomic/Button/Button";
import { FormRow } from "../../atomic/FormRow/FormRow";
import { Input } from "../../atomic/Input/Input";
import { Label } from "../../atomic/Label/Label";
import { Title } from "../../atomic/Title/Title";
import cn from "classnames";
import styles from "./FormStyles.module.css";
import { useForm } from "./useForm";
import { useAuth } from "../../../core/auth/context/AuthProvider";

interface IProps {
  onSubmitted: (email: string) => void;
}

export function SignUpForm({ onSubmitted }: IProps) {
  const { signUp, loading } = useAuth();
  const { errors, state, onChange, onSubmit } = useForm();

  return (
    <div>
      <form onSubmit={onSubmit(signUp, onSubmitted)} className={styles.form}>
        <Title weight="semiBold" size={40} className={styles.title}>
          Sign up
        </Title>

        <FormRow>
          <Label error={errors?.email} htmlFor="email">
            Email
          </Label>
          <Input
            onChange={onChange}
            type="email"
            name="email"
            fullWidth
            id="email"
            autoFocus
            value={state.email}
            error={errors?.email}
          />
        </FormRow>

        <FormRow>
          <Label error={errors?.password} htmlFor="password">
            Password
          </Label>
          <Input
            onChange={onChange}
            type="password"
            name="password"
            fullWidth
            id="password"
            value={state.password}
            error={errors?.password}
          />
        </FormRow>

        <div className={styles.footer}>
          <FormRow>
            <Button
              disabled={loading}
              type="submit"
              variant="accent"
              center
              fullWidth
            >
              {loading ? "Setting your account up..." : "Sign up"}
            </Button>
          </FormRow>

          <Link href={{ pathname: "/auth", query: { mode: "sign-in" } }}>
            <a className={cn(styles.link, styles.block)}>
              Already have an account?
            </a>
          </Link>
        </div>
      </form>
    </div>
  );
}
