import React, { useEffect, useRef, useState } from "react";
import { Panel } from "../../atomic/Panel/Panel";
import styles from "./ComingFromListEditorMessage.module.css";
import { CSSTransition } from "react-transition-group";
import { Icon } from "../../atomic/Icon/Icon";

interface IProps {
  isActive?: boolean;
}

export function ComingFromListEditorMessage({ isActive = false }: IProps) {
  const ref = useRef(null);
  const [activated, setActivated] = useState(false);

  useEffect(() => {
    setActivated(isActive);
  }, [isActive]);

  return (
    <CSSTransition
      nodeRef={ref}
      classNames={{
        enter: styles.animEnter,
        enterActive: styles.animEnterActive,
        exit: styles.animExit,
        exitActive: styles.animExitActive,
      }}
      timeout={500}
      in={activated}
      unmountOnExit
    >
      <div ref={ref}>
        <Panel className={styles.comingFromListEditorMessage}>
          <div className={styles.check}>
            <Icon name="check" />
          </div>
          Your list is safe and sound. 🥰
          <br />
          Complete the process by making a FREE account.👇
        </Panel>
      </div>
    </CSSTransition>
  );
}
