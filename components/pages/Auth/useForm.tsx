import { ChangeEvent, FormEvent, useState } from "react";
import { useNotifications } from "../../../core/notifications/context/NotificationsProvider";
import { NotificationTypes } from "../../../core/notifications/types/NotificationTypes";

interface Errors {
  email?: string;
  password?: string;
}

export function useForm() {
  const { addNotification: add } = useNotifications();
  const [errors, setErrors] = useState<Errors | null>(null);
  const [state, setState] = useState({
    email: "",
    password: "",
  });

  const validate = () => {
    const errors: Errors = {};

    if (!state.email) {
      errors.email = "Email must be provided.";
    }
    if (!state.password) {
      errors.password = "Password must be provided.";
    }

    return Object.keys(errors).length ? errors : null;
  };

  const onSubmit =
    (authFunction: any, onSubmitted?: (email: string) => void) =>
    async (e: FormEvent) => {
      e.preventDefault();

      setErrors(null);

      const errors = validate();

      if (errors) {
        setErrors(errors);
        return;
      }

      const { user, error } = await authFunction({
        email: state.email,
        password: state.password,
      });

      if (!error && user && typeof onSubmitted === "function")
        onSubmitted(state.email);
      if (error)
        add({
          type: NotificationTypes.Error,
          content: error.message,
        });
    };

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;

    setState(prev => ({ ...prev, [name]: value }));
  };

  return {
    errors,
    state,
    setState,
    onSubmit,
    onChange,
  };
}
