import React, { useEffect, useState } from "react";
import styles from "./Auth.module.css";
import { SignUpForm } from "./SignUpForm";
import { useRouter } from "next/dist/client/router";
import { SignInForm } from "./SignInForm";
import { PostSignUp } from "./PostSignUp";
import { ComingFromListEditorMessage } from "./ComingFromListEditorMessage";

export enum AuthMode {
  SignUp = "sign-up",
  SignIn = "sign-in",
  PostSignUp = "post-sign-up",
}

export enum AuthFrom {
  ListEditor = "list-editor",
}

export function Auth() {
  const [mode, setMode] = useState(AuthMode.SignUp);
  const [from, setFrom] = useState<AuthFrom | null>(null);
  const [signedUpUserEmail, setSignedUpUserEmail] = useState<
    string | undefined
  >();
  const router = useRouter();
  const isComingFromListEditor = !!(from && from === AuthFrom.ListEditor);

  useEffect(() => {
    const mode = router.query.mode;
    const from = router.query.from;
    if (mode) setMode(mode as AuthMode);
    if (from) setFrom(from as AuthFrom);

    return () => {
      setFrom(null);
    };
  }, [router.query]);

  const onSignUpSubmitted = (email: string) => {
    setSignedUpUserEmail(email);
    router.push({ pathname: "/auth", query: { mode: "post-sign-up" } });
  };

  return (
    <div className={styles.auth}>
      <div className={styles.container}>
        <ComingFromListEditorMessage isActive={isComingFromListEditor} />
        <div
          style={{
            marginTop: isComingFromListEditor ? -10 : 0,
            position: "relative",
            zIndex: 10,
          }}
        >
          {mode === AuthMode.SignIn ? <SignInForm /> : null}
          {mode === AuthMode.SignUp ? (
            <SignUpForm onSubmitted={onSignUpSubmitted} />
          ) : null}
          {mode === AuthMode.PostSignUp ? (
            <PostSignUp email={signedUpUserEmail} />
          ) : null}
        </div>
      </div>
    </div>
  );
}
