import React from "react";
import Link from "next/link";
import { Button } from "../../atomic/Button/Button";
import { FormRow } from "../../atomic/FormRow/FormRow";
import { Input } from "../../atomic/Input/Input";
import { Label } from "../../atomic/Label/Label";
import { Title } from "../../atomic/Title/Title";
import cn from "classnames";
import styles from "./FormStyles.module.css";
import { useForm } from "./useForm";
import { useAuth } from "../../../core/auth/context/AuthProvider";

export function SignInForm() {
  const { signIn } = useAuth();
  const { errors, state, onChange, onSubmit } = useForm();

  return (
    <div>
      <form onSubmit={onSubmit(signIn)} className={styles.form}>
        <Title weight="semiBold" size={40} className={styles.title}>
          Sign in
        </Title>

        <FormRow>
          <Label error={errors?.email} htmlFor="email">
            Email
          </Label>
          <Input
            onChange={onChange}
            value={state.email}
            type="email"
            name="email"
            fullWidth
            id="email"
            autoFocus
            error={errors?.email}
          />
        </FormRow>

        <FormRow>
          <Label error={errors?.password} htmlFor="password">
            Password
          </Label>
          <Input
            onChange={onChange}
            value={state.password}
            type="password"
            name="password"
            fullWidth
            id="password"
            error={errors?.password}
          />
        </FormRow>

        <div className={styles.footer}>
          <FormRow>
            <Button type="submit" variant="accent" center fullWidth>
              Sign in
            </Button>
          </FormRow>

          <Link href={{ pathname: "/auth", query: { mode: "sign-up" } }}>
            <a className={cn(styles.link, styles.block)}>Create new account?</a>
          </Link>
        </div>
      </form>
    </div>
  );
}
