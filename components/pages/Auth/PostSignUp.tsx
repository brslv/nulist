import { useRouter } from "next/router";
import { useEffect } from "react";
import { Title } from "../../atomic/Title/Title";
import styles from "./PostSignUp.module.css";

export function PostSignUp({ email }: { email?: string }) {
  const router = useRouter();

  useEffect(() => {
    if (!email) router.push({ pathname: "/auth", query: { mode: "sign-up" } });
  }, [email]); // eslint-disable-line

  if (!email) return null;

  return (
    <div style={{ textAlign: "center", maxWidth: "38ch" }}>
      <Title size={40} weight="semiBold" className={styles.title}>
        One last step
      </Title>
      Thanks for registering.
      <br />
      Now check your email to complete the process.{" "}
      {email ? (
        <span>
          (<strong>{email}</strong>)
        </span>
      ) : null}
    </div>
  );
}
