import styles from "./Create.module.css";
import { Page, PageMain } from "../../atomic/Page/Page";
import React, { useEffect } from "react";
import { ListEditor } from "../../ListEditor/ListEditor";
import { useUserLists } from "../../../core/list/context/UserListsProvider";
import { usePlanValidators } from "../../../core/plans/usePlanValidators";
import { usePlansModal } from "../../../core/plans/context/PlansModalProvider";
import { useAuth } from "../../../core/auth/context/AuthProvider";
import { ListsLimitReached } from "../../PlansModal/reasons/ListsLimitReached";
import { EventReasons } from "../../../core/plans/EventReasons";
import { ListVisibilities } from "../../../core/list/types/ListVisibilities";

export function Create() {
  const { isExceedingFreeListsCount } = usePlanValidators();
  const { open } = usePlansModal();
  const { user } = useAuth();
  const { lists, loading, load } = useUserLists();

  useEffect(() => {
    if (user && lists === null) {
      load(user, { visibility: ListVisibilities.All });
    }

    if (user && !loading && isExceedingFreeListsCount(lists || [])) {
      open({
        reason: <ListsLimitReached />,
        eventReason: EventReasons.ListsLimitReached,
      });
    }
  }, [isExceedingFreeListsCount, lists, load, loading, open, user]);

  return (
    <Page>
      <PageMain>
        <div className={styles.create}>
          <ListEditor />
        </div>
      </PageMain>
    </Page>
  );
}
