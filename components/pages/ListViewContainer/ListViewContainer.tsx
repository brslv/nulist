import Head from "next/head";
import React, { useEffect } from "react";
import { IList } from "../../../core/list/types/IList";
import { Page, PageMain } from "../../atomic/Page/Page";
import { ListView } from "../../ListView/ListView";
import { usePageControls } from "../../PageControls/PageControlsProvider";

interface IProps {
  list: IList;
}

export function ListViewContainer({ list }: IProps) {
  const { hide, show } = usePageControls();

  useEffect(() => {
    hide();
    return () => show();
  }, [hide, show]);

  return (
    <Page>
      <Head>
        <title key="title">{list.title}</title>
        <meta property="og:title" content={list.title} key="og:title" />
        <meta
          property="description"
          content={list.description || list.title}
          key="description"
        />
        <meta
          property="og:description"
          content={list.description || list.title}
          key="og:description"
        />
      </Head>
      <PageMain>
        <ListView list={list} />
      </PageMain>
    </Page>
  );
}
