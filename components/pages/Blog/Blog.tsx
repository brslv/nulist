import React from "react";
import { IBlogArticle } from "../../../core/blog/types/IBlogArticle";
import { PageTitle } from "../../atomic/Page/Page";
import { ArticleCard } from "../../blog/ArticleCard/ArticleCard";

interface IProps {
  articles: IBlogArticle[];
}

export function Blog({ articles }: IProps) {
  return (
    <>
      <PageTitle title="Latest articles" />
      <ul style={{ padding: 0 }}>
        {articles.map(article => (
          <li style={{ padding: 0, listStyle: "none" }} key={article.slug}>
            <ArticleCard article={article} />
          </li>
        ))}
      </ul>
    </>
  );
}
