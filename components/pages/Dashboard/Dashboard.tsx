import React from "react";
import styles from "./Dashboard.module.css";
import { PageTitle } from "../../atomic/Page/Page";
import { DashboardSections } from "../../DashboardDections/DashboardSections";

export function Dashboard() {
  return (
    <div className={styles.dashboard}>
      <PageTitle title="Dashboard" icon="dashboard" />
      <DashboardSections />
    </div>
  );
}
