import React, { FormEvent, ReactNode, useEffect, useState } from "react";
import { IconKey } from "../../../lib/icons";
import styles from "./Row.module.css";
import cn from "classnames";
import { Icon } from "../../atomic/Icon/Icon";
import { Button } from "../../atomic/Button/Button";
import { Input } from "../../atomic/Input/Input";
import { useOnKeyUp } from "../../../lib/useOnKeyUp";
import { useRouter } from "next/router";
import { PulseDot, PulseDotPosition } from "../../atomic/PulseDot/PuseDot";
import { Title } from "../../atomic/Title/Title";

export type RowEditActions = {
  stopEditing: () => void;
  startEditing: () => void;
  disable: () => void;
  enable: () => void;
};
export type OnChangeFn = (newValue: string, actions: RowEditActions) => void;
export interface IProps {
  highlightName?: string;
  name: string;
  icon: IconKey;
  value?: ReactNode;
  onChange?: OnChangeFn;
  push?: boolean;
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
const noop = () => {};

export function Row({
  highlightName,
  name,
  icon,
  value,
  onChange,
  push = true,
}: IProps) {
  const router = useRouter();
  const [highlightTooltip, setHighlightTooltip] = useState<
    string | undefined
  >();
  const [isHighlighted, setIsHighlighted] = useState(
    !!(router.query.hightlight && router.query.highlight === highlightName)
  );
  const [isEditing, setIsEditing] = useState(false);
  const [isDisabled, setIsDisabled] = useState(false);
  const [newValue, setNewValue] = useState(value || "");
  const isEditable = typeof onChange === "function";

  useEffect(() => {
    setNewValue(value || "");
  }, [value]);

  useEffect(() => {
    if (router.query.highlight && highlightName) {
      setIsHighlighted(router.query.highlight === highlightName);
      setHighlightTooltip(router.query["highlight-tooltip"] as string);
    }
  }, [highlightName, router]);

  useOnKeyUp<KeyboardEvent>({
    fn: e => {
      if (e.key === "Escape") {
        stopEditing();
      }
    },
  });

  const startEditing = () => setIsEditing(true);
  const stopEditing = () => setIsEditing(false);
  const disable = () => setIsDisabled(true);
  const enable = () => setIsDisabled(false);

  const onSubmit = (e: FormEvent) => {
    e.preventDefault();

    if (typeof newValue === "string" && typeof onChange === "function") {
      onChange(newValue, { stopEditing, startEditing, disable, enable });
    }
  };

  return (
    <div className={cn(styles.root, styles.row, { [styles.push]: push })}>
      <Icon dimmed withBox name={icon} pushBoxRight />

      <div className={cn(styles.row, styles.left)}>
        <Title
          component="h4"
          size={25}
          weight="regular"
          className={styles.name}
        >
          {name}
        </Title>
        {!isEditing ? (
          <div className={styles.valueContainer}>
            {isEditable ? (
              <Button
                center
                variant="primary"
                onClick={isEditable ? startEditing : noop}
                className={styles.btn}
                icon={!value ? "edit" : undefined}
                iconOnly={!value}
                fullWidth
              >
                {!!value ? value : null}
              </Button>
            ) : (
              <span>{!!value ? value : "---"}</span>
            )}
          </div>
        ) : null}

        {isEditing ? (
          <form onSubmit={isDisabled ? noop : onSubmit}>
            <div className={styles.row}>
              <Input
                disabled={isDisabled}
                autoFocus
                value={typeof newValue === "string" ? newValue : ""}
                onChange={e => setNewValue(e.target.value)}
                placeholder="Type and hit enter..."
              />
              <div className={styles.closeFormBtn}>
                <Button
                  onClick={stopEditing}
                  type="button"
                  variant="basic"
                  icon="close"
                  iconOnly
                />
              </div>
            </div>
          </form>
        ) : null}

        <div className={styles.pulseDotContainer}>
          <PulseDot
            position={PulseDotPosition.TopRight}
            isActive={isHighlighted}
            tooltip={highlightTooltip}
          />
        </div>
      </div>
    </div>
  );
}
