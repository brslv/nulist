import React from "react";
import { IUser } from "../../../core/auth/types/IUser";
import { PageTitle } from "../../atomic/Page/Page";
import { IUserProfile } from "../../../core/auth/types/IUserProfile";
import { OnChangeFn, Row } from "./Row";
import { useAuthenticatedUserProfile } from "../../../core/profile/context/AuthenticatedUserProfileProvider";
import { DateFormats, formatDate } from "../../../lib/formatDate";
import styles from "./Profile.module.css";

interface IProps {
  user: IUser;
  profile: IUserProfile | null;
}

export function Profile({ user, profile }: IProps) {
  const { update } = useAuthenticatedUserProfile();
  const createdAt = formatDate(user.created_at);

  const onNameChange: OnChangeFn = async (newValue, actions) => {
    actions.disable();

    try {
      await update({
        name: newValue,
      });
    } finally {
      actions.enable();
      actions.stopEditing();
    }
  };

  return (
    <div>
      <PageTitle
        icon="profile"
        title={profile?.name ? `Hello, ${profile.name}` : "Profile"}
      />

      <div className={styles.container}>
        <Row
          highlightName="name"
          name="Your name"
          icon="name"
          value={profile?.name}
          onChange={onNameChange}
        />

        <Row name="Email" icon="email" value={user.email} />

        <Row name="Registered on" icon="calendar" value={createdAt} />

        <Row
          push={false}
          name="Plan"
          icon="star"
          value={
            profile?.isPro ? (
              <span
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-end",
                }}
              >
                <div>Pro plan</div>
                {profile.proStartedAt ? (
                  <span style={{ fontSize: 10 }}>
                    Start date:{" "}
                    {formatDate(
                      profile?.proStartedAt,
                      DateFormats.ShortMonthDayYearHr
                    )}
                  </span>
                ) : null}
              </span>
            ) : (
              "Free plan"
            )
          }
        />
      </div>
    </div>
  );
}
