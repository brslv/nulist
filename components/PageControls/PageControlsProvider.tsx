import { useRouter } from "next/router";
import {
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import { useAuth } from "../../core/auth/context/AuthProvider";
import { IUser } from "../../core/auth/types/IUser";
import { IUserProfile } from "../../core/auth/types/IUserProfile";
import {
  OnboardingModals,
  useOnboardingModals,
} from "../../core/onboarding/modals/OnboardingModalsProvider";
import { usePlansModal } from "../../core/plans/context/PlansModalProvider";
import { EventReasons } from "../../core/plans/EventReasons";
import { useAuthenticatedUserProfile } from "../../core/profile/context/AuthenticatedUserProfileProvider";

interface IProps {
  children: ReactNode;
}

interface ICtx {
  isVisible: boolean;
  isStripped: boolean;
  itemsVisibility: {
    blog: boolean;
    authButton: boolean;
    feedback: boolean;
    nulistProPlan: boolean;
    user: boolean;
    goToDashboard: boolean;
  };
  isUserMenuOpen: boolean;
  show: () => void;
  hide: () => void;
  strip: () => void;
  unstrip: () => void;
  openUserMenu: () => void;
  closeUserMenu: () => void;
  onSignupClick: () => void;
  onSigninClick: () => void;
  onDashboardClick: () => void;
  onProfileClick: () => void;
  onSignOutClick: () => void;
  onUnlockProClick: () => void;
  onQuickHelpClick: () => void;
  onGoToDashboardClick: () => void;
  showGoToDashboardBtn: () => void;
  hideGoToDashboardBtn: () => void;
}

const PageControlsContext = createContext<ICtx | null>(null);

function getItemsVisibilityState(
  user: IUser | null,
  profile: IUserProfile | null,
  pathname: string,
  isStripped: boolean
) {
  const hasUser = !!user;
  const isPro = profile?.isPro;

  return {
    blog: !isStripped,
    authButton: !hasUser,
    feedback: !isStripped && hasUser,
    nulistProPlan: hasUser && !isPro,
    user: hasUser,
    goToDashboard:
      (pathname === "/list/[uid]/edit" || pathname === "/") && hasUser,
  };
}

export function PageControlsProvider({ children }: IProps) {
  const router = useRouter();
  const { user, signOut } = useAuth();
  const { profile } = useAuthenticatedUserProfile();
  const [isVisible, setIsVisible] = useState(true);
  const [isStripped, setIsStripped] = useState(false);
  const [itemsVisibility, setItemsVisibility] = useState(
    getItemsVisibilityState(user, profile, router.pathname, isStripped)
  );
  const [isUserMenuOpen, setIsUserMenuOpen] = useState(false);
  const { open: openPlansModal } = usePlansModal();
  const { open: openOnboardingModal } = useOnboardingModals();

  useEffect(() => {
    setItemsVisibility(
      getItemsVisibilityState(user, profile, router.pathname, isStripped)
    );
  }, [profile, user, router.pathname, isStripped]);

  const show = () => setIsVisible(true);
  const hide = () => setIsVisible(false);
  const strip = useCallback(() => setIsStripped(true), []);
  const unstrip = useCallback(() => setIsStripped(false), []);
  const openUserMenu = () => setIsUserMenuOpen(true);
  const closeUserMenu = () => setIsUserMenuOpen(false);
  const showGoToDashboardBtn = useCallback(
    () => setItemsVisibility(prev => ({ ...prev, goToDashboard: true })),
    []
  );
  const hideGoToDashboardBtn = useCallback(
    () => setItemsVisibility(prev => ({ ...prev, goToDashboard: false })),
    []
  );

  const onSignupClick = () => {
    router.push("/auth");
  };
  const onSigninClick = () => {
    router.push("/auth?mode=sign-in");
  };
  const onDashboardClick = () => {
    router.push("/dashboard");
    setIsUserMenuOpen(false);
  };
  const onProfileClick = () => {
    router.push("/profile");
    setIsUserMenuOpen(false);
  };
  const onSignOutClick = () => {
    signOut();
    setIsUserMenuOpen(false);
  };
  const onUnlockProClick = () => {
    openPlansModal({
      title: "Nulist Pro",
      eventReason: EventReasons.UserInitiated,
    });
  };
  const onQuickHelpClick = () => {
    openOnboardingModal(OnboardingModals.Help);
    setIsUserMenuOpen(false);
  };
  const onGoToDashboardClick = () => {
    hideGoToDashboardBtn();
    router.push("/dashboard");
  };

  return (
    <PageControlsContext.Provider
      value={{
        isVisible,
        isStripped,
        itemsVisibility,
        isUserMenuOpen,
        show,
        hide,
        strip,
        unstrip,
        openUserMenu,
        closeUserMenu,
        onSignupClick,
        onSigninClick,
        onDashboardClick,
        onProfileClick,
        onSignOutClick,
        onUnlockProClick,
        onQuickHelpClick,
        onGoToDashboardClick,
        showGoToDashboardBtn,
        hideGoToDashboardBtn,
      }}
    >
      {children}
    </PageControlsContext.Provider>
  );
}

export function usePageControls() {
  const ctx = useContext(PageControlsContext);
  if (!ctx) throw new Error("Improper use of PageControlsProvider.");
  return ctx;
}
