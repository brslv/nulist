import Link from "next/link";
import React, { useRef } from "react";
import { useAuth } from "../../core/auth/context/AuthProvider";
import {
  OnboardingModals,
  useOnboardingModals,
} from "../../core/onboarding/modals/OnboardingModalsProvider";
import { Button } from "../atomic/Button/Button";
import { ContextMenu } from "../atomic/ContextMenu/ContextMenu";
import { ContextMenuContainer } from "../atomic/ContextMenu/ContextMenuContainer";
import { ContextMenuItem } from "../atomic/ContextMenu/ContextMenuItem";
import { ContextMenuSeparator } from "../atomic/ContextMenu/ContextMenuSeparator";
import { ContextMenuTitle } from "../atomic/ContextMenu/ContextMenuTitle";
import styles from "./PageControlsDesktop.module.css";
import { usePageControls } from "./PageControlsProvider";

export function PageControlsDesktop() {
  const userProfileBtnRef = useRef(null);
  const { user } = useAuth();
  const {
    itemsVisibility,
    isUserMenuOpen,
    onSigninClick,
    onDashboardClick,
    onSignOutClick,
    onProfileClick,
    onUnlockProClick,
    onQuickHelpClick,
    openUserMenu,
    closeUserMenu,
  } = usePageControls();
  const { open } = useOnboardingModals();

  return (
    <div className={styles.pageControlsDesktop}>
      <div className={styles.links}>
        {itemsVisibility.blog ? (
          <Link href="/blog" passHref>
            <Button component="a" variant="nostyle" narrow>
              Blog
            </Button>
          </Link>
        ) : null}
        {itemsVisibility.authButton ? (
          <Button variant="nostyle" onClick={() => onSigninClick()}>
            Sign in
          </Button>
        ) : null}
        {itemsVisibility.feedback ? (
          <Button
            variant="nostyle"
            narrow
            onClick={() => open(OnboardingModals.Feedback)}
          >
            Feedback
          </Button>
        ) : null}
        {itemsVisibility.nulistProPlan ? (
          <Button
            variant="nostyle"
            narrow
            className="gumroad-button"
            onClick={onUnlockProClick}
          >
            Unlock Pro
          </Button>
        ) : null}
        {itemsVisibility.user ? (
          <div>
            <Button
              ref={userProfileBtnRef}
              icon="user"
              iconOnly
              className={styles.btn}
              onClick={openUserMenu}
            />
            <ContextMenu
              isOpen={isUserMenuOpen}
              onOutsideClick={closeUserMenu}
              style={{ right: 20 }}
              addTopOffset={30}
              anchorRef={userProfileBtnRef}
            >
              <ContextMenuContainer>
                <ContextMenuTitle>{user?.email}</ContextMenuTitle>
                <ContextMenuItem
                  fullWidth
                  icon="dashboard"
                  onClick={onDashboardClick}
                >
                  Dashboard
                </ContextMenuItem>
                <ContextMenuItem
                  fullWidth
                  icon="profile"
                  onClick={onProfileClick}
                >
                  Profile
                </ContextMenuItem>
                <ContextMenuSeparator />
                <ContextMenuItem
                  fullWidth
                  icon="questionmark"
                  onClick={onQuickHelpClick}
                >
                  Quick help
                </ContextMenuItem>

                <ContextMenuSeparator />
                <ContextMenuItem
                  fullWidth
                  icon="logout"
                  onClick={onSignOutClick}
                >
                  Log out
                </ContextMenuItem>
              </ContextMenuContainer>
            </ContextMenu>
          </div>
        ) : null}
      </div>
    </div>
  );
}
