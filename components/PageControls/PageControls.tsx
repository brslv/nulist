import Image from "next/image";
import Link from "next/link";
import React from "react";
import { Button } from "../atomic/Button/Button";
import styles from "./PageControls.module.css";
import { PageControlsDesktop } from "./PageControlsDesktop";
import { PageControlsMobile } from "./PageControlsMobile";
import { usePageControls } from "./PageControlsProvider";
import cn from "classnames";

export function PageControls() {
  const { isVisible, isStripped, itemsVisibility, onGoToDashboardClick } =
    usePageControls();

  if (!isVisible) return null;

  return (
    <div className={cn(styles.pageControls, { [styles.stripped]: isStripped })}>
      <div className={styles.left}>
        <div className={styles.logoContainer}>
          <Link href="/dashboard">
            <a style={{ display: "flex", cursor: "pointer" }}>
              <Image
                src="/logo-small.svg"
                alt="Nulist logo"
                width="70"
                height="23.73"
              />
            </a>
          </Link>
        </div>
        {itemsVisibility.goToDashboard ? (
          <Button
            icon="goBack"
            variant="plain"
            narrow
            rootClassName={styles.goToDashboardBtnRoot}
            className={styles.goToDashboardBtn}
            onClick={onGoToDashboardClick}
          >
            Go to dashboard
          </Button>
        ) : null}
      </div>

      <PageControlsDesktop />
      <PageControlsMobile />
    </div>
  );
}
