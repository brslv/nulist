import Link from "next/link";
import React, { useRef, useState } from "react";
import { Button } from "../atomic/Button/Button";
import { CSSTransition } from "react-transition-group";
import styles from "./PageControlsMobile.module.css";
import { Icon } from "../atomic/Icon/Icon";
import {
  OnboardingModals,
  useOnboardingModals,
} from "../../core/onboarding/modals/OnboardingModalsProvider";
import { usePageControls } from "./PageControlsProvider";

export function PageControlsMobile() {
  const { open: openOnboardingModal } = useOnboardingModals();
  const {
    itemsVisibility,
    onSignOutClick,
    onSigninClick,
    onUnlockProClick,
    onQuickHelpClick,
  } = usePageControls();
  const [isOpen, setIsOpen] = useState(false);
  const ref = useRef(null);
  const open = () => setIsOpen(true);
  const close = () => setIsOpen(false);

  return (
    <div className={styles.pageControlsMobile}>
      <Button variant="basic" icon="menu" iconOnly onClick={open} />
      <CSSTransition
        nodeRef={ref}
        in={isOpen}
        classNames={{
          enter: styles.animEnter,
          enterActive: styles.animEnterActive,
          exit: styles.animExit,
          exitActive: styles.animExitActive,
        }}
        timeout={200}
        unmountOnExit
      >
        <div ref={ref} className={styles.menu}>
          <div className={styles.menuInner} onClickCapture={close}>
            <Button
              variant="basic"
              icon="close"
              iconOnly
              rootClassName={styles.closeBtn}
              onClick={close}
            />
            <Link href="/blog" passHref>
              <a className={styles.link}>
                <Icon name="blog" withBox pushBoxRight />
                <span>Blog</span>
              </a>
            </Link>
            {itemsVisibility.authButton ? (
              <div tabIndex={0} onClick={onSigninClick} className={styles.link}>
                <Icon name="signUp" withBox pushBoxRight />
                <span>Sign in</span>
              </div>
            ) : null}
            {itemsVisibility.user ? (
              <>
                <Link href="/dashboard">
                  <a className={styles.link}>
                    <Icon name="dashboard" withBox pushBoxRight />
                    <span>Dashboard</span>
                  </a>
                </Link>
                <Link href="/profile">
                  <a className={styles.link}>
                    <Icon name="profile" withBox pushBoxRight />
                    <span>Profile</span>
                  </a>
                </Link>
                <div
                  tabIndex={0}
                  onClick={onSignOutClick}
                  className={styles.link}
                >
                  <Icon name="logout" withBox pushBoxRight />
                  <span>Log out</span>
                </div>
              </>
            ) : null}

            <hr />
            {itemsVisibility.user ? (
              <div
                tabIndex={0}
                onClick={onQuickHelpClick}
                className={styles.link}
              >
                <Icon name="questionmark" withBox pushBoxRight />
                <span>Quick help</span>
              </div>
            ) : null}
            <div
              className={styles.link}
              tabIndex={0}
              onClick={() => openOnboardingModal(OnboardingModals.Feedback)}
            >
              <Icon name="heart" withBox pushBoxRight />
              <span>Feedback</span>
            </div>

            {itemsVisibility.nulistProPlan ? (
              <>
                <hr />
                <Button
                  fullWidth
                  variant="accent"
                  center
                  onClick={onUnlockProClick}
                >
                  Unlock Pro
                </Button>
              </>
            ) : null}
          </div>
        </div>
      </CSSTransition>
    </div>
  );
}
