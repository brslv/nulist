import React, { MutableRefObject, ReactNode, useState, useRef } from "react";
import styles from "./ControlsRow.module.css";
import cn from "classnames";
import { Button } from "../atomic/Button/Button";
import { Confirmation } from "../atomic/Confirmation/Confirmation";
import { ContextMenu } from "../atomic/ContextMenu/ContextMenu";
import { ContextMenuContainer } from "../atomic/ContextMenu/ContextMenuContainer";
import { ContextMenuItem } from "../atomic/ContextMenu/ContextMenuItem";
import { ContextMenuTitle } from "../atomic/ContextMenu/ContextMenuTitle";
import { useMediaQuery } from "@react-hook/media-query";
import { IconKey } from "../../lib/icons";

interface IChildrenFnProps {
  deleteBtnRef: MutableRefObject<HTMLButtonElement | undefined>;
  menuBtnRef: MutableRefObject<HTMLButtonElement | undefined>;
}

interface IProps {
  children: ({ deleteBtnRef, menuBtnRef }: IChildrenFnProps) => ReactNode;
  onDelete?: () => void;
  onMenu?: () => void;
  onAddBellow?: () => void;
  align?: "center" | "flex-start";
  fullWidth?: boolean;
  active?: boolean;
  className?: any;
  controlsClassName?: any;
  controlsStyle?: any;
  icons?: {
    add?: IconKey;
    remove?: IconKey;
    menu?: IconKey;
  };
  big?: boolean;
}

const defaultIcons: Required<IProps["icons"]> = {
  add: "add",
  remove: "trash",
  menu: "menu",
};

export function ControlsRow({
  children,
  onDelete,
  onMenu,
  onAddBellow,
  align = "center",
  fullWidth = false,
  active = false,
  className,
  controlsClassName,
  controlsStyle = {},
  icons = defaultIcons,
  big = false,
}: IProps) {
  const isMobile = useMediaQuery("only screen and (max-width: 950px)");
  const [isDeleteConfirmationOpen, setIsDeleteConfirmationOpen] =
    useState(false);
  const deleteBtnRef = useRef<HTMLButtonElement>();
  const menuBtnRef = useRef<HTMLButtonElement>();
  const mobileContextMenuBtnRef = useRef<HTMLButtonElement>();
  const [isMobileContextMenuOpen, setIsMobileContextMenuOpen] = useState(false);
  const isOnlyOneButtonActive =
    [onDelete, onMenu, onAddBellow].reduce((acc, fn) => {
      if (typeof fn === "function") acc++;
      return acc;
    }, 0) === 1;

  const internalOnDelete = () => {
    setIsDeleteConfirmationOpen(true);
  };

  const onMobileContextMenuOutsideClick = () => {
    setIsMobileContextMenuOpen(false);
  };

  const onMobileContextMenuItemClick =
    (type: "delete" | "add-bellow" | "menu") => () => {
      if (type === "delete" && typeof onDelete === "function") onDelete();
      if (type === "add-bellow" && typeof onAddBellow === "function")
        onAddBellow();
      if (type === "menu" && typeof onMenu === "function") onMenu();
      setIsMobileContextMenuOpen(false);
    };

  return (
    <div
      className={cn(styles.controlsRow, {
        [styles.active]: active,
        [styles.fullWidth]: fullWidth,
        className,
      })}
      style={{ alignItems: align }}
    >
      <div
        className={cn(styles.controls, controlsClassName)}
        style={controlsStyle}
      >
        {typeof onDelete === "function" ? (
          <div className={styles.control}>
            <Button
              ref={deleteBtnRef}
              variant="plain"
              icon={icons?.remove || defaultIcons?.remove}
              iconOnly
              dimmed
              narrow
              onClick={internalOnDelete}
              iconProps={{ className: cn({ [styles.big]: big }) }}
            />
          </div>
        ) : null}
        {typeof onAddBellow === "function" ? (
          <div className={styles.control}>
            <Button
              ref={deleteBtnRef}
              variant="plain"
              icon={icons?.add || defaultIcons?.add}
              iconOnly
              dimmed
              narrow
              onClick={onAddBellow}
              iconProps={{ className: cn({ [styles.big]: big }) }}
            />
          </div>
        ) : null}
        {typeof onMenu === "function" ? (
          <div className={styles.control}>
            <Button
              ref={menuBtnRef}
              variant="plain"
              icon={icons?.menu || defaultIcons?.menu}
              iconOnly
              dimmed
              narrow
              onClick={onMenu}
              iconProps={{ className: cn({ [styles.big]: big }) }}
            />
          </div>
        ) : null}
      </div>

      <Confirmation
        isOpen={isDeleteConfirmationOpen}
        onCancel={() => setIsDeleteConfirmationOpen(false)}
        onOk={() => {
          if (typeof onDelete === "function") onDelete();
        }}
      >
        Removing a list item is permanent. Are you sure?
      </Confirmation>

      {children({
        deleteBtnRef,
        menuBtnRef: isMobile ? mobileContextMenuBtnRef : menuBtnRef,
      })}

      <div className={styles.mobileControls}>
        {isOnlyOneButtonActive ? (
          <>
            {typeof onDelete === "function" ? (
              <div className={styles.control}>
                <Button
                  ref={mobileContextMenuBtnRef}
                  variant="plain"
                  icon={icons?.remove || defaultIcons?.remove}
                  iconOnly
                  dimmed
                  narrow
                  onClick={internalOnDelete}
                  iconProps={{ className: cn({ [styles.big]: big }) }}
                />
              </div>
            ) : null}
            {typeof onAddBellow === "function" ? (
              <div className={styles.control}>
                <Button
                  ref={mobileContextMenuBtnRef}
                  variant="plain"
                  icon={icons?.add || defaultIcons?.add}
                  iconOnly
                  dimmed
                  narrow
                  onClick={onAddBellow}
                  iconProps={{ className: cn({ [styles.big]: big }) }}
                />
              </div>
            ) : null}
            {typeof onMenu === "function" ? (
              <div className={styles.control}>
                <Button
                  ref={mobileContextMenuBtnRef}
                  variant="plain"
                  icon={icons?.menu || defaultIcons?.menu}
                  iconOnly
                  dimmed
                  narrow
                  onClick={onMenu}
                  iconProps={{ className: cn({ [styles.big]: big }) }}
                />
              </div>
            ) : null}
          </>
        ) : (
          <Button
            ref={mobileContextMenuBtnRef}
            icon="menu"
            iconOnly
            variant="plain"
            dimmed
            narrow
            onClick={() => setIsMobileContextMenuOpen(true)}
            iconProps={{ className: cn({ [styles.big]: big }) }}
          />
        )}

        <ContextMenu
          onOutsideClick={onMobileContextMenuOutsideClick}
          isOpen={isMobileContextMenuOpen}
          anchorRef={mobileContextMenuBtnRef}
          addTopOffset={10}
          style={{ right: 0 }}
        >
          <ContextMenuContainer>
            <ContextMenuTitle>Actions</ContextMenuTitle>
            {typeof onDelete === "function" ? (
              <ContextMenuItem
                icon={icons?.remove || defaultIcons?.remove}
                fullWidth
                onClick={onMobileContextMenuItemClick("delete")}
              >
                Delete
              </ContextMenuItem>
            ) : null}
            {typeof onAddBellow === "function" ? (
              <ContextMenuItem
                icon={icons?.add || defaultIcons?.add}
                fullWidth
                onClick={onMobileContextMenuItemClick("add-bellow")}
              >
                Add item bellow
              </ContextMenuItem>
            ) : null}
            {typeof onMenu === "function" ? (
              <ContextMenuItem
                icon={icons?.menu || defaultIcons?.menu}
                fullWidth
                onClick={onMobileContextMenuItemClick("menu")}
              >
                Edit properties
              </ContextMenuItem>
            ) : null}
          </ContextMenuContainer>
        </ContextMenu>
      </div>
    </div>
  );
}
