import React, { ReactNode, useRef, useState } from "react";
import { Button } from "../../atomic/Button/Button";
import { Title } from "../../atomic/Title/Title";
import { CSSTransition } from "react-transition-group";
import styles from "./ListSettings.module.css";
import { useListEditor } from "../../../core/list/context/ListEditorProvider";
import { PulseDot, PulseDotPosition } from "../../atomic/PulseDot/PuseDot";

interface IProps {
  children: ReactNode;
}

export function ListSettings({ children }: IProps) {
  const ref = useRef(null);
  const settingsBtnRef = useRef(null);
  const { isPublic } = useListEditor();
  const [isMinimizedRowVisible, setIsMinimizedRowVisible] = useState(true);
  const [isMinimized, setIsMinimized] = useState(true);

  return (
    <>
      <div ref={ref} className={styles.listSettings}>
        <CSSTransition
          nodeRef={settingsBtnRef}
          in={isMinimizedRowVisible}
          classNames={{
            enter: styles.animEnter,
            enterActive: styles.animEnterActive,
            exit: styles.animExit,
            exitActive: styles.animExitActive,
          }}
          timeout={200}
          onExited={() => setIsMinimized(false)}
          unmountOnExit
        >
          <div ref={settingsBtnRef} className={styles.minimizedRow}>
            {isPublic ? (
              <div>
                <PulseDot
                  pulse={false}
                  isActive={true}
                  position={PulseDotPosition.TopRight}
                  tooltip="List is public"
                />
              </div>
            ) : null}
            <Button
              dimmed
              variant="nostyle"
              icon="settings"
              iconOnly
              narrow
              iconProps={{ className: styles.settingsIcon }}
              rootClassName={styles.settingsBtn}
              onClick={() => setIsMinimizedRowVisible(false)}
            />
          </div>
        </CSSTransition>

        <CSSTransition
          nodeRef={ref}
          in={!isMinimized}
          classNames={{
            enter: styles.animEnter,
            enterActive: styles.animEnterActive,
            exit: styles.animExit,
            exitActive: styles.animExitActive,
          }}
          timeout={200}
          onExited={() => setIsMinimizedRowVisible(true)}
          unmountOnExit
        >
          <div ref={ref} className={styles.container}>
            <div className={styles.head}>
              <Title component="div" weight="regular" uppercase>
                List settings
              </Title>

              <Button
                narrow
                variant="basic"
                icon="minimize"
                iconOnly
                onClick={() => setIsMinimized(true)}
              />
            </div>

            <div className={styles.content}>{children}</div>
          </div>
        </CSSTransition>
      </div>
    </>
  );
}
