import React from "react";
import { useBundlesManager } from "../../../core/bundles/context/BundlesManagerProvider";
import { useBundles } from "../../../core/bundles/context/BundlesProvider";
import { useListBundles } from "../../../core/bundles/context/ListBundlesProvider";
import { useListEditor } from "../../../core/list/context/ListEditorProvider";
import { Button } from "../../atomic/Button/Button";
import { Icon } from "../../atomic/Icon/Icon";
import { Label } from "../../atomic/Label/Label";
import Toggle from "../../atomic/Toggle/Toggle";
import styles from "./ListSettingsManager.module.css";

export function ListSettingsManager() {
  const { open: openBundlesManager } = useBundlesManager();
  const { isPublic, updateIsPublic } = useListEditor();
  const { bundles } = useBundles();
  const { selectedBundles } = useListBundles();

  const onToggle = () => updateIsPublic(!isPublic);

  return (
    <div>
      <div className={styles.row}>
        <div className={styles.left}>
          <div>
            <Icon name="people" dimmed withBox pushBoxRight />
          </div>
          <div className={styles.label}>
            <Label>Public</Label>
          </div>
        </div>
        <div className={styles.right}>
          <Toggle isOn={isPublic} onToggle={onToggle} />
        </div>
      </div>
      <div className={styles.row}>
        <div className={styles.left}>
          <div>
            <Icon name="bundle" dimmed withBox pushBoxRight />
          </div>
          <div className={styles.label}>
            <Label>Bundles</Label>{" "}
            <span className={styles.selectedBundlesCount}>
              ({selectedBundles.length})
            </span>
            <div className={styles.selectedBundlesList}>
              {selectedBundles
                .slice(0, 2)
                .map(id => bundles.find(bundle => bundle.id === id)?.title)
                .join(", ")}
              {selectedBundles.length > 2 ? `...` : null}
              {!selectedBundles.length ? "No bundles selected" : null}
            </div>
          </div>
        </div>
        <div className={styles.right}>
          <Button
            variant="basic"
            icon="edit"
            onClick={openBundlesManager}
            fontSize={10}
          >
            Edit
          </Button>
        </div>
      </div>
    </div>
  );
}
