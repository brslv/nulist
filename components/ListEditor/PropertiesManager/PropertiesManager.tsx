import React, { FormEvent, useRef } from "react";
import { useListEditor } from "../../../core/list/context/ListEditorProvider";
import { useProperties } from "../../../core/list/hooks/useProperties";
import { ListItemProperties } from "../../../core/list/types/ListItemProperties";
import { useOnKeyDown } from "../../../lib/useOnKeyDown";
import { Button } from "../../atomic/Button/Button";
import { Input } from "../../atomic/Input/Input";
import { RichEditor } from "../../atomic/RichEditor/RichEditor";
import styles from "./PropertiesManager.module.css";

interface IProps {
  onCancel: () => void;
}

export function PropertiesManager({ onCancel }: IProps) {
  const urlRef = useRef(null);
  const { managingProperties, startEditing: startEditingListItem } =
    useListEditor();
  const {
    state: propertiesState,
    updateProperty,
    save,
  } = useProperties(managingProperties);

  useOnKeyDown({
    fn: (e: KeyboardEvent) => {
      if (e.key === "Enter" && e.shiftKey) {
        submitForm();
        e.preventDefault();
        e.stopPropagation();
      }
    },
  });

  const onChange = (name: ListItemProperties, value: string) => {
    updateProperty(name, value);
  };

  const onSubmit = (e: FormEvent) => {
    e.preventDefault();
    submitForm();
  };

  const submitForm = () => {
    if (managingProperties?.id) startEditingListItem(managingProperties.id);
    onCancel();
    save();
  };

  return (
    <form onSubmit={onSubmit}>
      <div className={styles.propertiesManager}>
        <div className={styles.main}>
          <label className={styles.content}>
            <RichEditor
              autoFocus
              value={propertiesState.content}
              onUpdate={value => onChange("content", value)}
            />
            <p className={styles.markdownHint}>
              This editor supports some basic Markdown formatting.
            </p>
          </label>

          <Input
            fullWidth
            ref={urlRef}
            type="text"
            name="url"
            placeholder="https://"
            value={propertiesState.url}
            onChange={e =>
              onChange(e.target.name as ListItemProperties, e.target.value)
            }
          />

          <div className={styles.footer}>
            <Button
              type="submit"
              variant="accent"
              icon="save"
              rootClassName={styles.submitBtn}
            >
              Save
            </Button>

            <Button variant="plain" onClick={onCancel}>
              Cancel
            </Button>
          </div>
        </div>
      </div>
    </form>
  );
}
