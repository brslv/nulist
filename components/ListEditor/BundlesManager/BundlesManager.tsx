import Link from "next/link";
import React, { KeyboardEvent, useState } from "react";
import { useAuth } from "../../../core/auth/context/AuthProvider";
import { useBundlesManager } from "../../../core/bundles/context/BundlesManagerProvider";
import { useBundles } from "../../../core/bundles/context/BundlesProvider";
import { useListBundles } from "../../../core/bundles/context/ListBundlesProvider";
import { IBundle } from "../../../core/bundles/types/IBundle";
import { useOnKeyDown } from "../../../lib/useOnKeyDown";
import { WithTooltip } from "../../../lib/WithTooltip";
import { Button } from "../../atomic/Button/Button";
import { Checkbox } from "../../atomic/Checkbox/Checkbox";
import { Confirmation } from "../../atomic/Confirmation/Confirmation";
import { Icon } from "../../atomic/Icon/Icon";
import { Input } from "../../atomic/Input/Input";
import { Modal } from "../../atomic/Modal/Modal";
import { ModalContent } from "../../atomic/Modal/ModalContent";
import { ModalHeading } from "../../atomic/Modal/ModalHeading";
import styles from "./BundlesManager.module.css";

export function BundlesManager() {
  const { user } = useAuth();
  const [searchValue, setSearchValue] = useState<string | null>(null);
  const [isDeleteConfirmationOpen, setIsDeleteConfirmationOpen] =
    useState(false);
  const [queuedForDeletion, setQueuedForDeletion] = useState<IBundle | null>(
    null
  );
  const { bundles, loading, isPlansModalOpen, addBundle, removeBundle } =
    useBundles();
  const { select, toggle, deselect, isSelected, updateBundles } =
    useListBundles();
  const { isOpen, close, toggle: toggleBundlesManager } = useBundlesManager();

  const _toggleBundlesManager = () => {
    updateBundles();
    toggleBundlesManager();
  };

  const onClose = () => {
    if (isDeleteConfirmationOpen || isPlansModalOpen) return;

    updateBundles();
    close();
  };

  const filteredBundles = bundles.filter(bundle =>
    bundle.title
      .toLowerCase()
      .startsWith((searchValue || "").toLocaleLowerCase())
  );

  const onCreate = async () => {
    const bundle = await addBundle({
      title: searchValue || "",
      isPublic: false,
    });

    if (bundle) {
      select(bundle);
      setSearchValue(null);
    }
  };

  const onKeyUp = (e: KeyboardEvent<HTMLInputElement>) => {
    if (loading) return;
    if (e.key === "Enter") onCreate();
  };

  const onDelete = (bundle: IBundle) => {
    setQueuedForDeletion(bundle);
    setIsDeleteConfirmationOpen(true);
  };

  useOnKeyDown<KeyboardEvent>({
    fn: e => {
      if (e.key === "Escape") onClose();
      if ((e.ctrlKey || e.metaKey) && e.key.toLowerCase() === "b")
        _toggleBundlesManager();
    },
  });

  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose} focusTrap={true}>
        <ModalHeading onClose={onClose}>
          <div style={{ display: "flex", alignItems: "center" }}>
            <span style={{ marginRight: 5 }}>Bundles</span>
            <WithTooltip content="Think of bundles as collections of lists. They help you stay organized and share lists in bulk with your friends.">
              <Icon name="info" />
            </WithTooltip>
          </div>
        </ModalHeading>

        {/* Logged in user */}

        {user ? (
          <ModalContent noSpace>
            <Input
              value={searchValue || ""}
              onChange={e => setSearchValue(e.target.value)}
              autoFocus
              placeholder="Search or create bundle..."
              className={styles.input}
              fullWidth
              maxLength={50}
              onKeyUp={onKeyUp}
            />
            {filteredBundles.map(bundle => {
              return (
                <label className={styles.bundle} key={bundle.id}>
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <Checkbox
                      checked={isSelected(bundle)}
                      onChange={() => toggle(bundle)}
                    />
                    {bundle.title}
                  </div>
                  <Button
                    icon="trash"
                    iconOnly
                    narrow
                    dimmed
                    onClick={() => onDelete(bundle)}
                  />
                </label>
              );
            })}

            {!bundles.length && (searchValue === null || searchValue === "") ? (
              <div className={styles.noBundlesMsg}>
                <Icon name="info" withBox pushBoxRight />
                <span>
                  Create your first bundle by typing anything in the input
                  above.
                </span>
              </div>
            ) : null}

            {searchValue !== null &&
            searchValue !== "" &&
            !filteredBundles.find(
              bundle => bundle.title.toLowerCase() === searchValue.toLowerCase()
            ) ? (
              <Button
                icon="add"
                fullWidth
                variant="nostyle"
                rootClassName={styles.createBtn}
                onClick={onCreate}
                disabled={loading}
              >
                Create {searchValue}
              </Button>
            ) : null}
          </ModalContent>
        ) : null}

        {/* Guest user */}

        {!user ? (
          <ModalContent className={styles.guestModalContent}>
            <p style={{ marginTop: 0 }}>
              Think of bundles as collections of lists. They help you stay
              organized and share lists in bulk with your friends.
            </p>

            <Link href="/auth" passHref>
              <Button component="a" variant="accent" center>
                Sign in to create bundle
              </Button>
            </Link>
          </ModalContent>
        ) : null}
      </Modal>

      <Confirmation
        isOpen={isDeleteConfirmationOpen}
        onCancel={() => {
          setIsDeleteConfirmationOpen(false);
          setQueuedForDeletion(null);
        }}
        onOk={() => {
          if (!queuedForDeletion) return;
          deselect(queuedForDeletion);
          removeBundle(queuedForDeletion.id);
          setQueuedForDeletion(null);
          setIsDeleteConfirmationOpen(false);
        }}
      >
        <span style={{ display: "block", maxWidth: "42ch" }}>
          <p style={{ marginTop: 0 }}>Removing a bundle is permanent.</p>
          <p>
            All other lists associated with this bundle will get removed from it
            as well.
          </p>
          <p style={{ marginBottom: 0 }}>Are you sure you want to continue?</p>
        </span>
      </Confirmation>
    </>
  );
}
