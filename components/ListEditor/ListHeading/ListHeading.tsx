import React, { FormEvent, useEffect, useState } from "react";
import styles from "./ListHeading.module.css";
import cn from "classnames";
import { Textarea } from "../../atomic/Textarea/Textarea";
import { EmojiPicker } from "./EmojiPicker";
import { useListEditor } from "../../../core/list/context/ListEditorProvider";
import { ListSettings } from "../ListSettings/ListSettings";
import { ListSettingsManager } from "../ListSettings/ListSettingsManager";

export function ListHeading() {
  const {
    errors,
    updateTitle,
    updateDescription,
    emoji,
    updateEmoji,
    loading,
  } = useListEditor();
  const listEditor = useListEditor();
  const [title, setTitle] = useState(listEditor.title);
  const [description, setDescription] = useState(listEditor.description);

  useEffect(() => {
    setTitle(listEditor.title);
    setDescription(listEditor.description);
  }, [listEditor.description, listEditor.title]);

  const onChange = (e: FormEvent<HTMLTextAreaElement>) => {
    const target = e.currentTarget;
    const name = target.name;
    const val = target.value;

    if (name === "title") setTitle(val);
    if (name === "description") setDescription(val);
  };

  const onBlur = (name: "title" | "description") => () => {
    if (name === "title") updateTitle(title);
    if (name === "description") updateDescription(description);
  };

  return (
    <>
      <div className={styles.titleContainer}>
        <div style={{ position: "relative" }}>
          <ListSettings>
            <ListSettingsManager />
          </ListSettings>
        </div>
        <EmojiPicker
          skeleton={loading}
          skeletonProps={{ width: 100, height: 100 }}
          onChange={updateEmoji}
          value={emoji}
        />
        <>
          <Textarea
            skeleton={loading}
            skeletonProps={{ height: 50 }}
            autoFocus
            value={title}
            name="title"
            onChange={onChange}
            className={styles.title}
            placeholder="Title"
            maxSymbols={40}
            invisibleBorders
            disableNewLine
            onBlur={onBlur("title")}
            error={errors.title}
          />
        </>
      </div>
      <div className={styles.descriptionContainer}>
        <Textarea
          skeleton={loading}
          skeletonProps={{ height: 100 }}
          value={description}
          name="description"
          onChange={onChange}
          placeholder="What is this list about..."
          className={cn(styles.description)}
          maxSymbols={250}
          invisibleBorders
          onBlur={onBlur("description")}
        />
      </div>
    </>
  );
}
