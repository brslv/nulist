import { EmojiButton } from "@joeattardi/emoji-button";
import { useEffect, useRef } from "react";
import cn from "classnames";
import styles from "./EmojiPicker.module.css";
import { IEmoji } from "../../../core/list/types/IEmoji";
import Skeleton, { SkeletonProps } from "react-loading-skeleton";

interface IProps {
  classNames?: any;
  noStyle?: boolean;
  value?: IEmoji;
  onChange?: (emoji: IEmoji) => void;
  skeleton?: boolean;
  skeletonProps?: SkeletonProps;
}

export function EmojiPicker({
  classNames,
  onChange,
  value,
  noStyle = false,
  skeleton = false,
  skeletonProps = { width: 100, height: 100 },
}: IProps) {
  const triggerRef = useRef(null);
  const isInViewMode = typeof onChange !== "function";
  const picker = useRef<EmojiButton | null>(null);

  useEffect(() => {
    if (document) {
      picker.current = new EmojiButton({
        showPreview: false,
        showRecents: false,
        showCategoryButtons: false,
        position: "bottom",
      });
    }
  }, []);

  useEffect(() => {
    if (!picker || !picker.current || isInViewMode) return;
    picker.current.on("emoji", selection => {
      onChange(selection);
    });
  }, [isInViewMode, onChange, picker]);

  const onClick = () => {
    if (isInViewMode) return;

    if (triggerRef && triggerRef.current && picker && picker.current)
      picker.current.togglePicker(triggerRef.current);
  };

  return (
    <button
      ref={triggerRef}
      onClick={onClick}
      className={cn(
        noStyle ? null : styles.emojiPicker,
        { [styles.viewMode]: isInViewMode },
        classNames
      )}
    >
      {skeleton ? <Skeleton {...skeletonProps} /> : null}
      {value && !skeleton ? (
        <span role="img" title={value.name} className={styles.emoji}>
          {value.emoji}
        </span>
      ) : null}
    </button>
  );
}
