import React, { RefObject, useState } from "react";
import { useListEditor } from "../../../core/list/context/ListEditorProvider";
import { useOnKeyDown } from "../../../lib/useOnKeyDown";
import { ContextMenu } from "../../atomic/ContextMenu/ContextMenu";
import { ContextMenuContainer } from "../../atomic/ContextMenu/ContextMenuContainer";
import { ContextMenuItem } from "../../atomic/ContextMenu/ContextMenuItem";
import { ContextMenuTitle } from "../../atomic/ContextMenu/ContextMenuTitle";
import styles from "./ListItemTypeContextMenu.module.css";
import { IListItemTypeDescriptor, ListItemTypes } from "./types/IListItemType";

interface IProps {
  inputRef: RefObject<any>;
  onSelect: (selectedType: ListItemTypes) => void;
}

const listItemTypeDescriptors: IListItemTypeDescriptor[] = [
  {
    id: 1,
    type: ListItemTypes.Separator,
    label: "Separator",
    icon: "separator",
    disabled: false,
  },
  // {
  //   id: 2,
  //   type: ListItemTypes.Text,
  //   label: "Text",
  //   icon: "text",
  //   disabled: false,
  // },
  // {
  //   id: 3,
  //   type: ListItemTypes.SubscriptionForm,
  //   label: "Subscription form",
  //   icon: "atSign",
  //   disabled: true,
  // },
];

type ActiveType = IListItemTypeDescriptor | null;

const getIndexOfCurrentlyActive = (state: IListItemTypeDescriptor) => {
  return listItemTypeDescriptors.findIndex(
    itemType => itemType.id === state.id
  );
};
const getNextItemType = (indexOfCurrentlyActive: number) => {
  const nextType = listItemTypeDescriptors[indexOfCurrentlyActive + 1];
  if (!nextType) return listItemTypeDescriptors[0];
  return nextType;
};
const getPrevItemType = (indexOfCurrentlyActive: number) => {
  const prevType = listItemTypeDescriptors[indexOfCurrentlyActive - 1];
  if (!prevType)
    return listItemTypeDescriptors[listItemTypeDescriptors.length - 1];
  return prevType;
};

export function ListItemTypeContextMenu({ inputRef, onSelect }: IProps) {
  const { managingItemType, cancelManagingItemType } = useListEditor();
  const [activeTypeDescriptor, setActiveTypeDescriptor] = useState<ActiveType>(
    listItemTypeDescriptors[0]
  );

  const onTypeContextMenuOutsideClick = () => cancelManagingItemType();

  // Arrow down | Arrow up
  useOnKeyDown({
    fn: (e: KeyboardEvent) => {
      if (!managingItemType) return;

      if (e.key === "ArrowDown") {
        e.preventDefault();
        setActiveTypeDescriptor(prev => {
          if (!prev) return listItemTypeDescriptors[0];
          const indexOfCurrentlyActive = getIndexOfCurrentlyActive(prev);
          const nextItemType = getNextItemType(indexOfCurrentlyActive);
          return nextItemType;
        });
      }

      if (e.key === "ArrowUp") {
        e.preventDefault();
        setActiveTypeDescriptor(prev => {
          if (!prev)
            return listItemTypeDescriptors[listItemTypeDescriptors.length];
          const indexOfCurrentlyActive = getIndexOfCurrentlyActive(prev);
          const prevItemType = getPrevItemType(indexOfCurrentlyActive);
          return prevItemType;
        });
      }

      if (
        e.key !== "ArrowUp" &&
        e.key !== "ArrowDown" &&
        e.key !== "Enter" &&
        e.key !== "Backspace"
      ) {
        cancelManagingItemType();
      }

      if (e.key === "Backspace") {
        e.preventDefault(); // we need this to keep the / and not delete it, just close the context menu
        cancelManagingItemType();
      }
    },
  });

  const onClick = (listItemTypeDescriptor: IListItemTypeDescriptor) => {
    setActiveTypeDescriptor(listItemTypeDescriptor);
    onSelect(listItemTypeDescriptor.type);
    cancelManagingItemType();
  };

  return (
    <ContextMenu
      focusTrap={true}
      style={{ minWidth: 200 }}
      onOutsideClick={onTypeContextMenuOutsideClick}
      isOpen={!!managingItemType}
      anchorRef={inputRef}
      addTopOffset={10}
    >
      <ContextMenuContainer className={styles.container}>
        <ContextMenuTitle>Convert to</ContextMenuTitle>
        {listItemTypeDescriptors.map(liType => {
          return (
            <ContextMenuItem
              disabled={liType.disabled}
              soon={liType.disabled}
              active={activeTypeDescriptor?.id === liType.id}
              key={liType.type}
              icon={liType.icon}
              onClick={() => onClick(liType)}
              fullWidth
            >
              {liType.label}
            </ContextMenuItem>
          );
        })}
      </ContextMenuContainer>
    </ContextMenu>
  );
}
