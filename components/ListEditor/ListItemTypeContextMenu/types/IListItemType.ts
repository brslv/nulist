import { IconKey } from "../../../../lib/icons";

export enum ListItemTypes {
  Separator = "separator",
  SubscriptionForm = "subscription-form",
  Li = "li",
  Text = "text",
}

export interface IListItemTypeDescriptor {
  id: number;
  type: ListItemTypes;
  label: string;
  icon: IconKey;
  disabled: boolean;
}
