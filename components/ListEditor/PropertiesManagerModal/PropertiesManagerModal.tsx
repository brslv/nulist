import React from "react";
import { useListEditor } from "../../../core/list/context/ListEditorProvider";
import { Modal, ModalPosition } from "../../atomic/Modal/Modal";
import { ModalCloseIcon } from "../../atomic/Modal/ModalCloseIcon";
import { ModalContent } from "../../atomic/Modal/ModalContent";
import { PropertiesManager } from "../PropertiesManager/PropertiesManager";
import styles from "./PropertiesManagerModal.module.css";

export function PropertiesManagerModal() {
  const {
    managingProperties,
    getItemWithId,
    cancelManagingProperties,
    startEditing,
  } = useListEditor();
  const item = getItemWithId(managingProperties?.id);

  const onCancel = () => {
    if (managingProperties?.id) startEditing(managingProperties.id);
    cancelManagingProperties();
  };

  return (
    <Modal
      isOpen={!!managingProperties}
      onClose={onCancel}
      containerStyles={styles.modalContainer}
      helperTop={
        <div className={styles.itemPreviewContainer}>
          <div className={styles.itemPreview}>
            <div
              className={styles.itemTitle}
              dangerouslySetInnerHTML={{
                __html: item?.title || "",
              }}
            />
            <div>
              <ModalCloseIcon onClick={onCancel} />
            </div>
          </div>
        </div>
      }
      position={ModalPosition.TopCenter}
    >
      <ModalContent noSpace>
        <PropertiesManager onCancel={onCancel} />
      </ModalContent>
    </Modal>
  );
}
