import React, { ReactNode } from "react";
import {
  Draggable,
  DraggableProvided,
  DraggableStateSnapshot,
} from "react-beautiful-dnd";
import { IListItem } from "../../../core/list/types/IListItem";

interface IProps {
  item: IListItem;
  draggableIndex: number;
  children: (
    provided: DraggableProvided,
    snapshot: DraggableStateSnapshot
  ) => ReactNode;
}

export function DraggableListItem({ item, draggableIndex, children }: IProps) {
  return (
    <Draggable
      key={item.id}
      draggableId={item.id.toString()}
      index={draggableIndex}
    >
      {(provided, snapshot) => (
        <div
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          {children(provided, snapshot)}
        </div>
      )}
    </Draggable>
  );
}
