import styles from "./ListItem.module.css";
import { IndexedRow } from "../List/IndexedRow";
import { ControlsRow } from "../../ControlsRow/ControlsRow";
import { useListEditor } from "../../../core/list/context/ListEditorProvider";
import { IListItem } from "../../../core/list/types/IListItem";
import React from "react";
import { EditListItemForm } from "../ListItemForm/EditListItemForm";
import { HighlightedRow } from "../../HighlightedRow/HighlightedRow";
import { ListItemPropertyIcons } from "../../ListItemPropertyIcons/ListItemPropertyIcons";

interface IProps {
  item: IListItem;
  onClick?: () => void;
  index?: number;
  onAddBellow: (id: IListItem["id"]) => void;
  isDragging?: boolean;
}

export function ListItem({
  item,
  onClick,
  index,
  onAddBellow,
  isDragging = false,
}: IProps) {
  const {
    focusedItem,
    updateItem,
    startEditing,
    removeItem,
    startManagingProperties,
  } = useListEditor();
  const isBeingFocused = !!focusedItem && focusedItem === item.id;

  const internalOnClick = () => {
    startEditing(item.id);
    if (typeof onClick === "function") onClick();
  };

  const onSubmit = (title: IListItem["title"]) => {
    const data = { title };
    updateItem(item.id, data);
  };

  const onRowClick = () => startEditing(item.id);
  const onMenu = () => startManagingProperties(item.id);

  return (
    <>
      <ControlsRow
        align="flex-start"
        active={isBeingFocused}
        onAddBellow={() => onAddBellow(item.id)}
        onDelete={() => removeItem(item.id)}
        onMenu={onMenu}
        controlsClassName={styles.controls}
      >
        {() => {
          return (
            <>
              <HighlightedRow
                onClick={onRowClick}
                isActive={!!isBeingFocused || isDragging}
              >
                <IndexedRow index={index}>
                  <div className={styles.container}>
                    <div className={styles.formContainer}>
                      <EditListItemForm
                        item={item}
                        onFocus={internalOnClick}
                        onSubmit={onSubmit}
                        onDelete={() => removeItem(item.id)}
                      />
                    </div>
                    <ListItemPropertyIcons item={item} />
                  </div>
                </IndexedRow>
              </HighlightedRow>
            </>
          );
        }}
      </ControlsRow>
    </>
  );
}
