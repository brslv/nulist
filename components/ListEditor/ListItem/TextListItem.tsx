import { useState } from "react";
import { useListEditor } from "../../../core/list/context/ListEditorProvider";
import { IListItem } from "../../../core/list/types/IListItem";
import { Textarea } from "../../atomic/Textarea/Textarea";
import { ControlsRow } from "../../ControlsRow/ControlsRow";
import { HighlightedRow } from "../../HighlightedRow/HighlightedRow";
import { IndexedRow } from "../List/IndexedRow";
import styles from "./TextListItem.module.css";

interface IProps {
  item: IListItem;
  onAddBellowClick: (id: IListItem["id"]) => void;
  isDragging?: boolean;
}

export function TextListItem({ item, onAddBellowClick, isDragging }: IProps) {
  const { editingItem, focusedItem, removeItem, startEditing } =
    useListEditor();
  const isBeingFocused = focusedItem === item.id;
  const isBeingEdited = editingItem === item.id;
  const isDimmed = !!(editingItem !== null && !isBeingEdited);
  const [value, setValue] = useState(item.content || "");

  const onBlur = () => {
    console.log("blur -> submit");
  };

  const onDelete = () => removeItem(item.id);

  const onClick = () => startEditing(item.id);

  return (
    <ControlsRow
      onAddBellow={() => onAddBellowClick(item.id)}
      onDelete={onDelete}
      active={isBeingFocused}
      align="flex-start"
    >
      {() => {
        return (
          <HighlightedRow
            isActive={isBeingFocused || isDragging}
            onClick={onClick}
          >
            <IndexedRow fullWidth dimmed={isDimmed}>
              <Textarea
                value={value}
                fullWidth
                onChange={e => setValue(e.currentTarget.value)}
                className={styles.input}
                onBlur={onBlur}
              />
            </IndexedRow>
          </HighlightedRow>
        );
      }}
    </ControlsRow>
  );
}
