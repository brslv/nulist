import { useListEditor } from "../../../core/list/context/ListEditorProvider";
import { IListItem } from "../../../core/list/types/IListItem";
import { useOnKeyDown } from "../../../lib/useOnKeyDown";
import { ControlsRow } from "../../ControlsRow/ControlsRow";
import { HighlightedRow } from "../../HighlightedRow/HighlightedRow";
import { IndexedRow } from "../List/IndexedRow";
import styles from "./SeparatorListItem.module.css";

interface IProps {
  item: IListItem;
  onAddBellowClick: (id: IListItem["id"]) => void;
  isDragging?: boolean;
}

export function SeparatorListItem({
  item,
  onAddBellowClick,
  isDragging,
}: IProps) {
  const {
    editingItem,
    focusedItem,
    removeItem,
    startEditingPrev,
    startEditing,
  } = useListEditor();
  const isBeingFocused = focusedItem === item.id;
  const isBeingEdited = editingItem === item.id;
  const isDimmed = !!(editingItem !== null && !isBeingEdited);

  const onDelete = () => removeItem(item.id);

  const onClick = () => startEditing(item.id);

  // Backspace
  useOnKeyDown<KeyboardEvent>({
    fn: e => {
      const isBackspace = e.key === "Backspace";

      if (isBeingFocused && isBackspace) {
        e.preventDefault();
        onDelete();
        startEditingPrev();
      }
    },
  });

  return (
    <ControlsRow
      onAddBellow={() => onAddBellowClick(item.id)}
      onDelete={onDelete}
      active={isBeingFocused}
      align="flex-start"
      controlsClassName={styles.controls}
    >
      {() => {
        return (
          <HighlightedRow
            isActive={isBeingFocused || isDragging}
            onClick={onClick}
          >
            <IndexedRow dimmed={isDimmed}>
              <hr className={styles.separator} />
            </IndexedRow>
          </HighlightedRow>
        );
      }}
    </ControlsRow>
  );
}
