import React, {
  useEffect,
  useRef,
  KeyboardEvent,
  useState,
  useCallback,
} from "react";
import { useListEditor } from "../../../core/list/context/ListEditorProvider";
import { IListItem } from "../../../core/list/types/IListItem";
import { useOnKeyDown } from "../../../lib/useOnKeyDown";
import { usePrevious } from "../../../lib/usePrevious";
import { Textarea } from "../../atomic/Textarea/Textarea";
import styles from "./EditListItemForm.module.css";
import { ICommonProps } from "./IListItemFormProps";

interface IProps {
  item: IListItem;
  onSubmit: ICommonProps["onSubmit"];
  onDelete: ICommonProps["onDelete"];
  onFocus?: () => void;
}

export function EditListItemForm({
  item,
  onSubmit,
  onDelete,
  onFocus,
}: IProps) {
  const ref = useRef(null);
  const {
    managingProperties,
    editingItem,
    cancelEditing,
    startEditingPrev,
    startEditingNext,
    startManagingProperties,
  } = useListEditor();

  const [title, setTitle] = useState(item.title || "");

  const internalOnChange = (value: string) => setTitle(value);

  const internalOnSubmit = useCallback(() => {
    if (!title && typeof onDelete === "function") {
      onDelete();
    } else {
      const sanitizedTitle = title.trim().replaceAll("\n", "");
      setTitle(sanitizedTitle);
      onSubmit(sanitizedTitle);
    }
  }, [onSubmit, onDelete, title]);

  const internalOnBlur = useCallback(() => {
    internalOnSubmit();
    cancelEditing();
  }, [cancelEditing, internalOnSubmit]);

  // Blur the input when the item is no longer being edited.
  const previousEditingItem: IListItem["id"] | undefined | null =
    usePrevious(editingItem);
  useEffect(() => {
    const hadBeenEdited =
      previousEditingItem && previousEditingItem === item.id;
    const isBeingEdited = editingItem && editingItem === item.id;

    if (!isBeingEdited && hadBeenEdited) {
      if (ref && ref.current) (ref as any).current.blur();
    }
  }, [editingItem, item.id, previousEditingItem]);

  // Focus the input if it's being edited
  useEffect(() => {
    if (editingItem && item && editingItem === item.id && !managingProperties) {
      if (ref && ref.current) (ref as any).current.focus();
    }
  }, [editingItem, item, managingProperties]);

  // Enter | Meta + Enter | Backspace
  useOnKeyDown<KeyboardEvent>({
    ref,
    fn: e => {
      const isEnter = e.key === "Enter";
      const isBackspace = e.key === "Backspace";
      const isAboutToDeleteItem =
        isBackspace && (title.length === 0 || title === "<br>"); // FF adds <br> tag on empty contenteditable

      if (isAboutToDeleteItem) {
        e.preventDefault();
        e.stopPropagation();
        if (typeof onDelete === "function") onDelete();
        startEditingPrev();
      }

      const isCtrlOrCmdEnter = (e.metaKey || e.ctrlKey) && e.key === "Enter";

      if (!isCtrlOrCmdEnter && isEnter) {
        internalOnSubmit();
        startEditingNext(item.id);
        e.preventDefault();
      }

      if (isCtrlOrCmdEnter && item) {
        internalOnSubmit();
        startManagingProperties(item.id);
        e.preventDefault();
      }
    },
  });

  return (
    <div style={{ position: "relative", width: "100%" }}>
      <Textarea
        name="editItem"
        fullWidth
        minRows={1}
        ref={ref}
        value={title}
        onBlur={internalOnBlur}
        onKeyPress={e => {
          if (e.key === "Enter") {
            e.preventDefault();
            internalOnSubmit();
          }
        }}
        onChange={e => internalOnChange(e.currentTarget.value)}
        onFocus={onFocus}
        className={styles.input}
        rootStyles={{ lineHeight: 0 }}
      />
    </div>
  );
}
