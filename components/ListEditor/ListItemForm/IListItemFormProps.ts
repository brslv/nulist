import { IListItem } from "../../../core/list/types/IListItem";

export interface ICommonProps {
  onSubmit: (title: IListItem["title"]) => void;
  item?: IListItem;
  onDelete?: () => void;
  onChange?: (value: IListItem["title"]) => void;
  onFocus?: () => void;
}
