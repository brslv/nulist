import React, {
  forwardRef,
  useRef,
  useState,
  useEffect,
  useCallback,
} from "react";
import { useListEditor } from "../../../core/list/context/ListEditorProvider";
import { IListItem } from "../../../core/list/types/IListItem";
import { useOnKeyDown } from "../../../lib/useOnKeyDown";
import { useOnKeyUp } from "../../../lib/useOnKeyUp";
import { usePrevious } from "../../../lib/usePrevious";
import { Textarea } from "../../atomic/Textarea/Textarea";
import { ListItemTypeContextMenu } from "../ListItemTypeContextMenu/ListItemTypeContextMenu";
import { ListItemTypes } from "../ListItemTypeContextMenu/types/IListItemType";
import styles from "./CreateNewListItemForm.module.css";
import { ICommonProps } from "./IListItemFormProps";

export interface IProps {
  onSubmit: ICommonProps["onSubmit"];
  onConvertToType: (listItemType: ListItemTypes) => void;
  onDelete?: ICommonProps["onDelete"];
  onEmptyEscOrBackspace?: (e: KeyboardEvent) => void;
}

export const CreateNewListItemForm = forwardRef(
  (
    { onSubmit, onDelete, onConvertToType, onEmptyEscOrBackspace }: IProps,
    ref: any
  ) => {
    const innerRef = useRef(null);
    const inputRef = ref || innerRef;
    const {
      startEditingLast,
      addingItem,
      startAddingItem,
      cancelEditing,
      startManagingItemType,
      cancelAddingItem,
      managingItemType,
    } = useListEditor();
    const previousAddingItem: IListItem | undefined | null =
      usePrevious(addingItem);

    const [title, setTitle] = useState("");

    const internalOnSubmit = useCallback(() => {
      if (managingItemType) return;

      if (title === "" || title === "/") {
        if (typeof onDelete === "function") onDelete();
      } else {
        // const sanitizedTitle = title.replaceAll(/\n|&nbsp;|<br>/gi, "");
        const sanitizedTitle = title.trim().replaceAll("\n", "");
        setTitle(sanitizedTitle);
        onSubmit(sanitizedTitle);
        setTitle("");
      }
    }, [managingItemType, onDelete, onSubmit, title]);

    const internalOnChange = (value: string) => setTitle(value);

    const internalOnConvertToType = (selectedType: ListItemTypes) => {
      setTitle("");
      onConvertToType(selectedType);
    };

    // Meta + / -> focus on the input
    useOnKeyDown({
      fn: (e: KeyboardEvent) => {
        if ((e.ctrlKey || e.metaKey) && e.key === "/") {
          cancelEditing();
          startAddingItem();

          if (addingItem && inputRef && inputRef.current) {
            (inputRef as any).current.focus();
          }
        }
      },
    });

    useEffect(() => {
      if (addingItem) {
        if (inputRef && inputRef.current) {
          (inputRef as any).current.focus();
        }
      }
    }, [addingItem, inputRef]);

    // Esc || Backspace
    useOnKeyDown({
      ref: inputRef,
      fn: (e: KeyboardEvent) => {
        if (e.key === "Escape" || (e.key === "Backspace" && title === "")) {
          if (typeof onEmptyEscOrBackspace === "function") {
            onEmptyEscOrBackspace(e);
          } else {
            e.preventDefault();
            e.stopPropagation();
            startEditingLast();
          }
        }
      },
    });

    useOnKeyUp({
      fn: (e: KeyboardEvent) => {
        if (e.key === "/" && title === "/") {
          startManagingItemType();
        }
      },
    });

    useEffect(() => {
      if (title && title.endsWith("\n")) {
        internalOnSubmit();
      }
    }, [internalOnSubmit, title]);

    // Blur the input when no longer adding item.
    useEffect(() => {
      if (!addingItem && previousAddingItem) {
        if (inputRef && inputRef.current) (inputRef as any).current.blur();
      }
    }, [addingItem, previousAddingItem, inputRef]);

    return (
      <div style={{ position: "relative", width: "100%" }}>
        <Textarea
          name="newItem"
          fullWidth
          minRows={1}
          ref={inputRef}
          value={title}
          onBlur={() => {
            internalOnSubmit();
            if (!managingItemType) cancelAddingItem();
          }}
          onKeyPress={e => {
            if (e.key === "Enter") {
              e.preventDefault();
              internalOnSubmit();
            }
          }}
          onChange={e => internalOnChange(e.currentTarget.value)}
          onFocus={startAddingItem}
          className={styles.input}
          rootStyles={{ lineHeight: 0 }}
          placeholder="Title or / to convert"
        />

        <ListItemTypeContextMenu
          inputRef={inputRef}
          onSelect={selectedType => internalOnConvertToType(selectedType)}
        />
      </div>
    );
  }
);
