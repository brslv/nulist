import React, { useMemo, Fragment, useState, useEffect } from "react";
import styles from "./List.module.css";
import { ListItem } from "../ListItem/ListItem";
import { IndexedRow } from "./IndexedRow";
import { useListEditor } from "../../../core/list/context/ListEditorProvider";
import { IListItem } from "../../../core/list/types/IListItem";
import { AddBellow } from "./AddBellow";
import { CreateNewListItemForm } from "../ListItemForm/CreateNewListItemForm";
import { HighlightedRow } from "../../HighlightedRow/HighlightedRow";
import { useOnKeyDown } from "../../../lib/useOnKeyDown";
import { ListItemTypes } from "../ListItemTypeContextMenu/types/IListItemType";
import { SeparatorListItem } from "../ListItem/SeparatorListItem";
import { PropertiesManagerModal } from "../PropertiesManagerModal/PropertiesManagerModal";
import { DragDropContext, Droppable, DropResult } from "react-beautiful-dnd";
import { DraggableListItem } from "../ListItem/DraggableListItem";
import { SaveModal } from "../../SaveModal/SaveModal";
import { AskForFeedbackModal } from "../../SaveModal/AskForFeedbackModal";
import { WaitlistModal } from "../../WaitlistModal/WaitlistModal";
import {
  OnboardingModals,
  useOnboardingModals,
} from "../../../core/onboarding/modals/OnboardingModalsProvider";
import {
  getIndexesMap,
  getMaxIndex,
} from "../../../core/list/utils/getIndexesMap";
import Skeleton from "react-loading-skeleton";
import { TextListItem } from "../ListItem/TextListItem";
import { Button } from "../../atomic/Button/Button";
import { useAuth } from "../../../core/auth/context/AuthProvider";
import { Help } from "../../Help/Help";

interface IProps {
  onCancel: () => void;
}

export function List({ onCancel }: IProps) {
  const {
    loading,
    items,
    focusedItem,
    saveBtnLabel,
    managingProperties,
    addingItem,
    addItem,
    startEditing,
    startEditingNext,
    startEditingPrev,
    startEditingLast,
    startEditingFirst,
    cancelEditing,
    cancelFocus,
    startAddingItem,
    onDragItem,
    onSave,
  } = useListEditor();
  const { user } = useAuth();
  const { isOpen, open, close } = useOnboardingModals();
  const [isSaveModalOpen, setIsSaveModalOpen] = useState(false);
  const [isAskForFeedbackModalOpen, setIsAskForFeedbackModalOpen] =
    useState(false);
  const [addBellowItem, setAddBellowItem] = useState<IListItem["id"] | null>(
    null
  );

  const indexesMap = useMemo(() => getIndexesMap(items), [items]);

  useEffect(() => {
    const handleBeforeUnload = (e: any) => {
      e.preventDefault();
      e.returnValue = "";
    };

    window.addEventListener("beforeunload", handleBeforeUnload);

    return () => {
      window.removeEventListener("beforeunload", handleBeforeUnload);
    };
  }, []);

  // Meta + Arrow down
  useOnKeyDown({
    fn: (e: KeyboardEvent) => {
      if (focusedItem && e.key === "ArrowDown" && (e.metaKey || e.ctrlKey)) {
        setAddBellowItem(focusedItem);
        cancelEditing();
        cancelFocus();
      }
    },
  });

  // Arrow down|down on focused item / adding item / add bellow item
  useOnKeyDown({
    fn: (e: KeyboardEvent) => {
      if (managingProperties) return;
      if (
        (e.key === "ArrowDown" || e.key === "ArrowUp") &&
        !addingItem &&
        !focusedItem &&
        !addBellowItem
      ) {
        startEditingFirst();
      }
      if (e.key === "ArrowDown" && addingItem) startEditingFirst();
      if (e.key === "ArrowDown" && focusedItem) startEditingNext();
      if (e.key === "ArrowDown" && addBellowItem)
        startEditingNext(addBellowItem);
      if (e.key === "ArrowUp" && addingItem) {
        e.preventDefault();
        startEditingLast();
      }
      if (e.key === "ArrowUp" && focusedItem) {
        // Firefox goes to the beginning of the line on ArrowUp
        // We need to prevent this, in order to keep the focus on the prev item
        // or the CreateNewListItemForm input
        e.preventDefault();
        startEditingPrev();
      }
      if (e.key === "ArrowUp" && addBellowItem) startEditing(addBellowItem);
    },
  });

  const onAddBellowClick = (item: IListItem["id"]) => {
    setAddBellowItem(item);
  };

  const onAddBellowSubmit = (title: IListItem["title"]) => {
    if (title === "") {
      onAddBellowDelete();
    }

    if (addBellowItem) {
      const item = addItem({
        type: ListItemTypes.Li,
        data: title,
        options: { bellowItemWithId: addBellowItem },
      });
      setAddBellowItem(null);
      if (item) startEditing(item.id);
    }
  };

  const onAddBellowDelete = () => {
    if (addBellowItem) {
      startEditing(addBellowItem);
      setAddBellowItem(null);
    }
  };

  const onCreateNewSubmit = (title: IListItem["title"]) => {
    addItem({
      type: ListItemTypes.Li,
      data: title,
    });
  };

  const onConvertToType = (type: ListItemTypes) => {
    if (type === ListItemTypes.Separator) {
      const item = addItem({
        type: ListItemTypes.Separator,
        data: null,
        options: {
          bellowItemWithId: addBellowItem || undefined,
        },
      });

      if (addBellowItem) {
        if (item) startEditing(item.id);
        setAddBellowItem(null);
      }
    }

    if (type === ListItemTypes.Text) {
      const item = addItem({
        type: ListItemTypes.Text,
        data: "Hey, this is a text node.",
        options: {
          bellowItemWithId: addBellowItem || undefined,
        },
      });

      if (addBellowItem) {
        if (item) startEditing(item.id);
        setAddBellowItem(null);
      }
    }
  };

  const onDragEnd = (dropResult: DropResult) => {
    if (!dropResult.destination) {
      return;
    }

    onDragItem(dropResult.source.index, dropResult.destination.index);
  };

  if (loading) {
    return <Skeleton count={1} height={20} />;
  }

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId="list">
        {droppableProvided => {
          return (
            <div
              {...droppableProvided.droppableProps}
              ref={droppableProvided.innerRef}
              className={styles.list}
            >
              <ul className={styles.ul}>
                {items.map((item, i) => {
                  const isAddingBellow =
                    addBellowItem && addBellowItem === item.id;

                  return (
                    <Fragment key={item.id}>
                      {item.type === ListItemTypes.Separator ? (
                        <DraggableListItem item={item} draggableIndex={i}>
                          {(_, draggableSnapshot) => (
                            <SeparatorListItem
                              item={item}
                              onAddBellowClick={onAddBellowClick}
                              isDragging={draggableSnapshot.isDragging}
                            />
                          )}
                        </DraggableListItem>
                      ) : null}

                      {item.type === ListItemTypes.Li ? (
                        <DraggableListItem item={item} draggableIndex={i}>
                          {(_, draggableSnapshot) => (
                            <ListItem
                              item={item}
                              index={indexesMap[item.id]}
                              onAddBellow={onAddBellowClick}
                              isDragging={draggableSnapshot.isDragging}
                            />
                          )}
                        </DraggableListItem>
                      ) : null}

                      {item.type === ListItemTypes.Text ? (
                        <DraggableListItem item={item} draggableIndex={i}>
                          {(_, draggableSnapshot) => (
                            <TextListItem
                              item={item}
                              onAddBellowClick={onAddBellowClick}
                              isDragging={draggableSnapshot.isDragging}
                            />
                          )}
                        </DraggableListItem>
                      ) : null}

                      {isAddingBellow ? (
                        <AddBellow
                          onSubmit={onAddBellowSubmit}
                          onDelete={onAddBellowDelete}
                          onConvertToType={onConvertToType}
                        />
                      ) : null}
                    </Fragment>
                  );
                })}

                {droppableProvided.placeholder}

                {!addBellowItem ? (
                  <HighlightedRow
                    onClick={() => startAddingItem()}
                    isActive={addingItem}
                  >
                    <IndexedRow index={getMaxIndex(indexesMap) + 1}>
                      <CreateNewListItemForm
                        onSubmit={onCreateNewSubmit}
                        onConvertToType={onConvertToType}
                      />
                    </IndexedRow>
                  </HighlightedRow>
                ) : null}

                <PropertiesManagerModal />
                <SaveModal
                  isOpen={isSaveModalOpen}
                  onClose={() => setIsSaveModalOpen(false)}
                  onCancel={() => {
                    setIsSaveModalOpen(false);
                    setIsAskForFeedbackModalOpen(true);
                  }}
                  onJoinWaitlist={() => {
                    setIsSaveModalOpen(false);
                    open(OnboardingModals.Waitlist);
                  }}
                />
                <AskForFeedbackModal
                  isOpen={isAskForFeedbackModalOpen}
                  onClose={() => setIsAskForFeedbackModalOpen(false)}
                />
                <WaitlistModal
                  isOpen={isOpen(OnboardingModals.Waitlist)}
                  onClose={close}
                />
              </ul>

              <div className={styles.footer}>
                <div className={styles.buttons}>
                  <Button variant="accent" onClick={onSave}>
                    {saveBtnLabel}
                  </Button>
                  {user ? (
                    <Button variant="plain" onClick={onCancel}>
                      Cancel
                    </Button>
                  ) : null}
                </div>
                <div>
                  <Help />
                </div>
              </div>
            </div>
          );
        }}
      </Droppable>
    </DragDropContext>
  );
}
