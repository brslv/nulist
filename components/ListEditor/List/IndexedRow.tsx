import styles from "./IndexedRow.module.css";
import cn from "classnames";
import { ReactNode } from "react";

interface IProps {
  index?: ReactNode;
  children: ReactNode;
  dimmed?: boolean;
  fullWidth?: boolean;
}

export function IndexedRow({
  index,
  children,
  dimmed = false,
  fullWidth = false,
}: IProps) {
  return (
    <li
      className={cn(styles.indexedRow, {
        [styles.dimmed]: dimmed,
      })}
    >
      {fullWidth ? null : (
        <span
          className={cn(styles.index, {
            [styles.noAfter]: index === undefined,
          })}
        >
          {index || ""}
        </span>
      )}
      {children}
    </li>
  );
}
