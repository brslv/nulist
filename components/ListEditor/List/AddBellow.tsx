import React, { useRef, useEffect } from "react";
import styles from "./AddBellow.module.css";
import { IndexedRow } from "./IndexedRow";
import {
  CreateNewListItemForm,
  IProps as ICreateNewListItemFormProps,
} from "../ListItemForm/CreateNewListItemForm";
import { HighlightedRow } from "../../HighlightedRow/HighlightedRow";
import { ICommonProps } from "../ListItemForm/IListItemFormProps";

interface IProps {
  onSubmit: ICommonProps["onSubmit"];
  onDelete: ICommonProps["onDelete"];
  onConvertToType: ICreateNewListItemFormProps["onConvertToType"];
}

export function AddBellow({ onSubmit, onDelete, onConvertToType }: IProps) {
  const ref = useRef(null);

  useEffect(() => {
    if (ref && ref.current) {
      (ref as any).current.focus();
    }
  }, [ref]);

  const onEmptyEscOrBackspace = (e: KeyboardEvent) => {
    e.preventDefault();
    e.stopPropagation();
    if (typeof onDelete === "function") onDelete();
  };

  return (
    <div className={styles.addBellowRowContainer}>
      <HighlightedRow isActive={true}>
        <IndexedRow index="*">
          <CreateNewListItemForm
            ref={ref}
            onDelete={onDelete}
            onSubmit={onSubmit}
            onConvertToType={onConvertToType}
            onEmptyEscOrBackspace={onEmptyEscOrBackspace}
          />
        </IndexedRow>
      </HighlightedRow>
    </div>
  );
}
