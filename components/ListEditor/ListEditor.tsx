import React, { useEffect, useState } from "react";
import { useListEditor } from "../../core/list/context/ListEditorProvider";
import { List } from "./List/List";
import { ListHeading } from "./ListHeading/ListHeading";
import { ListBundlesProvider } from "../../core/bundles/context/ListBundlesProvider";
import { BundlesManager } from "./BundlesManager/BundlesManager";
import styles from "./ListEditor.module.css";
import { usePageControls } from "../PageControls/PageControlsProvider";
import { Confirmation } from "../atomic/Confirmation/Confirmation";
import { useRouter } from "next/router";

export function ListEditor() {
  const router = useRouter();
  const { strip, unstrip } = usePageControls();
  const { resetState } = useListEditor();
  const [isCancelConfirmationOpen, setIsCancelConfirmationOpen] =
    useState(false);

  const onCancel = () => {
    setIsCancelConfirmationOpen(true);
  };
  const cancel = () => router.push("/dashboard");

  useEffect(() => {
    strip();
    return () => {
      resetState();
      unstrip();
    };
  }, [resetState, strip, unstrip]);

  return (
    <ListBundlesProvider>
      <div className={styles.listEditor}>
        <ListHeading />
        <List onCancel={onCancel} />
        <BundlesManager />
        <Confirmation
          isOpen={isCancelConfirmationOpen}
          onCancel={() => setIsCancelConfirmationOpen(false)}
          onOk={() => {
            cancel();
          }}
        >
          All unsaved changes will be lost. Continue?
        </Confirmation>
      </div>
    </ListBundlesProvider>
  );
}
