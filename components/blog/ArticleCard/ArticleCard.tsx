import Image from "next/image";
import Link from "next/link";
import React from "react";
import { IBlogArticle } from "../../../core/blog/types/IBlogArticle";
import { Title } from "../../atomic/Title/Title";
import styles from "./ArticleCard.module.css";
import cn from "classnames";
import { Button } from "../../atomic/Button/Button";
import { formatArticleDate } from "../../../core/blog/utils/formatArticleDate";

interface IProps {
  article: IBlogArticle;
}

export function ArticleCard({ article }: IProps) {
  const coverPosition =
    styles[`cover_${article.metadata.image.coverPosition || "center"}`];

  return (
    <Link href={article.slug} passHref>
      <a className={styles.articleCard}>
        <div className={styles.mainImgContainer}>
          <Image
            placeholder="blur"
            blurDataURL={article.metadata.image.src}
            src={article.metadata.image.src}
            alt={article.metadata.image.alt}
            layout="fill"
            className={cn(styles.mainImg, { [coverPosition]: true })}
          />
        </div>
        <div className={styles.content}>
          <div className={styles.main}>
            <Title size={50} weight="medium">
              {article.metadata.title}
            </Title>
            <div className={styles.dateContainer}>
              <time className={styles.date} dateTime={article.metadata.date}>
                &mdash; {formatArticleDate(article.metadata.date)}
              </time>
            </div>
            <Title
              component="h2"
              size={30}
              weight="regular"
              className={styles.description}
            >
              {article.metadata.description}
            </Title>
          </div>
          <div className={styles.footer}>
            <Button variant="accent">Read</Button>
          </div>
        </div>
      </a>
    </Link>
  );
}
