import Image from "next/image";
import React, { ReactNode } from "react";
import Head from "next/head";
import { IBlogArticleMetadata } from "../../../core/blog/types/IBlogArticleMetadata";
import { Page, PageMain } from "../../atomic/Page/Page";
import { Title } from "../../atomic/Title/Title";
import styles from "./BlogArticleLayout.module.css";
import cn from "classnames";
import { formatArticleDate } from "../../../core/blog/utils/formatArticleDate";

interface IProps {
  metadata: IBlogArticleMetadata;
  children: ReactNode;
}

export function BlogArticleLayout({ metadata, children }: IProps) {
  const coverPosition =
    styles[
      `cover_${
        metadata.image.coverPositionInArticle ||
        metadata.image.coverPosition ||
        "center"
      }`
    ];
  const head = {
    title: `${metadata.title} • Nulist`,
    description: metadata.description,
    image: { ...metadata.image },
  };

  return (
    <Page>
      <Head>
        <title key="title">{head.title}</title>
        <meta property="og:title" content={head.title} key="og:title" />
        <meta
          property="description"
          content={head.description}
          key="description"
        />
        <meta
          property="og:description"
          content={head.description}
          key="og:description"
        />
        <meta
          property="og:image"
          content={`https://nuli.st${head.image.src}`}
          key="og:image"
        />
        <meta
          name="twitter:card"
          content="summary_large_image"
          key="og:twitter:card"
        />
        <meta
          name="twitter:image:alt"
          content={head.image.alt}
          key="og:twitter:image:alt"
        />
      </Head>
      <PageMain>
        <Title size={60} weight="bold">
          {metadata.title}
        </Title>
        <time className={styles.date} id="publishDate">
          &mdash; {formatArticleDate(metadata.date)}
        </time>
        <Title
          component="h2"
          size={40}
          weight="light"
          className={styles.description}
        >
          {metadata.description}
        </Title>
        <div className={styles.mainImgWrapper}>
          <div className={styles.mainImgContainer}>
            <Image
              placeholder="blur"
              blurDataURL={metadata.image.src}
              src={metadata.image.src}
              alt={metadata.image.alt}
              layout="fill"
              className={cn(styles.mainImg, { [coverPosition]: true })}
            />
          </div>
        </div>
        <div className={styles.content}>{children}</div>
      </PageMain>
    </Page>
  );
}
