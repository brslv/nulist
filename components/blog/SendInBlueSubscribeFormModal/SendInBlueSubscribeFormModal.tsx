import { Modal } from "../../atomic/Modal/Modal";
import { ModalHeading } from "../../atomic/Modal/ModalHeading";
interface IProps {
  isOpen: boolean;
  onClose: () => void;
}

export function SendInBlueSubscribeFormModal({ isOpen, onClose }: IProps) {
  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalHeading onClose={onClose} transparent border={false} gradient />
      <div>
        <iframe
          width="540"
          height="440"
          src="https://c93fb850.sibforms.com/serve/MUIEABYkpNGT_98m2Ylj5_ntzdBt-IuL-Nl2rvOlzONZSskQAyQ-EMkoLEEmnPxXW-Ssn1XzsoW1b9aUVUbu6BsGtLkA1-sSjzsY5t94-g5OzX5lF6wJcZqmN6tRp48G5iL06rWf_Ta0U_EkOa6LHDT_tb2JsiNeRJ60xbH8-F8Nw7uyPSTO_cp_ugfSmvul8_tQbDKNjBuKjK-v"
          frameBorder="0"
          scrolling="no"
          allowFullScreen
          style={{
            display: "block",
            marginLeft: "auto",
            marginRight: "auto",
            maxWidth: "100%",
          }}
        ></iframe>
      </div>
    </Modal>
  );
}
