import { ReactNode } from "react";
import { Panel } from "../../atomic/Panel/Panel";

interface IProps {
  children: ReactNode;
}

export function CallToActionPanel({ children }: IProps) {
  return <Panel>{children}</Panel>;
}
