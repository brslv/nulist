import React from "react";
import Link from "next/link";
import { Button } from "../../atomic/Button/Button";

export function BackToBlogBtn() {
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Link href="/blog" passHref>
        <Button component="a" variant="basic" icon="goBack">
          Back to blog
        </Button>
      </Link>
    </div>
  );
}
