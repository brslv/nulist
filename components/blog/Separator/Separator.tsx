import styles from "./Separator.module.css";

export function Separator() {
  return (
    <div className={styles.separator}>
      <div className={styles.dot} />
      <div className={styles.dot} />
      <div className={styles.dot} />
    </div>
  );
}
