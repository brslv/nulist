import React, { useState } from "react";
import { BlurryBackground } from "../../atomic/BlurryBackground/BlurryBackground";
import { Button } from "../../atomic/Button/Button";
import { Title } from "../../atomic/Title/Title";
import { SendInBlueSubscribeFormModal } from "../SendInBlueSubscribeFormModal/SendInBlueSubscribeFormModal";
import styles from "./BlogSubscribeForm.module.css";

export function BlogSubscribeForm() {
  const [isSendInBlueModalOpen, setIsSendInBlueModalOpen] = useState(false);

  return (
    <div className={styles.subscribeForm}>
      <BlurryBackground
        style={{ top: -500, left: -200 }}
        position="absolute"
        opacity={10}
      />
      <div className={styles.content}>
        <div className={styles.titleContainer}>
          <Title component="h4" size={50}>
            Be the first to get our fresh and juicy content!
          </Title>
        </div>

        <p>
          Do you want to learn nifty tricks on how to leverage the power of
          lists and become the best version of your productive self?
        </p>

        <div className={styles.btnsContainer}>
          <Button
            variant="accent"
            onClick={() => setIsSendInBlueModalOpen(true)}
          >
            Subscribe to our newsletter
          </Button>
        </div>

        <SendInBlueSubscribeFormModal
          isOpen={isSendInBlueModalOpen}
          onClose={() => setIsSendInBlueModalOpen(false)}
        />
      </div>
    </div>
  );
}
