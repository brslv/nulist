import NextImage from "next/image";
import styles from "./Image.module.css";

interface IProps {
  src: string;
  alt: string;
}

export function Image({ src, alt }: IProps) {
  return (
    <div style={{ position: "relative", width: "100%", height: 450 }}>
      <NextImage
        placeholder="blur"
        blurDataURL={src}
        src={src}
        layout="fill"
        alt={alt}
        className={styles.img}
      />
    </div>
  );
}
