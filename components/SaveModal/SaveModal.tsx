import { Modal } from "../atomic/Modal/Modal";
import { ModalContent } from "../atomic/Modal/ModalContent";
import styles from "./SaveModal.module.css";
import React from "react";
import Image from "next/image";
import me from "../../public/images/me.jpg";
import { Title } from "../atomic/Title/Title";
import { Button } from "../atomic/Button/Button";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onCancel: () => void;
  onJoinWaitlist: () => void;
}

export function SaveModal({
  isOpen,
  onClose,
  onCancel,
  onJoinWaitlist,
}: IProps) {
  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalContent noSpace>
        <div className={styles.saveModal}>
          <div className={styles.meContainer}>
            <Image
              src={me}
              layout="fill"
              alt="Photo of Borislav Grigorov"
              className={styles.me}
            />
          </div>
          <div className={styles.content}>
            <Title size={40} weight="semiBold" className={styles.title}>
              I need to tell you something. 😬
            </Title>
            <p>
              I know it might sound lame, but saving lists doesn&apos;t work,
              yet. I still work on this.
            </p>
            <strong>
              Now, why did I ship something that&apos;s basically useless?
            </strong>
            <ul>
              <li>Firstly, to collect feedback from real people.</li>
              <li>Secondly, to get into the habit of shipping things.</li>
              <li>And lastly, to have something to show to people.</li>
            </ul>
            <p>
              If you think Nulist looks promising, come and join the waitlist!
              I&apos;ll get in touch with you once the whole thing is ready and
              useable.
            </p>
            <small>
              I hate spammy emails, so you won&apos;t see any of these from me.
            </small>

            <div className={styles.footer}>
              <Button variant="primary" onClick={onJoinWaitlist}>
                Join the waitlist ❤️
              </Button>

              <Button variant="plain" onClick={onCancel}>
                I&apos;m not sure I&apos;d use it 🤷‍♂️
              </Button>
            </div>
          </div>
        </div>
      </ModalContent>
    </Modal>
  );
}
