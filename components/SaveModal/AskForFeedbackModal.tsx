import {
  OnboardingModals,
  useOnboardingModals,
} from "../../core/onboarding/modals/OnboardingModalsProvider";
import { Button } from "../atomic/Button/Button";
import { Modal } from "../atomic/Modal/Modal";
import { ModalContent } from "../atomic/Modal/ModalContent";
import { ModalHeading } from "../atomic/Modal/ModalHeading";
import styles from "./AskForFeedbackModal.module.css";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
}

export function AskForFeedbackModal({ isOpen, onClose }: IProps) {
  const { open } = useOnboardingModals();

  const onOpenFeedback = () => {
    onClose();
    open(OnboardingModals.Feedback);
  };

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalHeading onClose={onClose}>Feedback?</ModalHeading>
      <ModalContent hasModalHeadingOffset>
        <div className={styles.askForFeedbackModal}>
          <p>Do you mind giving me some feedback?</p>
          <p> Takes less than 30 sec.</p>
          <div className={styles.footer}>
            <Button variant="primary" onClick={onOpenFeedback}>
              Sure 🤙
            </Button>
            <Button variant="plain" onClick={onClose}>
              Nope, leave me alone.
            </Button>
          </div>
        </div>
      </ModalContent>
    </Modal>
  );
}
