import { usePlansModal } from "../../core/plans/context/PlansModalProvider";
import { Modal } from "../atomic/Modal/Modal";
import { ModalContent } from "../atomic/Modal/ModalContent";
import { ModalHeading } from "../atomic/Modal/ModalHeading";
import styles from "./PlansModal.module.css";
import { PlansModalMessage } from "./PlansModalMessage";

export function PlansModal() {
  const { isOpen, close, title, setTitle, content, reason } = usePlansModal();

  return (
    <Modal
      isOpen={isOpen}
      onClose={close}
      onClosed={() => setTitle("")}
      containerStyles={styles.plansModal}
    >
      <ModalHeading border={false} transparent onClose={close}>
        {title || "You reached a PRO feature!"}
      </ModalHeading>
      <ModalContent hasModalHeadingOffset>
        {content || <PlansModalMessage reason={reason} />}
      </ModalContent>
    </Modal>
  );
}
