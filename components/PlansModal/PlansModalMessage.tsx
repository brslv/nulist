import styles from "./PlansModalMessage.module.css";
import React, { ReactNode } from "react";
import { Button } from "../atomic/Button/Button";
import { Title } from "../atomic/Title/Title";
import cn from "classnames";
import Link from "next/link";
import { useAuth } from "../../core/auth/context/AuthProvider";
import { fireClientEvent } from "../../core/clientEvents/fireClientEvent";
import { ClientEvents } from "../../core/clientEvents/ClientEvents";
import { CONSTRAINTS } from "../../core/plans/constraints";

interface IProps {
  reason: ReactNode | null;
}

export function PlansModalMessage({ reason }: IProps) {
  const { user } = useAuth();

  const onUnlockProClick = () => {
    fireClientEvent(ClientEvents.UpgradeProClick);
  };

  return (
    <div>
      <div style={{ position: "relative" }}>
        <div className={styles.priceBox}>
          <span className={styles.price}>$12/month</span>
          <span className={styles.subtext}>Billed monthly</span>
          <Link
            href={`https://nulist.gumroad.com/l/PWfHT?wanted=true&email=${user?.email}`}
            passHref
          >
            <Button
              component="a"
              icon="rocket"
              variant="primary"
              rootClassName={styles.btnRoot}
              className={styles.btn}
              iconProps={{ className: cn(styles.btnIcon) }}
              onClick={onUnlockProClick}
            >
              Unlock Pro
            </Button>
          </Link>
        </div>

        {reason ? <div className={styles.reason}>{reason}</div> : null}

        <div className={styles.listContainer}>
          <div className={styles.unlocks}>With Pro you get...</div>
          <ul className={styles.list}>
            <Item
              emoji="💥"
              subtext={`Go beyond the ${CONSTRAINTS.FREE_PLAN.MAX_LISTS} lists constraint.`}
            >
              Unlimited lists
            </Item>
            <Item
              emoji="🤯"
              subtext={`${CONSTRAINTS.FREE_PLAN.MAX_LIST_ITEMS} items is not enough? It's unlimited on Pro!`}
            >
              Unlimited list items
            </Item>
            <Item
              emoji="️✨"
              subtext={`${CONSTRAINTS.FREE_PLAN.MAX_BUNDLES} bundles is ... meh? Time to go wild with unlimited bundles!`}
            >
              Unlimited bundles
            </Item>
            <Item
              emoji="💅"
              isSoon
              subtext="Customize your lists to fit your style."
            >
              List customizations
            </Item>
            <Item
              emoji="📈"
              isSoon
              subtext="Measure visits, clicks, bounce, etc."
            >
              Analytics
            </Item>
            <Item
              emoji="❤️"
              subtext="I'll do my best to resolve any issues for you as quickly as
              possible."
            >
              Priority support
            </Item>
          </ul>
        </div>
      </div>
    </div>
  );
}

function Item({
  emoji,
  children,
  subtext,
  isSoon = false,
}: {
  emoji?: string;
  children: ReactNode;
  subtext?: ReactNode;
  isSoon?: boolean;
}) {
  return (
    <li className={cn(isSoon ? styles.isSoon : null)}>
      <span className={styles.emoji} role="img">
        {emoji}
      </span>
      {/* <div>
        <span className={styles.check}>
          <Icon name={icon} />
        </span>
      </div> */}
      <Title size={25} weight="medium">
        {children}
      </Title>
      {isSoon ? <div className={styles.soon}>Coming soon</div> : null}
      {subtext ? <p className={styles.subtext}>{subtext}</p> : null}
      {/* {isSoon ? <span className={styles.soon}>Soon</span> : null} */}
    </li>
  );
}
