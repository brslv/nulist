import { CONSTRAINTS } from "../../../core/plans/constraints";

export function BundlesLimitReached() {
  return (
    <div>
      <p style={{ marginTop: 0 }}>
        You&apos;re seeing this, because you have reached the free bundles
        limit. ({CONSTRAINTS.FREE_PLAN.MAX_BUNDLES} bundles)
      </p>
      <p style={{ marginBottom: 0 }}>
        You need to upgrade to unlock unlimited bundles.
      </p>
    </div>
  );
}
