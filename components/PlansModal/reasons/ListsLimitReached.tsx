import { CONSTRAINTS } from "../../../core/plans/constraints";

export function ListsLimitReached() {
  return (
    <div>
      <p style={{ marginTop: 0 }}>
        You&apos;re seeing this, because you have reached the free lists limit.
        ({CONSTRAINTS.FREE_PLAN.MAX_LISTS} lists)
      </p>
      <p style={{ marginBottom: 0 }}>
        You need to upgrade to unlock unlimited lists.
      </p>
    </div>
  );
}
