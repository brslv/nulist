import { CONSTRAINTS } from "../../../core/plans/constraints";

export function ListItemsLimitReached() {
  return (
    <span>
      You&apos;re seeing this, because you reached the list items limit (
      {CONSTRAINTS.FREE_PLAN.MAX_LIST_ITEMS} list items).
    </span>
  );
}
