import React, { useEffect, useState } from "react";
import { Modal, IProps as IModalProps } from "../atomic/Modal/Modal";
import { ModalContent } from "../atomic/Modal/ModalContent";
import { ModalHeading } from "../atomic/Modal/ModalHeading";
import styles from "./WaitlistModal.module.css";

interface IProps {
  isOpen: IModalProps["isOpen"];
  onClose: IModalProps["onClose"];
}

export function WaitlistModal({ isOpen, onClose }: IProps) {
  const [isLoading, setIsLoading] = useState(true);

  const internalOnClose = () => {
    setIsLoading(false);
    if (typeof onClose === "function") onClose();
  };

  useEffect(() => {
    if (isOpen) setIsLoading(true);
  }, [isOpen]);

  const stopLoading = () => {
    setIsLoading(false);
  };

  return (
    <Modal
      onClose={internalOnClose}
      isOpen={isOpen}
      containerStyles={styles.waitlistModal}
    >
      <ModalHeading onClose={internalOnClose}>Waitlist</ModalHeading>
      <ModalContent hasModalHeadingOffset>
        {isLoading ? <div className={styles.loader}>Loading...</div> : null}
        <iframe
          src="https://tally.so/embed/mD5Aq3?hideTitle=1&alignLeft=1"
          width="100%"
          frameBorder={0}
          marginHeight={0}
          height={570}
          marginWidth={0}
          onLoad={stopLoading}
          title="nu⍛st / wait⍛st"
        ></iframe>
      </ModalContent>
    </Modal>
  );
}
