export function FaqList() {
  return (
    <div style={{ margin: `30px auto`, textAlign: "center", padding: 20 }}>
      <p>Have you seen a page that empty before? 😱</p>
      <p>
        I hope to fill it up with content soon.
        <br /> Meanwhile, you can shoot questions on twitter -{" "}
        <a href="https://twitter.com/Brslv" target="_blank" rel="noreferrer">
          @Brslv
        </a>
        .
      </p>
    </div>
  );
}
