import styles from "./ShortcutsList.module.css";

export function ShortcutsList() {
  return (
    <div className={styles.shortcutsList}>
      <div className={styles.row}>
        <div>Navigate list items</div>
        <div className={styles.shortcut}>Up/Down arrows</div>
      </div>
      <div className={styles.row}>
        <div>Focus on the &quot;Create new item&quot; input</div>
        <div className={styles.shortcut}>Cmd or Ctrl + /</div>
      </div>
      <div className={styles.row}>
        <div>Add item bellow</div>
        <div className={styles.shortcut}>Cmd or Ctrl + Arrow down</div>
      </div>
      <div className={styles.row}>
        <div>Open list item&apos;s properties manager</div>
        <div className={styles.shortcut}>Cmd or Ctrl + Enter</div>
      </div>
      <div className={styles.row}>
        <div>Open list item&apos;s properties manager</div>
        <div className={styles.shortcut}>Cmd or Ctrl + Enter</div>
      </div>
      <div className={styles.row}>
        <div>Submit list item&apos;s properties form</div>
        <div className={styles.shortcut}>Shift + Enter</div>
      </div>
    </div>
  );
}
