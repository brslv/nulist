import React, { ReactNode, useState } from "react";
import Image from "next/image";
import { Button } from "../atomic/Button/Button";
import styles from "./HelpManager.module.css";
import cn from "classnames";
import navigation from "../../public/images/help/navigation.gif";
import { Key } from "../atomic/Key/Key";
import { Title } from "../atomic/Title/Title";
import quickFocus from "../../public/images/help/quick-focus.gif";
import addItemBellow from "../../public/images/help/add-item-bellow.gif";
import propertiesManager from "../../public/images/help/properties-manager.gif";
import dragAndDrop from "../../public/images/help/drag-and-drop.gif";

enum HelpSection {
  Navigation,
  QuickFocus,
  AddItemBellow,
  PropertiesManager,
  DragAndDropItems,
}

export function HelpManager() {
  const [activeSection, setActiveSection] = useState<HelpSection>(
    HelpSection.Navigation
  );

  const activate = (section: HelpSection) => () => setActiveSection(section);

  return (
    <div className={styles.helpManager}>
      <div className={styles.left}>
        <Button
          variant="context"
          fullWidth
          noOutlineOnFocus
          onClick={activate(HelpSection.Navigation)}
          rootClassName={cn(styles.btn, {
            [styles.active]: activeSection === HelpSection.Navigation,
          })}
        >
          Navigation
        </Button>
        <Button
          variant="context"
          fullWidth
          noOutlineOnFocus
          onClick={activate(HelpSection.QuickFocus)}
          rootClassName={cn(styles.btn, {
            [styles.active]: activeSection === HelpSection.QuickFocus,
          })}
        >
          Quick focus
        </Button>
        <Button
          variant="context"
          fullWidth
          noOutlineOnFocus
          onClick={activate(HelpSection.AddItemBellow)}
          rootClassName={cn(styles.btn, {
            [styles.active]: activeSection === HelpSection.AddItemBellow,
          })}
        >
          Add item bellow
        </Button>
        <Button
          variant="context"
          fullWidth
          noOutlineOnFocus
          onClick={activate(HelpSection.DragAndDropItems)}
          rootClassName={cn(styles.btn, {
            [styles.active]: activeSection === HelpSection.DragAndDropItems,
          })}
        >
          Drag and drop
        </Button>
        <Button
          variant="context"
          fullWidth
          noOutlineOnFocus
          onClick={activate(HelpSection.PropertiesManager)}
          rootClassName={cn(styles.btn, {
            [styles.active]: activeSection === HelpSection.PropertiesManager,
          })}
        >
          Properties manager
        </Button>
      </div>

      <div className={styles.right}>
        {activeSection === HelpSection.Navigation ? <NavigationHelp /> : null}
        {activeSection === HelpSection.QuickFocus ? <QuickFocus /> : null}
        {activeSection === HelpSection.AddItemBellow ? <AddItemBellow /> : null}
        {activeSection === HelpSection.PropertiesManager ? (
          <PropertiesManager />
        ) : null}
        {activeSection === HelpSection.DragAndDropItems ? (
          <DragAndDropItems />
        ) : null}
      </div>
    </div>
  );
}

function SectionContent({
  title,
  imgSrc,
  imgAlt,
  children,
  subtitle,
  keys,
}: {
  title: ReactNode;
  imgSrc: any;
  imgAlt: string;
  children?: ReactNode;
  subtitle?: ReactNode;
  keys?: ReactNode;
}) {
  return (
    <div>
      <div className={styles.heading}>
        <Title size={30} weight="medium" className={styles.sectionTitle}>
          {title}
        </Title>
        <div className={styles.keys}>{keys}</div>
      </div>
      {subtitle ? (
        <Title size={30} weight="light" className={styles.sectionSubtitle}>
          {subtitle}
        </Title>
      ) : null}
      <Image src={imgSrc} alt={imgAlt} className={styles.image} />
      {children ? <p>{children}</p> : null}
    </div>
  );
}

function NavigationHelp() {
  return (
    <SectionContent
      title="Navigation"
      subtitle="Navigate list items using your keyboard."
      imgSrc={navigation}
      imgAlt="Navigation"
      keys={
        <>
          <Key arrowUp /> / <Key arrowDown />
        </>
      }
    />
  );
}
function QuickFocus() {
  return (
    <SectionContent
      title="Quick focus"
      subtitle='Quickly focus the "Create new item" input.'
      imgSrc={quickFocus}
      imgAlt="Quick Focus"
      keys={
        <>
          <Key cmdOrCtrl /> + <Key forwardSlash />
        </>
      }
    />
  );
}
function AddItemBellow() {
  return (
    <SectionContent
      title="Add item bellow"
      subtitle="Focus on a list item and use the key combination to create a new item bellow."
      imgSrc={addItemBellow}
      imgAlt="Add item bllow"
      keys={
        <>
          <Key cmdOrCtrl /> + <Key arrowDown />
        </>
      }
    />
  );
}
function PropertiesManager() {
  return (
    <SectionContent
      title="Properties manager"
      imgSrc={propertiesManager}
      imgAlt="Properties manager"
      subtitle="Add additional information to the list item, such as content or URL."
      keys={
        <>
          <Key cmdOrCtrl /> + <Key enter />
        </>
      }
    />
  );
}
function DragAndDropItems() {
  return (
    <SectionContent
      title="Drag and drop"
      imgSrc={dragAndDrop}
      imgAlt="Drag and drop"
      subtitle="You can easily reorder list items using drag-and-drop."
    />
  );
}
