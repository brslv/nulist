import {
  OnboardingModals,
  useOnboardingModals,
} from "../../core/onboarding/modals/OnboardingModalsProvider";
import { Button } from "../atomic/Button/Button";
import styles from "./Help.module.css";

export function Help() {
  const { open } = useOnboardingModals();

  return (
    <>
      <div className={styles.help}>
        <Button
          variant="plain"
          icon="questionmark"
          onClick={() => open(OnboardingModals.Help)}
          rootClassName={styles.helpBtnRoot}
        >
          Quick help
        </Button>
      </div>
    </>
  );
}
