import React, { useState } from "react";
import {
  OnboardingModals,
  useOnboardingModals,
} from "../../core/onboarding/modals/OnboardingModalsProvider";
import { Modal, ModalPosition } from "../atomic/Modal/Modal";
import { ModalContent } from "../atomic/Modal/ModalContent";
import { ModalHeading } from "../atomic/Modal/ModalHeading";
import { ShortcutsList } from "./ShortcutsList";
import styles from "./HelpModal.module.css";
import { FaqList } from "./FaqList";
import { Tabs, ITabItem } from "../atomic/Tabs/Tabs";

export enum TabTypes {
  Shortcuts,
  FAQ,
}

export function HelpModal() {
  const { isOpen, close } = useOnboardingModals();
  const [activeTab, setActiveTab] = useState<TabTypes>(TabTypes.Shortcuts);
  const _isOpen = isOpen(OnboardingModals.Help);

  const onTabChange = (item: ITabItem<TabTypes>) => setActiveTab(item.type);

  const sections: Array<ITabItem<TabTypes>> = [
    {
      type: TabTypes.Shortcuts,
      label: "Shortcuts",
      isActive: activeTab === TabTypes.Shortcuts,
    },
    {
      type: TabTypes.FAQ,
      label: "FAQ",
      isActive: activeTab === TabTypes.FAQ,
    },
  ];

  return (
    <Modal isOpen={_isOpen} onClose={close} position={ModalPosition.Center}>
      <ModalHeading onClose={close}>Quick help</ModalHeading>
      <ModalContent className={styles.helpModal} hasModalHeadingOffset noSpace>
        <Tabs
          items={sections}
          onChange={(item: ITabItem<TabTypes>) => onTabChange(item)}
          className={styles.tabs}
        />

        {activeTab === TabTypes.Shortcuts ? <ShortcutsList /> : null}
        {activeTab === TabTypes.FAQ ? <FaqList /> : null}
      </ModalContent>
    </Modal>
  );
}
