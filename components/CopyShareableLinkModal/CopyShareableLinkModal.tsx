import CopyToClipboard from "react-copy-to-clipboard";
import { Button } from "../atomic/Button/Button";
import { Input } from "../atomic/Input/Input";
import { Modal, ModalPosition } from "../atomic/Modal/Modal";
import { ModalContent } from "../atomic/Modal/ModalContent";
import { ModalHeading } from "../atomic/Modal/ModalHeading";
import styles from "./CopyShareableLinkModal.module.css";

interface IProps {
  isOpen: boolean;
  onClose: () => void;

  value: string;
  onCopy: () => void;
  isCopied: boolean;
}

export function CopyShareableLinkModal({
  isOpen,
  onClose,
  value,
  onCopy,
  isCopied,
}: IProps) {
  return (
    <Modal
      containerStyles={styles.linkModal}
      position={ModalPosition.TopCenter}
      isOpen={isOpen}
      onClose={onClose}
    >
      <ModalHeading onClose={onClose}>Shareable link</ModalHeading>

      <ModalContent hasModalHeadingOffset>
        <div className={styles.content}>
          <div className={styles.inputContainer}>
            <Input fullWidth type="text" disabled value={value} />
          </div>
          <CopyToClipboard text={value} onCopy={onCopy}>
            <Button variant="primary">{isCopied ? "Copied 🥳" : "Copy"}</Button>
          </CopyToClipboard>
        </div>
      </ModalContent>
    </Modal>
  );
}
