import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useUserLists } from "../../core/list/context/UserListsProvider";
import { ListVisibilities } from "../../core/list/types/ListVisibilities";
import { ListViewTypes } from "../../core/list/types/ListViewTypes";
import {
  IRoundedTabItem,
  RoundedTabs,
} from "../atomic/RoundedTabs/RoundedTabs";
import styles from "./DashboardSections.module.css";
import { BundlesStack } from "../BundlesStack/BundlesStack";
import { Lists } from "../ListsStack/Lists";

export function DashboardSections() {
  const router = useRouter();
  const visibilityFromUrl = router.query.visibility;
  const viewTypeFromUrl = router.query.type;
  const { setLists } = useUserLists();
  const [state, setState] = useState<{
    visibility: ListVisibilities | null;
    viewType: ListViewTypes | null;
  }>({ visibility: null, viewType: null });

  useEffect(() => {
    setState(prev => ({
      ...prev,
      viewType: (viewTypeFromUrl as ListViewTypes) || ListViewTypes.Lists,
      visibility:
        (visibilityFromUrl as ListVisibilities) || ListVisibilities.All,
    }));
  }, [visibilityFromUrl, viewTypeFromUrl]);

  const onVisibilityChange = (item: IRoundedTabItem<ListVisibilities>) => {
    setLists(null);
    router.push({ query: { visibility: item.type, type: state.viewType } });
    setState(prev => ({ ...prev, visibility: item.type }));
  };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const onViewTypeChange = (item: IRoundedTabItem<ListViewTypes>) => {
    router.push({ query: { visibility: state.visibility, type: item.type } });
    setState(prev => ({ ...prev, viewType: item.type }));
  };

  const visibilities = [
    {
      type: ListVisibilities.All,
      label: "All",
      isActive: state.visibility === ListVisibilities.All,
    },
    {
      type: ListVisibilities.Private,
      label: "Private",
      isActive: state.visibility === ListVisibilities.Private,
    },
    {
      type: ListVisibilities.Public,
      label: "Public",
      isActive: state.visibility === ListVisibilities.Public,
    },
  ];

  const viewTypes = [
    {
      type: ListViewTypes.Lists,
      label: "Lists",
      isActive: state.viewType === ListViewTypes.Lists,
    },
    {
      type: ListViewTypes.Bundles,
      label: "Bundles",
      isActive: state.viewType === ListViewTypes.Bundles,
    },
  ];

  return (
    <div className={styles.dashboardSections}>
      <div className={styles.controls}>
        <div className={styles.left}>
          <RoundedTabs
            items={viewTypes}
            onChange={(item: IRoundedTabItem<ListViewTypes>) => {
              onViewTypeChange(item);
            }}
          />
        </div>
        <RoundedTabs
          items={visibilities}
          onChange={(item: IRoundedTabItem<ListVisibilities>) =>
            onVisibilityChange(item)
          }
          className={styles.visibilityControls}
        />
      </div>

      <hr className={styles.separator} />

      {state.viewType === ListViewTypes.Bundles ? (
        <BundlesStack visibility={state.visibility} />
      ) : (
        <Lists visibility={state.visibility} />
      )}
    </div>
  );
}
