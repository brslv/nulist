import Link from "next/link";
import React from "react";
import { IListItem } from "../../core/list/types/IListItem";
import { isRichContentEmpty } from "../../lib/isRichContentEmpty";
import { Button } from "../atomic/Button/Button";
import styles from "./ListItemPropertyIcons.module.css";
import cn from "classnames";

interface IProps {
  item: IListItem;
}

export function ListItemPropertyIcons({ item }: IProps) {
  return (
    <div className={styles.propertyIconsContainer}>
      {item.url ? (
        <div className={styles.iconContainer}>
          <Link href={item.url} passHref>
            <Button
              target="_blank"
              component="a"
              variant="basic"
              className={cn(styles.linkBtn, styles.propertyIcon)}
              rootClassName={styles.linkBtnRoot}
              tooltip="Visit link"
              narrow
              onClick={e => e.stopPropagation()}
              icon="link"
              iconOnly
            />
          </Link>
        </div>
      ) : null}

      {item.content && !isRichContentEmpty(item.content) ? (
        <div className={styles.iconContainer}>
          <Button
            disabled
            variant="nostyle"
            className={styles.propertyIcon}
            icon="paragraph"
            iconOnly
            narrow
          />
        </div>
      ) : null}
    </div>
  );
}
