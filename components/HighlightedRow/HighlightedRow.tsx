import { ReactNode } from "react";
import styles from "./HighlightedRow.module.css";
import cn from "classnames";

interface IProps {
  children: ReactNode;
  isActive?: boolean;
  onClick?: () => void;
}
export function HighlightedRow({
  children,
  onClick,
  isActive = false,
}: IProps) {
  return (
    <div
      tabIndex={-1}
      className={cn(styles.highlightedRow, {
        [styles.isActive]: isActive,
        [styles.clickable]: typeof onClick === "function",
      })}
      onClick={onClick}
    >
      {children}
    </div>
  );
}
