import React, { ReactNode } from "react";
import { ModalCloseIcon } from "./ModalCloseIcon";
import styles from "./ModalHeading.module.css";
import cn from "classnames";

export interface IProps {
  children?: ReactNode;
  withCloseIcon?: boolean;
  transparent?: boolean;
  gradient?: boolean;
  onClose?: () => void;
  border?: boolean;
  className?: any;
}

export function ModalHeading({
  children,
  onClose,
  className,
  transparent = false,
  gradient = false,
  withCloseIcon = true,
  border = true,
}: IProps) {
  return (
    <div
      className={cn(
        styles.modalHeading,
        {
          [styles.border]: border,
          [styles.transparent]: transparent,
          [styles.gradient]: gradient,
        },
        className
      )}
    >
      {children}
      {withCloseIcon && onClose && !children ? <div /> : null}
      {withCloseIcon && onClose ? <ModalCloseIcon onClick={onClose} /> : null}
    </div>
  );
}
