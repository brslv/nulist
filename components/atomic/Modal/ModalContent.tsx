import { ReactNode } from "react";
import styles from "./ModalContent.module.css";
import cn from "classnames";

interface IProps {
  children: ReactNode;
  hasModalHeadingOffset?: boolean;
  className?: any;
  noSpace?: boolean;
}

export function ModalContent({
  children,
  hasModalHeadingOffset,
  className,
  noSpace = false,
}: IProps) {
  return (
    <div
      className={cn(
        styles.modalContent,
        {
          [styles.modalHeadingOffset]: hasModalHeadingOffset,
          [styles.noSpace]: noSpace,
        },
        className
      )}
    >
      {children}
    </div>
  );
}
