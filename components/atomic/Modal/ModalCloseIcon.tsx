import React from "react";
import { Button } from "../Button/Button";
import styles from "./ModalCloseIcon.module.css";

interface IProps {
  onClick: () => void;
}

export function ModalCloseIcon({ onClick }: IProps) {
  return (
    <Button
      variant="plain"
      icon="close"
      iconOnly
      narrow
      iconProps={{ className: styles.modalCloseIcon }}
      onClick={onClick}
    />
  );
}
