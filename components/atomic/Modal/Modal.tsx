import { forwardRef, ReactNode, useEffect, useRef, useState } from "react";
import styles from "./Modal.module.css";
import Image from "next/image";
import cn from "classnames";
import FocusTrap from "focus-trap-react";
import { useOverlayInteractions } from "../../../core/useOverlayInteractions";
import { CSSTransition } from "react-transition-group";
import { Portal } from "../Portal/Portal";
import blur from "../../../public/images/bg-blur.svg";
import useMeasure from "react-use-measure";
import { useOnKeyUp } from "../../../lib/useOnKeyUp";

export enum ModalPosition {
  Center = "center",
  TopCenter = "topCenter",
  BottomRight = "bottomRight",
}

export interface IProps {
  children: ReactNode;
  isOpen: boolean;
  onClose?: () => void;
  onClosed?: () => void;
  closeOnOutsideClick?: boolean;
  containerStyles?: any;
  helperTop?: ReactNode;
  helper?: ReactNode;
  position?: ModalPosition;
  bgBlur?: boolean;
  focusTrap?: boolean;

  // Set this to true if the page, where the given modal should be displayed,
  // is SSR (server side rendered). This will drop the use of animations, so that the modal
  // is visible in the HTML of the server side render output.
  //
  // If it's false, the modal will not be present in the resulting HTML, because the animation
  // unmounts the component and the modal never gets rendered on the server.
  disableAnimationsForSSR?: boolean;
}

const Bg = forwardRef((_props: any, ref: any) => {
  return <div ref={ref} className={styles.bg} />;
});

const InternalContainer = forwardRef(
  (
    {
      isOpen,
      position,
      helperTop,
      wrapperRef,
      containerStyles,
      bgBlur,
      children,
      helper,
      focusTrap,
    }: Partial<IProps> & { wrapperRef: any },
    ref: any
  ) => {
    const [containerRef, bounds] = useMeasure();
    return (
      <FocusTrap active={isOpen && focusTrap}>
        <div
          ref={ref}
          className={cn(styles.modal, {
            [styles.center]: position === ModalPosition.Center,
            [styles.topCenter]: position === ModalPosition.TopCenter,
            [styles.bottomRight]: position === ModalPosition.BottomRight,
          })}
        >
          <div className={styles.wrapper} ref={wrapperRef}>
            {helperTop ? <>{helperTop}</> : null}
            <div
              ref={containerRef}
              className={cn(styles.container, containerStyles)}
            >
              {bgBlur ? (
                <div
                  className={styles.bgBlurContainer}
                  style={{ height: bounds.height, overflow: "hidden" }}
                >
                  <Image
                    src={blur}
                    alt="Blurry background"
                    className={styles.bgBlur}
                  />
                </div>
              ) : null}
              <div>{children}</div>
            </div>
            {helper ? (
              <div className={styles.helperContainer}>{helper}</div>
            ) : null}
          </div>
        </div>
      </FocusTrap>
    );
  }
);

export function Modal({
  children,
  isOpen,
  onClose,
  onClosed,
  closeOnOutsideClick,
  containerStyles,
  helperTop,
  helper,
  position = ModalPosition.Center,
  bgBlur = false,
  disableAnimationsForSSR = false,
  focusTrap = true,
}: IProps) {
  const bgRef = useRef(null);
  const modalRef = useRef(null);
  const wrapperRef = useRef(null);
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    setIsMounted(true);
  }, []);

  useOnKeyUp<KeyboardEvent>({
    fn: e => {
      if (isOpen && e.key === "Escape") {
        console.log("escape", onClose);
        onClose && onClose();
      }
    },
  });

  useOverlayInteractions({
    ref: wrapperRef,
    onClose,
    isOpen,
    closeOnOutsideClick,
  });

  if (disableAnimationsForSSR && !isMounted && isOpen) {
    return (
      <>
        <Bg ref={bgRef} />
        <InternalContainer
          isOpen={isOpen}
          containerStyles={containerStyles}
          helperTop={helperTop}
          helper={helper}
          position={position}
          bgBlur={bgBlur}
          wrapperRef={wrapperRef}
          focusTrap={focusTrap}
        >
          {children}
        </InternalContainer>
      </>
    );
  }

  return (
    <Portal>
      <CSSTransition
        nodeRef={bgRef}
        in={isOpen}
        classNames={{
          enter: styles.animBgEnter,
          enterActive: styles.animBgEnterActive,
          exit: styles.animBgExit,
          exitActive: styles.animBgExitActive,
        }}
        onExited={onClosed}
        timeout={200}
        unmountOnExit
      >
        <Bg ref={bgRef} />
      </CSSTransition>
      <CSSTransition
        nodeRef={modalRef}
        in={isOpen}
        classNames={{
          enter: styles.animEnter,
          enterActive: styles.animEnterActive,
          exit: styles.animExit,
          exitActive: styles.animExitActive,
        }}
        timeout={200}
        unmountOnExit
      >
        <InternalContainer
          isOpen={isOpen}
          containerStyles={containerStyles}
          helperTop={helperTop}
          helper={helper}
          position={position}
          bgBlur={bgBlur}
          ref={modalRef}
          wrapperRef={wrapperRef}
          focusTrap={focusTrap}
        >
          {children}
        </InternalContainer>
      </CSSTransition>
    </Portal>
  );
}
