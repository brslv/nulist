import { ReactNode } from "react";
import styles from "./ModalHelper.module.css";

interface IProps {
  children: ReactNode;
}
export function ModalHelper({ children }: IProps) {
  return <div className={styles.modalHelper}>{children}</div>;
}
