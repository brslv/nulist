import { MouseEvent } from "react";
import cn from "classnames";
import { Button } from "../Button/Button";
import styles from "./Toggle.module.css";

interface Props {
  id?: string;
  isOn: boolean;
  onToggle: (state: boolean) => void;
  disabled?: boolean;
}

export default function Toggle({
  id,
  isOn,
  onToggle,
  disabled = false,
}: Props) {
  const onClick = (e: MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    e.stopPropagation();
    if (!disabled) {
      onToggle(!isOn);
    }
  };

  return (
    <Button
      id={id}
      disabled={disabled}
      variant="nostyle"
      style={{ padding: 0 }}
      onClick={onClick}
      noOutlineOnFocus
      className={cn(styles.innerRoot, { [styles.disabled]: disabled })}
      rootClassName={cn(styles.root)}
    >
      <div
        className={cn(styles.rail, styles[isOn ? "on" : "off"], {
          [styles.disabled]: disabled,
        })}
      >
        <div className={styles.btn} />
      </div>
    </Button>
  );
}
