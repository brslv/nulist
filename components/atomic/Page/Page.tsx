import styles from "./Page.module.css";
import cn from "classnames";
import React, { ReactNode, useEffect } from "react";
import { Title } from "../Title/Title";
import { usePageControls } from "../../PageControls/PageControlsProvider";
import { IconKey } from "../../../lib/icons";
import { Icon } from "../Icon/Icon";
import { Breadcrumbs } from "../../Breadcrumbs/Breadcrumbs";
import { IBreadcrumb } from "../../../core/breadcrumbs/types/IBreadcrumb";
import { useBreadcrumbs } from "../../../core/breadcrumbs/context/BreadcrumbsProvider";

interface IProps {
  children: ReactNode;
}

export function Page({ children }: IProps) {
  return <div className={styles.page}>{children}</div>;
}

interface IPageMainProps extends IProps {
  withPadding?: boolean;
  wide?: boolean;
  breadcrumbs?: IBreadcrumb[];
}

export function PageMain({
  children,
  withPadding = true,
  wide = false,
  breadcrumbs,
}: IPageMainProps) {
  const { setBreadcrumbs } = useBreadcrumbs();
  const { isVisible } = usePageControls();

  useEffect(() => {
    if (breadcrumbs !== undefined) setBreadcrumbs(breadcrumbs);
  }, [breadcrumbs, setBreadcrumbs]);

  return (
    <div
      className={cn(styles.pageMain, {
        [styles.withPadding]: withPadding,
        [styles.wide]: wide,
        [styles.withoutPageControls]: !isVisible,
      })}
    >
      <div className={styles.breadcrumbs}>
        {breadcrumbs ? <Breadcrumbs /> : null}
      </div>
      {children}
    </div>
  );
}

interface IPageTitleProps {
  icon?: IconKey;
  title: ReactNode;
  right?: ReactNode;
  condenced?: boolean;
}

export function PageTitle({
  icon,
  title,
  right,
  condenced = false,
}: IPageTitleProps) {
  return (
    <>
      <div className={cn(styles.pageTitle, { [styles.condenced]: condenced })}>
        <Title size={50} weight="semiBold" className={styles.title}>
          {icon ? <Icon name={icon} className={styles.titleIcon} /> : null}
          {title}
        </Title>

        {right}
      </div>
    </>
  );
}
