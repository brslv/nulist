import { ReactNode } from "react";
import styles from "./FormRow.module.css";
import cn from "classnames";

interface IProps {
  children: ReactNode;
  isInline?: boolean;
  bottomSpace?: boolean;
  spaceBetween?: boolean;
  background?: boolean;
}

export function FormRow({
  children,
  isInline,
  bottomSpace = true,
  spaceBetween = false,
  background = false,
}: IProps) {
  return (
    <div
      className={cn(styles.formRow, {
        [styles.isInline]: isInline,
        [styles.bottomSpace]: bottomSpace,
        [styles.spaceBetween]: spaceBetween,
        [styles.background]: background,
      })}
    >
      {children}
    </div>
  );
}
