import { ComponentPropsWithoutRef } from "react";
import styles from "./Checkbox.module.css";
import cn from "classnames";

export function Checkbox(props: ComponentPropsWithoutRef<"input">) {
  return (
    <input
      {...props}
      type="checkbox"
      className={cn(styles.checkbox, props.className)}
    />
  );
}
