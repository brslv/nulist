import React, { ReactNode } from "react";
import { Button } from "../Button/Button";
import styles from "./AppMessage.module.css";
import cn from "classnames";
import { ButtonVariant } from "../Button/types/ButtonVariant";

interface IProps {
  children: ReactNode;
  active?: boolean;
  onClose?: () => void;
  rounded?: boolean;
  variant?: "basic" | "info";
  closeBtnVariant: ButtonVariant;
}

export function AppMessage({
  children,
  onClose,
  active = true,
  rounded = true,
  variant = "basic",
  closeBtnVariant = "nostyle",
}: IProps) {
  if (!active) return null;

  return (
    <div
      className={cn(styles.appMessage, {
        [styles.rounded]: rounded,
        [styles[variant]]: true,
      })}
    >
      <div className={styles.message}>{children}</div>
      {typeof onClose === "function" ? (
        <Button
          variant={closeBtnVariant}
          icon="close"
          iconOnly
          onClick={onClose}
          narrow
        />
      ) : null}
    </div>
  );
}
