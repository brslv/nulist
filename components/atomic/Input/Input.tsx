import styles from "./Input.module.css";
import inputFieldStyles from "../../../styles/InputField.module.css";
import cn from "classnames";
import { useRef, useEffect, ComponentPropsWithoutRef, forwardRef } from "react";

interface IProps {
  invisibleBorders?: boolean;
  fullWidth?: boolean;
  error?: string | null;
}

// eslint-disable-next-line
export const Input = forwardRef(
  (
    {
      value,
      onChange,
      name,
      type,
      className,
      placeholder,
      disabled = false,
      autoFocus = false,
      invisibleBorders = false,
      fullWidth = false,
      error,
      ...rest
    }: IProps & ComponentPropsWithoutRef<"input">,
    ref: any
  ) => {
    const innerRef = useRef(null);
    const inputRef = ref || innerRef;
    const classNames = cn(
      inputFieldStyles.inputField,
      styles.input,
      {
        [inputFieldStyles.invisibleBorders]: invisibleBorders,
        [styles.fullWidth]: fullWidth,
        [inputFieldStyles.error]: !!error,
      },
      className
    );

    useEffect(() => {
      if (autoFocus && inputRef && inputRef.current) {
        (inputRef as any).current.focus();
      }
    }, [autoFocus, inputRef]);

    return (
      <>
        <input
          ref={inputRef}
          value={value}
          onChange={onChange}
          name={name}
          type={type}
          placeholder={placeholder}
          disabled={disabled}
          className={classNames}
          {...rest}
        />
        {error ? <span className={styles.error}>{error}</span> : null}
      </>
    );
  }
);
