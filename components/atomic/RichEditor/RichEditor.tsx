import { useEditor, EditorContent } from "@tiptap/react";
import { Placeholder } from "@tiptap/extension-placeholder";
import StarterKit from "@tiptap/starter-kit";
import styles from "./RichEditor.module.css";
import { useEffect, useState } from "react";

interface IProps {
  value: string;
  onUpdate: (html: string) => void;
  autoFocus?: boolean;
  onBlur?: () => void;
  onFocus?: () => void;
}

export function RichEditor({
  value,
  onUpdate,
  autoFocus = false,
  onFocus,
  onBlur,
}: IProps) {
  const editor = useEditor({
    extensions: [
      StarterKit,
      Placeholder.configure({
        placeholder: "Content...",
      }),
    ],
    autofocus: autoFocus ? "end" : false,
    content: value,
    onUpdate: data => {
      onUpdate(data.editor.getHTML());
    },
  });
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    if (editor && editor.commands && !isMounted) {
      editor.commands.setContent(value);
      setIsMounted(true);
    }
  }, [editor, isMounted, value]);

  return (
    <div className={styles.richEditor}>
      <EditorContent onBlur={onBlur} onFocus={onFocus} editor={editor} />
    </div>
  );
}
