interface IProps {
  children?: string;
}

export function RichEditorView({ children }: IProps) {
  return (
    <div className="ProseMirrorContent">
      <div dangerouslySetInnerHTML={{ __html: children || "" }} />
    </div>
  );
}
