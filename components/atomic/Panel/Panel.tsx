import { ReactNode } from "react";
import { Button } from "../Button/Button";
import styles from "./Panel.module.css";
import cn from "classnames";

interface IProps {
  children: ReactNode;
  onClose?: () => void;
  className?: any;
}

export function Panel({ children, onClose, className }: IProps) {
  return (
    <div className={cn(styles.panel, className)}>
      {typeof onClose === "function" ? (
        <div className={styles.closeBtnContainer}>
          <Button icon="close" iconOnly narrow onClick={onClose} />
        </div>
      ) : null}
      {children}
    </div>
  );
}
