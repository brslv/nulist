import styles from "./RoundedTabs.module.css";
import cn from "classnames";
import { ComponentPropsWithoutRef } from "react";

export interface IRoundedTabItem<T> {
  type: T;
  label: string;
  isActive: boolean;
}

interface IProps<T> {
  items: Array<IRoundedTabItem<T>>;
  onChange: (item: IRoundedTabItem<T>) => void;
  className?: any;
  tabProps?: ComponentPropsWithoutRef<"button">;
}

export function RoundedTabs<T>({
  items,
  onChange,
  className,
  tabProps,
}: IProps<T>) {
  return (
    <div className={cn(styles.tabs, className)}>
      {items.map(item => (
        <button
          key={item.label}
          onClick={() => {
            if (!item.isActive) onChange(item);
          }}
          {...tabProps}
          className={cn(
            styles.tab,
            {
              [styles.active]: item.isActive,
            },
            tabProps?.className
          )}
        >
          {item.label}
        </button>
      ))}
    </div>
  );
}
