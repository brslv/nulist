import { ReactNode, useEffect, useState } from "react";
import { createPortal } from "react-dom";

interface IProps {
  children: ReactNode;
  id?: string;
}

export function Portal({ children, id = "portal" }: IProps) {
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    setIsMounted(true);

    return () => setIsMounted(false);
  }, []);

  return isMounted
    ? createPortal(children, document.querySelector(`#${id}`) as Element)
    : null;
}

export default Portal;
