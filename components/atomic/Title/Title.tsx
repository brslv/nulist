import styles from "./Title.module.css";
import cn from "classnames";
import { ReactNode } from "react";

interface IProps {
  children: ReactNode;
  component?: "h1" | "h2" | "h3" | "h4" | "h5" | "h6" | "p" | "div";
  size?: number;
  weight?:
    | "thin"
    | "light"
    | "regular"
    | "medium"
    | "semiBold"
    | "bold"
    | "black";
  uppercase?: boolean;
  className?: any;
  style?: any;
}

export function Title({
  children,
  component = "h1",
  size = 10,
  weight = "bold",
  uppercase = false,
  className,
  style,
}: IProps) {
  const Component = component;
  const sizeStyle = styles[`size${size}`];
  const weightStyle = styles[weight];

  return (
    <Component
      className={cn(
        styles.title,
        sizeStyle,
        weightStyle,
        {
          [styles.uppercase]: uppercase,
        },
        className
      )}
      style={style}
    >
      {children}
    </Component>
  );
}
