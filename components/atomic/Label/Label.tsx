import React, { ComponentPropsWithoutRef } from "react";
import styles from "./Label.module.css";
import cn from "classnames";

interface IProps {
  error?: string | null;
}

export function Label(props: IProps & ComponentPropsWithoutRef<"label">) {
  return (
    <label
      {...props}
      className={cn(styles.label, { [styles.error]: !!props.error })}
    >
      {props.children}
    </label>
  );
}
