import styles from "./Button.module.css";
import cn from "classnames";
import { Icon, IProps as IconProps } from "../Icon/Icon";
import { forwardRef, ReactNode, ComponentPropsWithoutRef } from "react";
import { IconKey } from "../../../lib/icons";
import { ButtonVariant } from "./types/ButtonVariant";
import { WithTooltip } from "../../../lib/WithTooltip";

export type ButtonType = ComponentPropsWithoutRef<"button">["type"];

interface IProps {
  component?: "button" | "a" | "div";
  children?: ReactNode;
  className?: any;
  rootClassName?: any;
  target?: "_blank";
  icon?: IconKey | null;
  iconProps?: Partial<IconProps>;
  fullWidth?: boolean;
  center?: boolean;
  iconOnly?: boolean;
  variant?: ButtonVariant;
  dimmed?: boolean;
  narrow?: boolean;
  disabled?: boolean;
  noOutlineOnFocus?: boolean;
  tooltip?: string;
  fontSize?: 10 | 20 | 25 | 30 | 40 | 50 | 60 | 70 | 80;
}

// eslint-disable-next-line
export const Button = forwardRef(
  (
    {
      component = "button",
      children,
      className,
      rootClassName,
      target,
      tooltip,
      icon = null,
      iconProps,
      fullWidth = false,
      center = false,
      iconOnly = false,
      type = "button",
      variant = "basic",
      dimmed = false,
      narrow = false,
      disabled = false,
      noOutlineOnFocus = false,
      fontSize = 20,
      ...rest
    }: IProps & ComponentPropsWithoutRef<"button">,
    ref
  ) => {
    const variantStyles = styles[`${variant}Variant`];
    const Component: any = component;
    const notClickable = disabled || component === "div";

    const componentProps: Partial<IProps & ComponentPropsWithoutRef<"button">> =
      {
        className: cn(
          styles.button,
          variantStyles,
          {
            [styles.fullWidth]: fullWidth,
            [styles.center]: center,
            [styles.dimmed]: dimmed,
            [styles.narrow]: narrow,
            [styles.disabled]: disabled,
            [styles.notClickable]: notClickable,
            [styles.noOutlineOnFocus]: noOutlineOnFocus,
          },
          rootClassName
        ),
      };
    if (component === "button") componentProps.type = type as ButtonType;
    if (component === "a" && target === "_blank")
      componentProps.target = "_blank";

    return (
      <Component ref={ref} disabled={disabled} {...rest} {...componentProps}>
        <WithTooltip disabled={!tooltip} content={tooltip}>
          <span
            className={cn(
              styles.buttonInner,
              {
                [styles.withIcon]: icon !== null,
                [styles.iconOnly]: iconOnly,
                [styles.narrow]: narrow,
                [styles.notClickable]: notClickable,
                [styles[`font${fontSize}`]]: true,
              },
              className
            )}
          >
            {icon ? (
              <Icon
                name={icon}
                {...iconProps}
                className={cn(
                  styles.icon,
                  iconProps ? iconProps.className : ""
                )}
              />
            ) : null}
            {children}
          </span>
        </WithTooltip>
      </Component>
    );
  }
);
