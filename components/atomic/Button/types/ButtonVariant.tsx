export type ButtonVariant =
  | "nostyle"
  | "basic"
  | "plain"
  | "primary"
  | "accent"
  | "danger"
  | "context";
