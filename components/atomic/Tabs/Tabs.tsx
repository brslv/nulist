import styles from "./Tabs.module.css";
import cn from "classnames";
import { ComponentPropsWithoutRef } from "react";

export interface ITabItem<T> {
  type: T;
  label: string;
  isActive: boolean;
}

interface IProps<T> {
  items: Array<ITabItem<T>>;
  onChange: (item: ITabItem<T>) => void;
  className?: any;
  tabProps?: ComponentPropsWithoutRef<"button">;
}

export function Tabs<T>({ items, onChange, className, tabProps }: IProps<T>) {
  return (
    <div className={cn(styles.tabsList, className)}>
      {items.map(item => (
        <button
          key={item.label}
          onClick={() => onChange(item)}
          {...tabProps}
          className={cn(
            styles.tab,
            {
              [styles.active]: item.isActive,
            },
            tabProps?.className
          )}
        >
          {item.label}
        </button>
      ))}
    </div>
  );
}
