import FocusTrap from "focus-trap-react";
import {
  ReactNode,
  MouseEvent,
  RefObject,
  useState,
  useEffect,
  useRef,
} from "react";
import { useOnClickOutside } from "../../../lib/useClickOutside";
import styles from "./ContextMenu.module.css";
import { CSSTransition } from "react-transition-group";

const DEFAULT_TOP_OFFSET = 40;

interface IProps {
  children: ReactNode;
  isOpen?: boolean;
  onOutsideClick?: (e: MouseEvent) => void;
  style?: any;
  addTopOffset?: number;
  anchorRef?: RefObject<any>;
  focusTrap?: boolean;
}

export function ContextMenu({
  children,
  isOpen = false,
  onOutsideClick,
  style = {},
  addTopOffset = 0,
  focusTrap = false,
  anchorRef,
}: IProps) {
  const contextMenuRef = useRef(null);
  const [topOffset, setTopOffset] = useState<number | null>(null);

  const internalOnOutsideClick = (e: MouseEvent) => {
    if (typeof onOutsideClick === "function") {
      onOutsideClick(e);
    }
  };

  const calculateContextMenuTopOffset = (ref: RefObject<any>) => {
    if (ref && ref.current) {
      return ref.current.getBoundingClientRect().height;
    }
  };

  useEffect(() => {
    if (!anchorRef || !anchorRef.current) {
      setTopOffset(DEFAULT_TOP_OFFSET);
    }

    if (isOpen && anchorRef && anchorRef.current) {
      const topOffset = calculateContextMenuTopOffset(anchorRef);
      setTopOffset(topOffset);
    }
  }, [isOpen, anchorRef]);

  useOnClickOutside(contextMenuRef, internalOnOutsideClick);

  return (
    <CSSTransition
      nodeRef={contextMenuRef}
      in={isOpen && topOffset !== null}
      classNames={{
        enter: styles.animEnter,
        enterActive: styles.animEnterActive,
        exit: styles.animExit,
        exitActive: styles.animExitActive,
      }}
      timeout={200}
      unmountOnExit
    >
      <FocusTrap active={focusTrap}>
        <div
          ref={contextMenuRef}
          style={{
            ...style,
            paddingBottom: 10,
            top: (topOffset || 0) + addTopOffset,
          }}
          className={styles.contextMenu}
        >
          {children}
        </div>
      </FocusTrap>
    </CSSTransition>
  );
}
