import { ReactNode } from "react";
import { Title } from "../../atomic/Title/Title";
import styles from "./ContextMenuTitle.module.css";

export function ContextMenuTitle({ children }: { children: ReactNode }) {
  return (
    <div className={styles.contextMenuTitle}>
      <Title uppercase size={10} weight="light" className={styles.title}>
        {children}
      </Title>
    </div>
  );
}
