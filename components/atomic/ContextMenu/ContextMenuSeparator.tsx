import styles from "./ContextMenuSeparator.module.css";

export function ContextMenuSeparator() {
  return <hr className={styles.contextMenuSeparator} />;
}
