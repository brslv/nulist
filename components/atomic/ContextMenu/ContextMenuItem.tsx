import cn from "classnames";
import { Button } from "../Button/Button";
import { ButtonVariant } from "../Button/types/ButtonVariant";
import { ReactNode, useEffect, useRef } from "react";
import { IconKey } from "../../../lib/icons";
import styles from "./ContextMenuItem.module.css";
import Toggle from "../Toggle/Toggle";

interface IProps {
  children: ReactNode;
  soon?: boolean;
  active?: boolean;
  onClick?: () => void;
  variant?: ButtonVariant;
  fullWidth?: boolean;
  icon?: IconKey;
  toggle?: boolean;
  toggleState?: boolean;
  onToggle?: (state: boolean) => void;
  disabled?: boolean;
}

/**
 * @param {Object} props
 * @param {import("../Button/Button").ButtonVariant} [props.variant=plain]
 */
export function ContextMenuItem({
  icon,
  onClick,
  children,
  active = false,
  toggle = false,
  toggleState,
  onToggle,
  disabled = false,
  soon = false,
  variant = "context",
  fullWidth = false,
}: IProps) {
  const ref = useRef();

  useEffect(() => {
    if (active && ref && ref.current) (ref as any).current.focus();
  }, [active]);

  if (toggle && onToggle && toggleState !== undefined) {
    return (
      <Button
        ref={ref}
        component="div"
        center={false}
        fullWidth={fullWidth}
        variant={variant}
        icon={icon}
        iconProps={{
          className: cn(styles.icon, {
            [styles.disabledIcon]: soon || disabled,
          }),
        }}
        onClick={onClick}
        disabled={disabled}
        dimmed={disabled}
        className={cn(styles.toggle, { [styles.soon]: soon })}
      >
        <div
          style={{
            marginRight: 30,
            flex: 1,
          }}
        >
          <span className={styles.children}>{children}</span>

          {soon ? <span className={styles.soonLabel}>Soon</span> : null}
        </div>

        <Toggle
          isOn={toggleState}
          onToggle={state => onToggle(state)}
          disabled={disabled}
        />
      </Button>
    );
  }

  return (
    <Button
      ref={ref}
      center={false}
      fullWidth={fullWidth}
      type="button"
      variant={variant}
      icon={icon}
      iconProps={{
        className: cn(styles.icon, { [styles.disabledIcon]: soon || disabled }),
      }}
      onClick={onClick}
      disabled={disabled}
      dimmed={disabled}
      className={cn({ [styles.soon]: soon })}
      rootClassName={cn(styles.btnRoot)}
    >
      <span className={styles.children}>{children}</span>
      {soon ? <span className={styles.soonLabel}>Soon</span> : null}
    </Button>
  );
}
