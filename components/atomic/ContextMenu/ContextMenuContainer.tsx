import styles from "./ContextMenuContainer.module.css";
import cn from "classnames";
import { ReactNode } from "react";

interface IProps {
  children: ReactNode;
  className?: any;
}

export function ContextMenuContainer({ children, className }: IProps) {
  return (
    <div className={cn(styles.contextMenuContainer, className)}>{children}</div>
  );
}
