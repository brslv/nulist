import { IconKey, icons } from "../../../lib/icons";
import styles from "./Icon.module.css";
import cn from "classnames";

export interface IProps {
  name: IconKey;
  className?: any;
  withBox?: boolean;
  pushBoxRight?: boolean;
  boxClassName?: any;
  dimmed?: boolean;
}

export function Icon({
  name,
  className,
  withBox = false,
  boxClassName,
  pushBoxRight,
  dimmed,
  ...rest
}: IProps) {
  const ResolvedIcon = icons[name];
  const IconComponent = <ResolvedIcon className={className} {...rest} />;

  if (withBox) {
    return (
      <div
        className={cn(
          styles.box,
          { [styles.pushRight]: pushBoxRight, [styles.dimmed]: dimmed },
          boxClassName
        )}
      >
        {IconComponent}
      </div>
    );
  }

  return IconComponent;
}
