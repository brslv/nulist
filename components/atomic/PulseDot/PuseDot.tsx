import styles from "./PulseDot.module.css";
import cn from "classnames";
import { WithTooltip } from "../../../lib/WithTooltip";
import { ReactNode, useState } from "react";

interface IProps {
  pulse?: boolean;
  isActive?: boolean;
  position?: PulseDotPosition;
  tooltip?: ReactNode;
  closeOnClick?: boolean;
}

export enum PulseDotPosition {
  TopLeft = "topLeft",
  TopRight = "topRight",
}

export function PulseDot({
  pulse = true,
  isActive,
  tooltip,
  position = PulseDotPosition.TopLeft,
  closeOnClick = false,
}: IProps) {
  const [isHidden, setIsHidden] = useState(false);

  const onClick = () => {
    if (closeOnClick) setIsHidden(true);
  };

  if (!isActive || isHidden) return null;

  return (
    <div
      onClick={onClick}
      className={cn(styles.pulseDot, {
        [styles.topLeft]: position === PulseDotPosition.TopLeft,
        [styles.topRight]: position === PulseDotPosition.TopRight,
      })}
    >
      <WithTooltip
        maxWidth={150}
        placement="right"
        disabled={!tooltip}
        content={tooltip}
      >
        <>
          <div className={styles.dot} />
          {pulse ? <div className={styles.pulse} /> : null}
        </>
      </WithTooltip>
    </div>
  );
}
