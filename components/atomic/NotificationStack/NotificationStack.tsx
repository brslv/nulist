import { useNotifications } from "../../../core/notifications/context/NotificationsProvider";
import { Notification } from "../Notification/Notification";
import { Portal } from "../Portal/Portal";
import styles from "./NotificationStack.module.css";

export function NotificationStack() {
  const { notifications } = useNotifications();

  return (
    <Portal>
      <div className={styles.notificationStack}>
        {notifications.map(notification => {
          return (
            <Notification notification={notification} key={notification.id} />
          );
        })}
      </div>
    </Portal>
  );
}
