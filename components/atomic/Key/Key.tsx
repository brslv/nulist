import { Icon } from "../Icon/Icon";
import styles from "./Key.module.css";

interface IProps {
  arrowUp?: boolean;
  arrowDown?: boolean;
  cmdOrCtrl?: boolean;
  forwardSlash?: boolean;
  enter?: boolean;
}

export function Key({
  arrowUp,
  arrowDown,
  cmdOrCtrl,
  forwardSlash,
  enter,
}: IProps) {
  if (arrowUp)
    return (
      <span className={styles.key}>
        <Icon name="up" />
      </span>
    );
  if (arrowDown)
    return (
      <span className={styles.key}>
        <Icon name="down" />
      </span>
    );

  if (cmdOrCtrl)
    return (
      <span className={styles.key}>
        <Icon name="cmd" />
        <Icon name="forwardSlash" />
        <span className={styles.text}>Ctrl</span>
      </span>
    );

  if (forwardSlash)
    return (
      <span className={styles.key}>
        <Icon name="forwardSlash" />
      </span>
    );

  if (enter)
    return (
      <span className={styles.key}>
        <Icon name="enter" />
        <span className={styles.text}>Enter</span>
      </span>
    );

  return null;
}
