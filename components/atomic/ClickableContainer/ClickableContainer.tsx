import { useRouter } from "next/router";
import { KeyboardEvent, ReactNode } from "react";
import styles from "./ClickableContainer.module.css";
import cn from "classnames";

interface IProps {
  children: ReactNode;
  className?: any;
  href?: string | { pathname: string; query?: any };
  onClick?: () => void;
  onEnter?: () => void;
  onSpace?: () => void;
  treatKeybardEventAsMouseEvent?: boolean;
}

export function ClickableContainer({
  children,
  className,
  href,
  onClick,
  onEnter,
  onSpace,
  treatKeybardEventAsMouseEvent = true,
}: IProps) {
  const router = useRouter();

  const onKeyUp = (e: KeyboardEvent<HTMLDivElement>) => {
    if (e.key === "Enter") {
      if (treatKeybardEventAsMouseEvent) internalOnClick();
      else if (typeof onEnter === "function") onEnter();
    }

    if (e.key === " ") {
      if (treatKeybardEventAsMouseEvent) internalOnClick();
      else if (typeof onSpace === "function") onSpace();
    }
  };

  const internalOnClick = () => {
    if (typeof onClick === "function") {
      onClick();
    }

    if (href) {
      router.push(href);
    }
  };

  return (
    <div
      tabIndex={0}
      className={cn(styles.clickableContainer, className)}
      onKeyUp={onKeyUp}
      onClick={internalOnClick}
    >
      {children}
    </div>
  );
}
