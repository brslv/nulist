import React, { useEffect, useRef, useState } from "react";
import { INotification } from "../../../core/notifications/types/INotification";
import { NotificationTypes } from "../../../core/notifications/types/NotificationTypes";
import styles from "./Notification.module.css";
import cn from "classnames";
import { Button } from "../Button/Button";
import { useNotifications } from "../../../core/notifications/context/NotificationsProvider";
import { CSSTransition } from "react-transition-group";

interface IProps {
  notification: INotification;
}
export function Notification({ notification }: IProps) {
  const ref = useRef(null);
  const { removeNotification: remove } = useNotifications();
  const [init, setInit] = useState(false);
  const [isRemoved, setIsRemoved] = useState(false);
  const typeStyles = {
    [styles.success]: notification.type === NotificationTypes.Success,
    [styles.error]: notification.type === NotificationTypes.Error,
  };

  const startRemoving = () => setIsRemoved(true);

  useEffect(() => {
    setInit(true);
  }, []);

  useEffect(() => {
    let to: any;
    if (notification.autoClose || notification.autoClose === undefined) {
      to = setTimeout(() => {
        startRemoving();
      }, notification.autoClose || 5000);
    }

    return () => clearTimeout(to);
  }, [notification.autoClose, notification.id, remove]);

  return (
    <CSSTransition
      nodeRef={ref}
      in={!isRemoved && init}
      classNames={{
        enter: styles.animEnter,
        enterActive: styles.animEnterActive,
        exit: styles.animExit,
        exitActive: styles.animExitActive,
      }}
      onExited={() => remove(notification.id)}
      timeout={200}
      unmountOnExit
    >
      <div ref={ref} className={cn(styles.notification, typeStyles)}>
        <div className={styles.content}>{notification.content}</div>
        {notification.hasCloseBtn ? (
          <Button
            variant="nostyle"
            icon="close"
            iconOnly
            narrow
            rootClassName={styles.removeBtnRoot}
            className={styles.removeBtn}
            onClick={startRemoving}
          />
        ) : null}
      </div>
    </CSSTransition>
  );
}
