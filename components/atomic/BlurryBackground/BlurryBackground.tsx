import Image from "next/image";
import styles from "./BlurryBackground.module.css";
import blur from "../../../public/images/bg-blur.svg";
import cn from "classnames";

export type BlurryBackgroundOpacity = 10 | 20 | 30 | 40 | 50 | 60;

interface IProps {
  style?: any;
  position?: "absolute" | "fixed";
  opacity?: BlurryBackgroundOpacity;
}

export function BlurryBackground({
  style,
  position = "fixed",
  opacity = 10,
}: IProps) {
  const opacityClass = styles[`opacity${opacity}`];
  const positionClass = styles[position];

  return (
    <div style={style} className={cn(styles.blurryBackground, positionClass)}>
      <Image
        src={blur}
        alt="Blurry background"
        layout="fixed"
        width="1440"
        height="900"
        className={cn(opacityClass)}
      />
    </div>
  );
}
