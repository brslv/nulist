import { ReactNode } from "react";
import { Button } from "../Button/Button";
import { Modal } from "../Modal/Modal";
import { ModalContent } from "../Modal/ModalContent";
import { ModalHeading } from "../Modal/ModalHeading";
import styles from "./Confirmation.module.css";

interface IProps {
  title?: ReactNode;
  children?: ReactNode;
  isOpen: boolean;
  onCancel: () => void;
  onOk: () => void;
}

export function Confirmation({
  children,
  isOpen,
  onCancel,
  onOk,
  title = "Confirmation required",
}: IProps) {
  return (
    <Modal focusTrap isOpen={isOpen} onClose={onCancel}>
      <ModalHeading onClose={onCancel}>{title}</ModalHeading>
      <ModalContent hasModalHeadingOffset>
        <div className={styles.content}>
          {children || "Are you sure you want to execute this action?"}
        </div>
        <div
          className={styles.buttons}
          onKeyUpCapture={e => {
            e.stopPropagation();
          }}
          onKeyDownCapture={e => {
            e.stopPropagation();
          }}
        >
          <Button
            variant="primary"
            rootClassName={styles.confirmBtn}
            onClick={e => {
              e.stopPropagation();
              onOk();
            }}
            autoFocus
          >
            Confirm
          </Button>
          <Button
            variant="plain"
            onClick={e => {
              e.stopPropagation();
              onCancel();
            }}
          >
            Cancel
          </Button>
        </div>
      </ModalContent>
    </Modal>
  );
}
