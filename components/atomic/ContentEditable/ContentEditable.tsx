import React, { RefObject, KeyboardEvent, FocusEvent } from "react";
import ReactContentEditable, { Props as RCEP } from "react-contenteditable";
import styles from "./ContentEditable.module.css";
import inputFieldStyles from "../../../styles/InputField.module.css";
import cn from "classnames";

function setEndOfContenteditable(contentEditableElement: HTMLElement) {
  let range, selection;
  if (document.createRange) {
    //Firefox, Chrome, Opera, Safari, IE 9+
    range = document.createRange(); //Create a range (a range is a like the selection but invisible)
    range.selectNodeContents(contentEditableElement); //Select the entire contents of the element with the range
    range.collapse(false); //collapse the range to the end point. false means collapse to end rather than the start
    selection = window.getSelection(); //get the selection object (allows you to change selection)
    if (selection) selection.removeAllRanges(); //remove any selections already made
    if (selection) selection.addRange(range); //make the range you have just created the visible selection
  } else if ((document as any).selection) {
    //IE 8 and lower
    range = (document.body as any).createTextRange(); //Create a range (a range is a like the selection but invisible)
    range.moveToElementText(contentEditableElement); //Select the entire contents of the element with the range
    range.collapse(false); //collapse the range to the end point. false means collapse to end rather than the start
    range.select(); //Select the range (make it the visible selection
  }
}

interface IProps {
  value: string;
  onBlur?: (e: FocusEvent<HTMLDivElement>, value: string) => void;
  autoFocus?: boolean;
  onChange?: (value: string) => void;
  className?: any;
  placeholderClassName?: any;
  tagName?: string;
  disabled?: boolean;
  clientRef?: RefObject<any>;
  placeholder?: string;
  onEnter?: (e: KeyboardEvent<HTMLDivElement>, value: string) => void;
  onFocus?: () => void;
  onClick?: () => void;
}

interface IState {
  showPlaceholder: boolean;
}

class ContentEditableCls extends React.Component<IProps, IState> {
  contentEditable: RefObject<any>;

  constructor(props: IProps) {
    super(props);

    this.contentEditable = props.clientRef || React.createRef();
    this.state = {
      showPlaceholder: !props.value,
    };
  }

  componentDidUpdate(prevProps: IProps) {
    if (
      prevProps.value &&
      prevProps.value.length &&
      this.props.value.length === 0
    ) {
      // the value has been resetted to ''

      if (
        this.props.autoFocus &&
        this.contentEditable &&
        this.contentEditable.current
      ) {
        // refocus on the content editable area
        this.contentEditable.current.focus();
      }
    }

    if (this.props.value.length && !prevProps.value.length)
      this.hidePlaceholder();

    if (!this.props.value.length && prevProps.value.length)
      this.showPlaceholder();
  }

  componentDidMount() {
    if (this.contentEditable && this.contentEditable.current) {
      this.contentEditable.current.addEventListener(
        "paste",
        (e: any) => {
          e.preventDefault();
          const text = e.clipboardData.getData("text/plain");
          this.hidePlaceholder();
          if (typeof this.props.onChange === "function")
            this.props.onChange(text);
        },
        false
      );
    }

    if (
      this.props.autoFocus &&
      this.contentEditable &&
      this.contentEditable.current
    ) {
      this.contentEditable.current.focus();
    }
  }

  handleChange: RCEP["onChange"] = e => {
    if (typeof this.props.onChange === "function")
      this.props.onChange(e.target.value);
  };

  handleBlur: RCEP["onBlur"] = e => {
    if (typeof this.props.onBlur === "function") {
      this.props.onBlur(e, this.props.value);
    }
  };

  handleKeyDown: RCEP["onKeyDown"] = e => {
    const isEnter = e.key === "Enter";
    if (isEnter && typeof this.props.onEnter === "function") {
      e.preventDefault();
      this.props.onEnter(e, this.props.value);
    }
  };

  onPlaceholderClick = () => {
    if (this.contentEditable && this.contentEditable.current)
      this.contentEditable.current.focus();
  };

  handleFocus: RCEP["onFocus"] = () => {
    if (this.contentEditable && this.contentEditable.current)
      setEndOfContenteditable(this.contentEditable.current);

    if (this.props.onFocus) this.props.onFocus();
  };

  showPlaceholder = () =>
    this.setState(prev => ({
      ...prev,
      showPlaceholder: true,
    }));
  hidePlaceholder = () =>
    this.setState(prev => ({ ...prev, showPlaceholder: false }));

  render = () => (
    <div style={{ position: "relative", width: "100%" }}>
      {this.props.placeholder && this.state.showPlaceholder ? (
        <button
          type="button"
          tabIndex={-1}
          className={cn(styles.placeholder, this.props.placeholderClassName)}
          onClick={this.onPlaceholderClick}
        >
          {this.props.placeholder}
        </button>
      ) : null}
      <ReactContentEditable
        className={cn(
          inputFieldStyles.inputField,
          styles.contentEditable,
          this.props.className
        )}
        innerRef={this.contentEditable}
        html={this.props.value}
        disabled={!!this.props.disabled}
        onChange={this.handleChange}
        onBlur={this.handleBlur}
        onFocus={this.handleFocus}
        onKeyDown={this.handleKeyDown}
        onClick={this.props.onClick}
        tagName={this.props.tagName || "article"}
      />
    </div>
  );
}

export const ContentEditable = React.forwardRef((props: IProps, ref: any) => {
  return <ContentEditableCls {...props} clientRef={ref} />;
});
