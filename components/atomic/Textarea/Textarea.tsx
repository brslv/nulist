import styles from "./Textarea.module.css";
import inputFieldStyles from "../../../styles/InputField.module.css";
import TextareaAutosize, {
  TextareaAutosizeProps,
} from "react-textarea-autosize";
import cn from "classnames";
import {
  KeyboardEvent,
  useState,
  FormEvent,
  forwardRef,
  ComponentPropsWithoutRef,
} from "react";
import Skeleton, { SkeletonProps } from "react-loading-skeleton";

interface IProps {
  value: string;
  name?: string;
  onChange: (e: FormEvent<HTMLTextAreaElement>) => void;
  className?: any;
  onKeyDown?: (e: KeyboardEvent<HTMLTextAreaElement>) => void;
  placeholder?: string;
  error?: string;
  fullWidth?: boolean;
  maxSymbols?: number | null;
  invisibleBorders?: boolean;
  autoFocus?: boolean;
  disableNewLine?: boolean;
  skeleton?: boolean;
  skeletonProps?: SkeletonProps;
  rootStyles?: any;
}

export const Textarea = forwardRef(
  (
    {
      value,
      className,
      name,
      onChange,
      onKeyDown,
      placeholder,
      error,
      fullWidth = false,
      maxSymbols = null,
      invisibleBorders = false,
      autoFocus = false,
      disableNewLine = false,
      skeleton = false,
      skeletonProps = {},
      rootStyles = {},
      ...rest
    }: IProps & TextareaAutosizeProps & ComponentPropsWithoutRef<"textarea">,
    ref: any
  ) => {
    const [showMaxSymbols, setShowMaxSymbols] = useState(false);

    const internalOnChange = (e: FormEvent<HTMLTextAreaElement>) => {
      const target = e.currentTarget;
      if (maxSymbols !== null && !isNaN(maxSymbols)) {
        if (target.value.length > maxSymbols) return;
      }

      onChange(e);
    };

    const internalOnKeyDown = (e: KeyboardEvent<HTMLTextAreaElement>) => {
      if (disableNewLine) {
        if (e.key === "Enter") {
          e.preventDefault();
        }
      }

      if (typeof onKeyDown === "function") onKeyDown(e);
    };

    return (
      <div style={{ position: "relative", width: "100%", ...rootStyles }}>
        {skeleton ? <Skeleton {...skeletonProps} /> : null}
        {!skeleton ? (
          <>
            <TextareaAutosize
              ref={ref}
              autoFocus={autoFocus}
              value={value}
              name={name}
              onChange={internalOnChange}
              placeholder={placeholder}
              onKeyDown={internalOnKeyDown}
              className={cn(
                inputFieldStyles.inputField,
                {
                  [styles.withMaxSymbolsCounter]: maxSymbols,
                  [inputFieldStyles.invisibleBorders]: invisibleBorders,
                  [styles.fullWidth]: fullWidth,
                },
                className
              )}
              {...rest}
              onFocus={e => {
                setShowMaxSymbols(true);
                rest.onFocus && rest.onFocus(e);
              }}
              onBlur={e => {
                setShowMaxSymbols(false);
                rest.onBlur && rest.onBlur(e);
              }}
            />
            {error ? (
              <span
                className={cn(styles.error, {
                  [styles.alignToInputText]: invisibleBorders,
                })}
              >
                {error}
              </span>
            ) : null}
            {maxSymbols ? (
              <div
                className={cn(styles.maxSymbols, {
                  [styles.alignToInputText]: invisibleBorders,
                })}
              >
                {showMaxSymbols ? (
                  <>
                    {value.length}/{maxSymbols}
                  </>
                ) : null}
              </div>
            ) : null}
          </>
        ) : null}
      </div>
    );
  }
);
