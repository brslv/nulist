import { useRef, useState } from "react";
import { useBundleForm } from "../../core/bundles/context/BundleFormProvider";
import { useBundles } from "../../core/bundles/context/BundlesProvider";
import { IBundle } from "../../core/bundles/types/IBundle";
import { Button } from "../atomic/Button/Button";
import { Confirmation } from "../atomic/Confirmation/Confirmation";
import { ContextMenu } from "../atomic/ContextMenu/ContextMenu";
import { ContextMenuContainer } from "../atomic/ContextMenu/ContextMenuContainer";
import { ContextMenuItem } from "../atomic/ContextMenu/ContextMenuItem";
import { ContextMenuSeparator } from "../atomic/ContextMenu/ContextMenuSeparator";
import { ContextMenuTitle } from "../atomic/ContextMenu/ContextMenuTitle";
import styles from "./BundleControls.module.css";

interface IProps {
  bundle: Partial<IBundle>;
}

export function BundleControls({ bundle }: IProps) {
  const btnRef = useRef(null);
  const [isOpen, setIsOpen] = useState(false);
  const { removeBundle } = useBundles();
  const [isDeleteConfirmationOpen, setIsDeleteConfirmationOpen] =
    useState(false);
  const { open: openBundleForm } = useBundleForm();

  const close = () => setIsOpen(false);
  const open = () => setIsOpen(true);
  const toggle = () => (isOpen ? close() : open());

  return (
    <div style={{ position: "relative" }}>
      <Button
        ref={btnRef}
        iconOnly
        icon="menu"
        narrow
        variant="plain"
        onClick={e => {
          e.stopPropagation();
          toggle();
        }}
      />
      <ContextMenu
        style={{ right: 0 }}
        isOpen={isOpen}
        onOutsideClick={close}
        anchorRef={btnRef}
        focusTrap={false}
        addTopOffset={10}
      >
        <ContextMenuContainer className={styles.container}>
          <ContextMenuTitle>Actions</ContextMenuTitle>
          <ContextMenuItem
            icon="edit"
            fullWidth
            onClick={() => {
              openBundleForm({ id: bundle.id || 0, ...bundle });
              setIsOpen(false);
            }}
          >
            Edit
          </ContextMenuItem>
          <ContextMenuSeparator />
          <ContextMenuItem
            icon="trash"
            fullWidth
            onClick={() => {
              setIsDeleteConfirmationOpen(true);
              setIsOpen(false);
            }}
          >
            Delete
          </ContextMenuItem>
        </ContextMenuContainer>
      </ContextMenu>
      <Confirmation
        isOpen={isDeleteConfirmationOpen}
        onCancel={() => {
          setIsDeleteConfirmationOpen(false);
        }}
        onOk={() => {
          removeBundle(bundle.id);
          setIsDeleteConfirmationOpen(false);
        }}
      >
        <span style={{ display: "block", maxWidth: "42ch" }}>
          <p style={{ marginTop: 0 }}>Removing a bundle is permanent.</p>
          <p>
            All other lists associated with this bundle will get removed from it
            as well.
          </p>
          <p style={{ marginBottom: 0 }}>Are you sure you want to continue?</p>
        </span>
      </Confirmation>
    </div>
  );
}
