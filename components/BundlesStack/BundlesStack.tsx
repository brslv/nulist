import { useBundleForm } from "../../core/bundles/context/BundleFormProvider";
import { useBundles } from "../../core/bundles/context/BundlesProvider";
import { IBundle } from "../../core/bundles/types/IBundle";
import { ListVisibilities } from "../../core/list/types/ListVisibilities";
import { Button } from "../atomic/Button/Button";
import { BundleCard } from "./BundleCard";
import styles from "./BundlesStack.module.css";

interface IProps {
  visibility: ListVisibilities | null;
}

export function BundlesStack({ visibility }: IProps) {
  const { bundles } = useBundles();
  const filteredBundles = bundles.filter(
    filterBundlesBasedOnVisibility(visibility)
  );

  return (
    <>
      <div className={styles.bundlesStack}>
        <CreateCard />

        {filteredBundles.map(bundle => (
          <BundleCard key={bundle.id} bundle={bundle} />
        ))}
      </div>
      {/* {filteredBundles.length === 0 && initiallyLoaded ? (
        <Panel className={styles.noBundlesMessage}>
          <Title component="div" size={30} weight="medium">
            No{" "}
            <span className={styles.visibility} data-visibility={visibility}>
              {visibility}
            </span>{" "}
            bundles found
          </Title>
          <p>
            Currently, you can create/edit bundles only from the list editor
            itself.
          </p>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Link href="/" passHref>
              <Button component="a" variant="accent">
                Go to the list editor
              </Button>
            </Link>
          </div>
        </Panel>
      ) : null} */}
    </>
  );
}

function filterBundlesBasedOnVisibility(visibility: ListVisibilities | null) {
  return function (bundle: IBundle) {
    if (visibility === null || visibility === ListVisibilities.All)
      return bundle;
    if (visibility === ListVisibilities.Private) return !bundle.isPublic;
    if (visibility === ListVisibilities.Public) return bundle.isPublic;
  };
}

function CreateCard() {
  const { open } = useBundleForm();

  return (
    <div className={styles.createCard}>
      <Button onClick={() => open()} variant="accent" icon="add">
        Create
      </Button>
    </div>
  );
}
