import { IBundle } from "../../core/bundles/types/IBundle";
import { PulseDot, PulseDotPosition } from "../atomic/PulseDot/PuseDot";
import { Title } from "../atomic/Title/Title";
import styles from "./BundleCard.module.css";
import { BundleControls } from "./BundleControls";
import { ClickableContainer } from "../atomic/ClickableContainer/ClickableContainer";

interface IProps {
  bundle: Partial<IBundle>;
}

export function BundleCard({ bundle }: IProps) {
  const href = `/bundle/${bundle.id}`;

  return (
    <ClickableContainer className={styles.bundle} href={href}>
      <div className={styles.backgroundStack} />
      <div
        style={{
          position: "absolute",
          marginTop: 10,
          marginLeft: 20,
          zIndex: 10,
        }}
      >
        <PulseDot
          position={PulseDotPosition.TopRight}
          isActive={bundle.isPublic}
          pulse={false}
        />
      </div>
      <div className={styles.container}>
        <div className={styles.heading}>
          <div className={styles.title}>
            <Title size={30} weight="medium">
              {bundle.title}
            </Title>
          </div>
          <div>
            <span
              onKeyUp={e => e.stopPropagation()}
              onClick={e => e.stopPropagation()}
            >
              <BundleControls bundle={bundle} />
            </span>
          </div>
        </div>
      </div>
    </ClickableContainer>
  );
}
