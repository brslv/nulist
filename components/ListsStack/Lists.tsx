import React, { useEffect, useState } from "react";
import { useAuth } from "../../core/auth/context/AuthProvider";
import { useUserLists } from "../../core/list/context/UserListsProvider";
import { ListVisibilities } from "../../core/list/types/ListVisibilities";
import { ListsStack } from "./ListsStack";

interface IProps {
  visibility: ListVisibilities | null;
}

export function Lists({ visibility }: IProps) {
  const { user } = useAuth();
  const { lists, loading, load } = useUserLists();
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    setIsMounted(true);
  }, []);

  useEffect(() => {
    if (!user) return;

    if (visibility) load(user, { visibility });
  }, [load, visibility, user]);

  if (!isMounted) return null;

  if (loading || lists === null) return null;

  return (
    <ListsStack
      lists={lists}
      showUnsavedLists={visibility === ListVisibilities.All}
    />
  );
}
