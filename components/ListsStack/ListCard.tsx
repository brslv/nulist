import { IList } from "../../core/list/types/IList";
import React from "react";
import { ListCardSkeleton } from "./ListCardSkeleton";
import { ListEditControls } from "./ListEditControls";
import { BundlesList } from "./BundlesList";
import { useShareableLinkModal } from "../../core/list/hooks/useShareableLinkModal";
import { CopyShareableLinkModal } from "../CopyShareableLinkModal/CopyShareableLinkModal";

interface IProps {
  list?: IList;
  loading?: boolean;
}

export function ListCard({ list, loading = false }: IProps) {
  const { isOpen, isCopied, listUrl, close, open, copy } =
    useShareableLinkModal({
      listUid: list?.uid,
    });

  return (
    <>
      <ListCardSkeleton
        href={`/list/${list?.uid}/edit`}
        title={list?.title}
        emoji={list?.emoji}
        emojiName={list?.emojiName}
        controls={
          list ? (
            <ListEditControls onLinkClick={() => open()} list={list} />
          ) : null
        }
        description={list?.description}
        loading={loading || !list}
        hasPulse={list?.isPublic}
        skeletonHeight={145}
        bundles={
          list?.bundles.length ? <BundlesList items={list.bundles} /> : null
        }
      />

      <CopyShareableLinkModal
        isOpen={isOpen}
        onClose={close}
        value={listUrl}
        onCopy={copy}
        isCopied={isCopied}
      />
    </>
  );
}
