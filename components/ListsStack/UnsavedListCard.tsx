import { IListEditorState } from "../../core/list/types/IListEditorState";
import { PulseDot, PulseDotPosition } from "../atomic/PulseDot/PuseDot";
import { ListCardSkeleton } from "./ListCardSkeleton";
import { UnsavedListEditControls } from "./UnsavedListEditControls";
import styles from "./UnsavedListCard.module.css";
import React from "react";
import { Title } from "../atomic/Title/Title";

interface IProps {
  unsavedListEditorState: IListEditorState;
  onDelete: () => void;
  loading?: boolean;
}

export function UnsavedListCard({
  unsavedListEditorState,
  onDelete,
  loading = false,
}: IProps) {
  return (
    <div className={styles.unsavedListCard}>
      <div style={{ display: "flex", position: "relative" }}>
        <Title size={10} uppercase weight="medium" className={styles.title}>
          Requires action
        </Title>
        <div style={{ position: "relative", marginLeft: 20 }}>
          <PulseDot
            isActive={true}
            position={PulseDotPosition.TopRight}
            tooltip={
              <>
                <p style={{ marginTop: 0 }}>
                  You created this list before signing in on Nulist.
                </p>
                <p style={{ marginBottom: 0 }}>
                  It&apos;s not saved yet, though. Go ahead and edit it and hit
                  &quot;Save&quot;.
                </p>
              </>
            }
          />
        </div>
      </div>
      <ListCardSkeleton
        href={{ pathname: `/`, query: { "load-unsaved": true } }}
        title={unsavedListEditorState.title}
        description={unsavedListEditorState.description}
        emoji={unsavedListEditorState.emoji.emoji}
        emojiName={unsavedListEditorState.emoji.name}
        controls={<UnsavedListEditControls onDelete={onDelete} />}
        loading={loading}
        bottomSpace={false}
        className={styles.list}
      />
    </div>
  );
}
