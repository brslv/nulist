import cn from "classnames";
import React, { ReactNode } from "react";
import Skeleton from "react-loading-skeleton";
import { ClickableContainer } from "../atomic/ClickableContainer/ClickableContainer";
import { Icon } from "../atomic/Icon/Icon";
import { PulseDot, PulseDotPosition } from "../atomic/PulseDot/PuseDot";
import { Title } from "../atomic/Title/Title";
import styles from "./ListCardSkeleton.module.css";

interface IProps {
  href: string | { pathname: string; query?: any };
  emojiName?: string;
  emoji?: string;
  title?: string;
  controls?: ReactNode;
  hasPulse?: boolean;
  description?: string;
  loading?: boolean;
  bottomSpace?: boolean;
  className?: any;
  skeletonHeight?: number;
  bundles?: ReactNode;
}

export function ListCardSkeleton({
  href,
  emojiName,
  emoji,
  title,
  controls,
  hasPulse = false,
  description,
  loading = false,
  bottomSpace = true,
  className,
  skeletonHeight = 150,
  bundles,
}: IProps) {
  if (loading)
    return (
      <Skeleton
        style={{ borderRadius: 15 }}
        count={1}
        height={skeletonHeight}
        className={styles.skeleton}
      />
    );

  return (
    <ClickableContainer
      className={cn(styles.listCardSkeletonContainer, {
        [styles.bottomSpace]: bottomSpace,
      })}
      href={href}
    >
      <div style={{ height: "100%" }} className={cn(className)}>
        <div
          style={{
            height: "100%",
            width: "100%",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <div className={styles.heading}>
            {hasPulse ? (
              <div style={{ position: "absolute", marginTop: -10 }}>
                <PulseDot
                  position={PulseDotPosition.TopRight}
                  isActive={true}
                  pulse={false}
                />
              </div>
            ) : null}
            <div className={styles.title}>
              <Title component="div" size={30} weight="medium">
                {title}
              </Title>
            </div>
            <div className={styles.controls}>
              <span title={emojiName} className={styles.emoji}>
                {emoji}
              </span>
              <span
                onKeyUp={e => e.stopPropagation()}
                onClick={e => e.stopPropagation()}
              >
                {controls}
              </span>
            </div>
          </div>
          <div className={styles.description}>
            {description || <Icon name="minus" />}
            <div className={styles.descriptionFade} />
          </div>
          <div style={{ height: "100%" }} className={styles.bundles}>
            {bundles}
          </div>
        </div>
      </div>
    </ClickableContainer>
  );
}
