import React, { useState } from "react";
import { Confirmation } from "../atomic/Confirmation/Confirmation";
import { ContextMenuContainer } from "../atomic/ContextMenu/ContextMenuContainer";
import { ContextMenuItem } from "../atomic/ContextMenu/ContextMenuItem";
import { ContextMenuTitle } from "../atomic/ContextMenu/ContextMenuTitle";
import { ListControlsSkeleton } from "./ListControlsSkeleton";

interface IProps {
  onDelete: () => void;
}

export function UnsavedListEditControls({ onDelete }: IProps) {
  const [isRemoveConfirmationOpen, setIsRemoveConfirmationOpen] =
    useState(false);

  return (
    <>
      <ListControlsSkeleton
        controls={() => (
          <>
            <ContextMenuContainer>
              <ContextMenuTitle>Actions</ContextMenuTitle>
              <ContextMenuItem
                onClick={() => setIsRemoveConfirmationOpen(true)}
                icon="trash"
                fullWidth
              >
                Delete
              </ContextMenuItem>
            </ContextMenuContainer>
          </>
        )}
      />

      <Confirmation
        isOpen={isRemoveConfirmationOpen}
        onCancel={() => setIsRemoveConfirmationOpen(false)}
        onOk={() => {
          setIsRemoveConfirmationOpen(false);
          onDelete();
        }}
      />
    </>
  );
}
