import router from "next/router";
import React, { MouseEvent, useState } from "react";
import { useUserLists } from "../../core/list/context/UserListsProvider";
import { IList } from "../../core/list/types/IList";
import { Confirmation } from "../atomic/Confirmation/Confirmation";
import { ContextMenuContainer } from "../atomic/ContextMenu/ContextMenuContainer";
import { ContextMenuItem } from "../atomic/ContextMenu/ContextMenuItem";
import { ContextMenuSeparator } from "../atomic/ContextMenu/ContextMenuSeparator";
import { ContextMenuTitle } from "../atomic/ContextMenu/ContextMenuTitle";
import { ListControlsSkeleton } from "./ListControlsSkeleton";

interface IProps {
  list: IList;
  onDelete?: () => void;
  onLinkClick?: () => void;
  showView?: boolean;
  showDelete?: boolean;
}

export function ListEditControls({
  list,
  onDelete,
  onLinkClick,
  showView = true,
  showDelete = true,
}: IProps) {
  const { remove } = useUserLists();
  const [isRemoveConfirmationOpen, setIsRemoveConfirmationOpen] =
    useState(false);

  const onBtnClick =
    (type: "link" | "view", closeControls?: () => void) =>
    (e?: MouseEvent<HTMLButtonElement>) => {
      if (e) {
        e.stopPropagation();
        e.preventDefault();
      }

      if (type === "view") {
        router.push(`/list/${list.uid}`);
      }

      if (type === "link" && typeof onLinkClick === "function") {
        onLinkClick();
      }

      if (closeControls) closeControls();
    };

  const internalOnDelete = () => {
    remove(list.id);
    if (typeof onDelete === "function") onDelete();
  };

  return (
    <>
      <ListControlsSkeleton
        controls={({ closeControls }) => (
          <>
            <ContextMenuContainer>
              <ContextMenuTitle>Actions</ContextMenuTitle>
              {showView ? (
                <ContextMenuItem
                  onClick={onBtnClick("view", closeControls)}
                  icon="view"
                  fullWidth
                >
                  View
                </ContextMenuItem>
              ) : null}
              <ContextMenuItem
                onClick={onBtnClick("link", closeControls)}
                icon="link"
                fullWidth
              >
                Shareable link
              </ContextMenuItem>
              <ContextMenuSeparator />
              {showDelete ? (
                <ContextMenuItem
                  onClick={() => {
                    setIsRemoveConfirmationOpen(true);
                    closeControls();
                  }}
                  icon="trash"
                  fullWidth
                >
                  Delete
                </ContextMenuItem>
              ) : null}
            </ContextMenuContainer>
          </>
        )}
      />

      <Confirmation
        isOpen={isRemoveConfirmationOpen}
        onCancel={() => setIsRemoveConfirmationOpen(false)}
        onOk={() => {
          setIsRemoveConfirmationOpen(false);
          internalOnDelete();
        }}
      />
    </>
  );
}
