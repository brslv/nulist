import Link from "next/link";
import { IList } from "../../core/list/types/IList";
import styles from "./BundlesList.module.css";

interface IProps {
  items: IList["bundles"];
}

export function BundlesList({ items }: IProps) {
  return (
    <>
      {items.map(item => (
        <span
          key={item.id}
          onClick={e => e.stopPropagation()}
          onKeyUp={e => e.stopPropagation()}
        >
          <Link href={`/bundle/${item.id}`}>
            <a tabIndex={0} className={styles.bundle}>
              {item.title}
            </a>
          </Link>
        </span>
      ))}
    </>
  );
}
