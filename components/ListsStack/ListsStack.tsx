import Link from "next/link";
import React from "react";
import { ClientEvents } from "../../core/clientEvents/ClientEvents";
import { fireClientEvent } from "../../core/clientEvents/fireClientEvent";
import { useUnsavedListEditorState } from "../../core/list/hooks/useUnsavedListEditorState";
import { IList } from "../../core/list/types/IList";
import { Button } from "../atomic/Button/Button";
import { ListCard } from "./ListCard";
import styles from "./ListsStack.module.css";
import { UnsavedListCard } from "./UnsavedListCard";

interface IProps {
  lists: IList[];
  showUnsavedLists?: boolean;
  showCreateBtn?: boolean;
}

export function ListsStack({
  lists,
  showUnsavedLists = true,
  showCreateBtn = true,
}: IProps) {
  const { unsavedListEditorState, clearUnsavedListEditorState } =
    useUnsavedListEditorState();

  const onCreateNewListClick = () => {
    fireClientEvent(ClientEvents.CreateListBtnClick, {
      location: "Dashboard - empty lists stack",
    });
  };

  return (
    <div className={styles.listsStack}>
      {showCreateBtn ? <CreateCard onClick={onCreateNewListClick} /> : null}

      {unsavedListEditorState && showUnsavedLists ? (
        <UnsavedListCard
          onDelete={clearUnsavedListEditorState}
          unsavedListEditorState={unsavedListEditorState}
        />
      ) : null}

      {lists.map(list => {
        return <ListCard list={list} key={list.id} />;
      })}
    </div>
  );
}

function CreateCard({ onClick }: { onClick: () => void }) {
  return (
    <div className={styles.createCard}>
      <Link href="/" passHref>
        <Button component="a" variant="accent" icon="add" onClick={onClick}>
          Create
        </Button>
      </Link>
    </div>
  );
}
