import React, { ReactNode, useRef, useState } from "react";
import { Button } from "../atomic/Button/Button";
import { ContextMenu } from "../atomic/ContextMenu/ContextMenu";

interface IProps {
  controls?: ({ closeControls }: { closeControls: () => void }) => ReactNode;
}

export function ListControlsSkeleton({ controls }: IProps) {
  const controlsBtnRef = useRef(null);
  const [isOpen, setIsOpen] = useState(false);

  const closeControls = () => setIsOpen(false);
  const toggleControls = () => {
    setIsOpen(prev => !prev);
  };

  return (
    <div style={{ position: "relative" }}>
      <Button
        ref={controlsBtnRef}
        iconOnly
        icon="menu"
        narrow
        variant="plain"
        onClick={e => {
          e.stopPropagation();
          toggleControls();
        }}
      />
      {controls ? (
        <ContextMenu
          focusTrap={false}
          onOutsideClick={closeControls}
          isOpen={isOpen}
          anchorRef={controlsBtnRef}
          addTopOffset={10}
          style={{ right: 0 }}
        >
          {controls({ closeControls })}
        </ContextMenu>
      ) : null}
    </div>
  );
}
