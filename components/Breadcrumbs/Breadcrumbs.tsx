import Link from "next/link";
import { useBreadcrumbs } from "../../core/breadcrumbs/context/BreadcrumbsProvider";
import styles from "./Breadcrumbs.module.css";

export function Breadcrumbs() {
  const { breadcrumbs } = useBreadcrumbs();

  if (!breadcrumbs) {
    return null;
  }

  return (
    <div className={styles.breadcrumbs}>
      <ol>
        {breadcrumbs.map(breadcrumb => {
          return (
            <li key={breadcrumb.href}>
              {breadcrumb.clickable !== false ? (
                <Link href={breadcrumb.href}>{breadcrumb.title}</Link>
              ) : (
                breadcrumb.title
              )}
            </li>
          );
        })}
      </ol>
    </div>
  );
}
