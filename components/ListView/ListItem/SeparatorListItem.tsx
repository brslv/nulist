import { HighlightedRow } from "../../HighlightedRow/HighlightedRow";
import { IndexedRow } from "../../ListEditor/List/IndexedRow";
import styles from "./SeparatorListItem.module.css";

export function SeparatorListItem() {
  return (
    <HighlightedRow>
      <IndexedRow>
        <hr className={styles.separator} />
      </IndexedRow>
    </HighlightedRow>
  );
}
