import React, { useState } from "react";
import { IListItem } from "../../../core/list/types/IListItem";
import { isRichContentEmpty } from "../../../lib/isRichContentEmpty";
import { HighlightedRow } from "../../HighlightedRow/HighlightedRow";
import { IndexedRow } from "../../ListEditor/List/IndexedRow";
import { ListItemPropertyIcons } from "../../ListItemPropertyIcons/ListItemPropertyIcons";
import { ListItemPropertiesModal } from "../ListItemPropertiesModal/ListItemPropertiesModal";
import styles from "./ListItem.module.css";

interface IProps {
  item: IListItem;
  index?: number;
}

export function ListItem({ item, index }: IProps) {
  const [isPropertiesModalOpen, setIsPropertiesModalOpen] = useState(false);

  const onClick = () => {
    setIsPropertiesModalOpen(true);
  };

  return (
    <>
      <HighlightedRow
        onClick={
          item.content && !isRichContentEmpty(item.content)
            ? onClick
            : undefined
        }
      >
        <IndexedRow index={index}>
          <div className={styles.container}>
            <div className={styles.title}>{item.title}</div>
            <ListItemPropertyIcons item={item} />
          </div>
        </IndexedRow>
      </HighlightedRow>
      <ListItemPropertiesModal
        item={item}
        isOpen={isPropertiesModalOpen}
        onClose={() => setIsPropertiesModalOpen(false)}
      />
    </>
  );
}
