import Link from "next/link";
import React from "react";
import { IListItem } from "../../../core/list/types/IListItem";
import { Button } from "../../atomic/Button/Button";
import { Modal, ModalPosition } from "../../atomic/Modal/Modal";
import { ModalContent } from "../../atomic/Modal/ModalContent";
import { ModalHeading } from "../../atomic/Modal/ModalHeading";
import { RichEditorView } from "../../atomic/RichEditor/RichEditorView";
import styles from "./ListItemPropertiesModal.module.css";

interface IProps {
  item: IListItem;
  isOpen: boolean;
  onClose: () => void;
}

export function ListItemPropertiesModal({ item, isOpen, onClose }: IProps) {
  return (
    <Modal
      containerStyles={styles.modalContainer}
      position={ModalPosition.TopCenter}
      isOpen={isOpen}
      onClose={onClose}
    >
      <ModalHeading onClose={onClose}>
        <div className={styles.heading}>
          <div className={styles.propertiesModalTitle}>{item.title}</div>
          {item.url ? (
            <Link href={item.url} passHref>
              <Button
                component="a"
                target="_blank"
                variant="basic"
                icon="link"
                narrow
              >
                Visit link
              </Button>
            </Link>
          ) : null}
        </div>
      </ModalHeading>
      <ModalContent hasModalHeadingOffset noSpace>
        <div className={styles.content}>
          <RichEditorView>{item.content}</RichEditorView>
        </div>
      </ModalContent>
    </Modal>
  );
}
