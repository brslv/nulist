import React, { useMemo } from "react";
import { IListItem } from "../../../core/list/types/IListItem";
import { getIndexesMap } from "../../../core/list/utils/getIndexesMap";
import { ListItemTypes } from "../../ListEditor/ListItemTypeContextMenu/types/IListItemType";
import { ListItem } from "../ListItem/ListItem";
import { SeparatorListItem } from "../ListItem/SeparatorListItem";
import styles from "./List.module.css";

interface IProps {
  items: IListItem[];
}

export function List({ items }: IProps) {
  const indexesMap = useMemo(() => getIndexesMap(items), [items]);

  return (
    <ul className={styles.ul}>
      {items.map(item => (
        <div key={item.id}>
          {item.type === ListItemTypes.Separator ? <SeparatorListItem /> : null}
          {item.type === ListItemTypes.Li ? (
            <ListItem item={item} index={indexesMap[item.id]} />
          ) : null}
        </div>
      ))}
    </ul>
  );
}
