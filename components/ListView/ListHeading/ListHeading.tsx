import React from "react";
import { useRouter } from "next/router";
import { IList } from "../../../core/list/types/IList";
import { WithTooltip } from "../../../lib/WithTooltip";
import { Title } from "../../atomic/Title/Title";
import { EmojiPicker } from "../../ListEditor/ListHeading/EmojiPicker";
import { ListEditControls } from "../../ListsStack/ListEditControls";
import styles from "./ListHeading.module.css";
import { Button } from "../../atomic/Button/Button";
import { useAuth } from "../../../core/auth/context/AuthProvider";
import { useShareableLinkModal } from "../../../core/list/hooks/useShareableLinkModal";
import { CopyShareableLinkModal } from "../../CopyShareableLinkModal/CopyShareableLinkModal";

interface IProps {
  list: IList;
}

export function ListHeading({ list }: IProps) {
  const router = useRouter();
  const { user } = useAuth();
  const { open, isOpen, listUrl, isCopied, copy, close } =
    useShareableLinkModal({ listUid: list.uid });
  const { isPublic, emoji, emojiName, title, description } = list;

  const onDelete = () => router.push("/");

  return (
    <div className={styles.listHeading}>
      <div className={styles.emoji}>
        <EmojiPicker value={{ emoji, name: emojiName || "emoji" }} />
      </div>
      <div className={styles.titleContainer}>
        <Title weight="bold" className={styles.title}>
          {title}
        </Title>
        <div className={styles.controls}>
          {!isPublic ? (
            <div className={styles.lock}>
              <WithTooltip
                disabled={isPublic}
                trigger="mouseenter click"
                content={
                  <span>
                    This list is private.
                    <br /> Only you can see it.
                  </span>
                }
              >
                <Button icon="lock" disabled narrow iconOnly variant="accent" />
              </WithTooltip>
            </div>
          ) : null}
          <ListEditControls
            list={list}
            onDelete={onDelete}
            showView={false}
            showDelete={!!user}
            onLinkClick={open}
          />
        </div>
      </div>
      <div className={styles.descriptionContainer}>
        <Title weight="medium" className={styles.description}>
          {description}
        </Title>
      </div>

      <CopyShareableLinkModal
        isOpen={isOpen}
        onClose={close}
        value={listUrl}
        onCopy={copy}
        isCopied={isCopied}
      />
    </div>
  );
}
