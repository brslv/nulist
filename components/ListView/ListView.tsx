import React from "react";
import { IList } from "../../core/list/types/IList";
import { List } from "./List/List";
import { ListHeading } from "./ListHeading/ListHeading";
import styles from "./ListView.module.css";
import { MadeWithNulistTag } from "./MadeWithNulistTag/MadeWithNulistTag";

interface IProps {
  list: IList;
}

export function ListView({ list }: IProps) {
  return (
    <div className={styles.listView}>
      <div className={styles.headingContainer}>
        <ListHeading list={list} />
      </div>
      {list.listItems ? <List items={list.listItems} /> : null}
      {!list.author?.isPro ? <MadeWithNulistTag /> : null}
    </div>
  );
}
