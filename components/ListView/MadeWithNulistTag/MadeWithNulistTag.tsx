import Link from "next/link";
import React from "react";
import { ROOT_URL } from "../../../core/config";
import { Button } from "../../atomic/Button/Button";
import styles from "./MadeWithNulistTag.module.css";

export function MadeWithNulistTag() {
  return (
    <div className={styles.madeWithNulistTag}>
      <Link href={ROOT_URL} passHref>
        <Button component="a" variant="basic" rootClassName={styles.btnRoot}>
          Made with nulist
        </Button>
      </Link>
    </div>
  );
}
