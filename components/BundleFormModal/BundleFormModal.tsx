import { useBundleForm } from "../../core/bundles/context/BundleFormProvider";
import { Modal } from "../atomic/Modal/Modal";
import { ModalContent } from "../atomic/Modal/ModalContent";
import { ModalHeading } from "../atomic/Modal/ModalHeading";
import { BundleForm } from "../BundleForm/BundleForm";

export function BundleFormModal() {
  const { isOpen, close } = useBundleForm();

  return (
    <Modal isOpen={isOpen} onClose={close}>
      <ModalHeading onClose={close}>Create bundle</ModalHeading>
      <ModalContent>
        <BundleForm />
      </ModalContent>
    </Modal>
  );
}
