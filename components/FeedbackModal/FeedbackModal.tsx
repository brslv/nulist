import React, { useEffect, useState } from "react";
import {
  OnboardingModals,
  useOnboardingModals,
} from "../../core/onboarding/modals/OnboardingModalsProvider";
import { Modal } from "../atomic/Modal/Modal";
import { ModalContent } from "../atomic/Modal/ModalContent";
import { ModalHeading } from "../atomic/Modal/ModalHeading";
import styles from "./FeedbackModal.module.css";

export function FeedbackModal() {
  const { isOpen, close } = useOnboardingModals();
  const [isLoading, setIsLoading] = useState(true);

  const internalOnClose = () => {
    setIsLoading(false);
    close();
  };

  useEffect(() => {
    if (isOpen(OnboardingModals.Feedback)) setIsLoading(true);
  }, [isOpen]);

  const stopLoading = () => {
    setIsLoading(false);
  };

  return (
    <Modal
      onClose={internalOnClose}
      isOpen={isOpen(OnboardingModals.Feedback)}
      containerStyles={styles.feedbackModal}
    >
      <ModalHeading onClose={internalOnClose}>Feedback</ModalHeading>
      <ModalContent hasModalHeadingOffset>
        {isLoading ? <div className={styles.loader}>Loading...</div> : null}
        <iframe
          src="https://tally.so/embed/wv44Dm?hideTitle=1&alignLeft=1"
          width="100%"
          frameBorder={0}
          marginHeight={0}
          height={570}
          marginWidth={0}
          onLoad={stopLoading}
          title="Nulist feedback"
          style={{
            display: isOpen(OnboardingModals.Feedback) ? "initial" : "none",
          }}
        />
      </ModalContent>
    </Modal>
  );
}
