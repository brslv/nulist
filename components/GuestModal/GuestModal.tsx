import React, { useEffect, useState } from "react";
import Image from "next/image";
import { Modal, ModalPosition } from "../atomic/Modal/Modal";
import { ModalCloseIcon } from "../atomic/Modal/ModalCloseIcon";
import { ModalContent } from "../atomic/Modal/ModalContent";
import { Title } from "../atomic/Title/Title";
import styles from "./GuestModal.module.css";
import listsShapes from "../../public/images/lists-shapes.svg";
import { Button } from "../atomic/Button/Button";
import { CONSTRAINTS } from "../../core/plans/constraints";
import { usePageControls } from "../PageControls/PageControlsProvider";
import { usePlansModal } from "../../core/plans/context/PlansModalProvider";
import { useMediaQuery } from "@react-hook/media-query";

interface IProps {
  isOpen: boolean;
}

export function GuestModal({ isOpen }: IProps) {
  const [internalIsOpen, setInternalIsOpen] = useState(isOpen);
  const { onSigninClick } = usePageControls();
  const { open: openPlansModal } = usePlansModal();
  const isBellow648px = useMediaQuery("only screen and (max-width: 648px)");

  useEffect(() => {
    setInternalIsOpen(isOpen);
  }, [isOpen]);

  const onClose = () => {
    setInternalIsOpen(false);
  };

  const onTry = () => {
    onClose();
  };

  const onSignIn = () => {
    onSigninClick();
  };

  const onSeePricingClick = () => {
    onClose();
    openPlansModal();
  };

  return (
    <Modal
      disableAnimationsForSSR
      bgBlur
      isOpen={internalIsOpen}
      position={ModalPosition.Center}
      onClose={onClose}
    >
      <ModalContent hasModalHeadingOffset className={styles.guestModal}>
        <div className={styles.closeBtnContainer}>
          <div className={styles.closeBtn}>
            <ModalCloseIcon onClick={onClose} />
          </div>
        </div>
        <div className={styles.listsShapesContainer}>
          <Image
            src={listsShapes}
            className={styles.listsShapes}
            alt="Lists shapes"
          />
        </div>
        <div className={styles.content}>
          <div className={styles.titleContainer}>
            <Title size={isBellow648px ? 40 : 60} weight="light">
              Your thoughts, knowledge and resources.
              <br /> Neatly organized in lists.
            </Title>
          </div>

          <div className={styles.subtitleContainer}>
            <Title component="h2" size={25} weight="regular">
              Lists have been around forever. They are simple and work out of
              the box. We leverage that, adding a bit of flavor to make it
              spicy.
            </Title>
          </div>

          <div className={styles.btns}>
            <Button
              center
              onClick={onSignIn}
              variant="accent"
              rootClassName={styles.signInBtnRoot}
            >
              Sign in
            </Button>
            <Button
              center
              onClick={onTry}
              variant="basic"
              rootClassName={styles.tryBtnRoot}
            >
              Try it without account
            </Button>
          </div>

          <p className={styles.helperText}>
            With FREE account, you get {CONSTRAINTS.FREE_PLAN.MAX_LISTS} lists
            for free with maxium of {CONSTRAINTS.FREE_PLAN.MAX_LIST_ITEMS} list
            items in each list. Want to unlock superpowers?
            <Button
              variant="nostyle"
              rootClassName={styles.seePricingBtnRoot}
              className={styles.seePricingBtn}
              narrow
              onClick={onSeePricingClick}
            >
              See pricing
            </Button>
          </p>
        </div>
      </ModalContent>
    </Modal>
  );
}
