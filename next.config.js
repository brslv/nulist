const frontmatterPlugin = require("./lib/frontmatter");
// We need to transpile the emoji-button component by ourselves: more - https://github.com/joeattardi/emoji-button/issues/166
const withTM = require("next-transpile-modules")(["@joeattardi/emoji-button"]);
const withMDX = require("@next/mdx")({
  extension: /\.mdx?$/,
  options: {
    remarkPlugins: [frontmatterPlugin],
  },
});

module.exports = withMDX(
  withTM({
    reactStrictMode: true,
    experimental: { nftTracing: true },
    pageExtensions: ["ts", "tsx", "md", "mdx"],
  })
);
